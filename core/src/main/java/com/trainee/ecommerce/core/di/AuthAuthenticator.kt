package com.trainee.ecommerce.core.di

import android.content.SharedPreferences
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.trainee.ecommerce.core.data.model.TokenBody
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.core.data.remote.response.TokenResponse
import com.trainee.ecommerce.core.util.NetworkConstant.BASE_URL
import com.trainee.ecommerce.core.util.NetworkConstant.CODE_UNAUTHORIZED
import com.trainee.ecommerce.core.util.NetworkConstant.HEADER_AUTHORIZATION
import com.trainee.ecommerce.core.util.UserPreference.refreshToken
import com.trainee.ecommerce.core.util.UserPreference.token
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class AuthAuthenticator @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val authorizationListener: AuthorizationListener,
    private val tokenInterceptor: TokenInterceptor,
    private val chuckerInterceptor: ChuckerInterceptor
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        synchronized(this) {
            return runBlocking {
                try {
                    val token = sharedPreferences.refreshToken

                    val newToken = getNewToken(token)
                    if (newToken == null) {
                        throw HttpException(newToken)
                    }

                    newToken.let {
                        sharedPreferences.token = it.token.accessToken
                        sharedPreferences.refreshToken = it.token.refreshToken
                        response.request.newBuilder()
                            .header(HEADER_AUTHORIZATION, "Bearer ${it.token.accessToken}")
                            .build()
                    }
                } catch (e: HttpException) {
                    if (e.code() == CODE_UNAUTHORIZED) {
                        authorizationListener.setUnauthorized()
                        null
                    } else {
                        null
                    }
                }
            }
        }
    }

    private suspend fun getNewToken(refreshToken: String?): TokenResponse {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(tokenInterceptor)
            .addInterceptor(chuckerInterceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

        val service = retrofit.create(ApiService::class.java)
        val tokenBody = TokenBody(refreshToken.toString())
        return service.refreshToken(tokenBody)
    }
}
