package com.trainee.ecommerce.core.data.repository

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.core.data.remote.response.Profile
import com.trainee.ecommerce.core.util.UserPreference.image
import com.trainee.ecommerce.core.util.UserPreference.name
import okhttp3.MultipartBody
import javax.inject.Inject

class ProfileRepository @Inject constructor(
    private val apiService: ApiService,
    private val userPref: SharedPreferences
) {
    fun profile(
        image: MultipartBody.Part,
        name: MultipartBody.Part
    ): LiveData<com.trainee.ecommerce.core.data.Result<Profile>> =
        liveData {
            emit(com.trainee.ecommerce.core.data.Result.Loading)
            try {
                val response = apiService.profile(image, name)
                val responseBody = response.profile
                userPref.name = responseBody.userName
                userPref.image = responseBody.userImage

                emit(com.trainee.ecommerce.core.data.Result.Success(responseBody))
            } catch (e: Exception) {
                emit(com.trainee.ecommerce.core.data.Result.Error(e))
            }
        }
}
