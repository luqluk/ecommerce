package com.trainee.ecommerce.core.di

import android.content.SharedPreferences
import com.trainee.ecommerce.core.BuildConfig.API_KEY
import com.trainee.ecommerce.core.util.NetworkConstant.HEADER_API_KEY
import com.trainee.ecommerce.core.util.NetworkConstant.HEADER_AUTHORIZATION
import com.trainee.ecommerce.core.util.NetworkConstant.LOGIN
import com.trainee.ecommerce.core.util.NetworkConstant.REFRESH
import com.trainee.ecommerce.core.util.NetworkConstant.REGISTER
import com.trainee.ecommerce.core.util.UserPreference.token
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class TokenInterceptor @Inject constructor(private val userPref: SharedPreferences) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url.encodedPath
        val isAuthUrl = url.contains(REGISTER) || url.contains(LOGIN) || url.contains(REFRESH)

        val headerName = if (isAuthUrl) HEADER_API_KEY else HEADER_AUTHORIZATION
        val headerValue = if (isAuthUrl) API_KEY else "Bearer ${userPref.token}"

        val newRequest = chain.request().newBuilder()
            .addHeader(headerName, headerValue)
            .build()

        return chain.proceed(newRequest)
    }
}
