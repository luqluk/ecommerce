package com.trainee.ecommerce.core.data.model

data class AuthBody(
    val email: String? = null,
    val password: String? = null,
    val firebaseToken: String? = null
)
