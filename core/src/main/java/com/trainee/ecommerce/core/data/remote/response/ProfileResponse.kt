package com.trainee.ecommerce.core.data.remote.response

import com.google.gson.annotations.SerializedName

data class ProfileResponse(
    val code: Int,
    val message: String,
    @SerializedName("data") val profile: Profile
)

data class Profile(
    val userName: String,
    val userImage: String
)
