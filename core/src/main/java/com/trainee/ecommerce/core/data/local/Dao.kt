package com.trainee.ecommerce.core.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import androidx.room.Update
import com.trainee.ecommerce.core.data.remote.response.CartEntity

@Dao
interface Dao {
    @Query("SELECT * FROM product")
    fun getCartProduct(): LiveData<List<CartEntity>>

    @Insert(onConflict = REPLACE)
    suspend fun insertCart(product: CartEntity)

    @Delete
    suspend fun deleteChecked(vararg product: CartEntity)

    @Update
    suspend fun update(vararg product: CartEntity)

    @Query("UPDATE product SET isSelected = :isSelected")
    suspend fun updateAll(isSelected: Boolean)
}
