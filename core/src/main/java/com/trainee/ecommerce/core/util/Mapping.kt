package com.trainee.ecommerce.core.util

import com.trainee.ecommerce.core.data.local.WishlistEntity
import com.trainee.ecommerce.core.data.model.Checkout
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.data.remote.response.DetailProduct
import com.trainee.ecommerce.core.data.remote.response.PaymentData
import com.trainee.ecommerce.core.data.remote.response.Transaction


object Mapping {

    fun DetailProduct.asCartEntity(): CartEntity {
        return CartEntity(
            this.brand,
            this.description,
            this.image.firstOrNull().toString(),
            this.productId,
            this.productName,
            this.productVariant.firstOrNull()?.variantPrice.toString().toInt(),
            this.productRating,
            this.productVariant.firstOrNull()?.variantName.toString(),
            this.sale,
            this.stock,
            this.store,
            this.totalRating,
            this.totalReview,
            this.totalSatisfaction,
            this.quantity,
            this.isSelected
        )
    }

    fun DetailProduct.asWishlistEntity(): WishlistEntity {
        return WishlistEntity(
            this.brand,
            this.description,
            this.image.firstOrNull().toString(),
            this.productId,
            this.productName,
            this.productVariant.firstOrNull()?.variantPrice.toString().toInt(),
            this.productRating,
            this.productVariant.firstOrNull()?.variantName.toString(),
            this.sale,
            this.stock,
            this.store,
            this.totalRating,
            this.totalReview,
            this.totalSatisfaction,
            this.isSelected
        )
    }

    fun WishlistEntity.asCartEntity(): CartEntity {
        return CartEntity(
            this.brand,
            this.description,
            this.image,
            this.productId,
            this.productName,
            this.productPrice,
            this.productRating,
            this.productVariant,
            this.sale,
            this.stock,
            this.store,
            this.totalRating,
            this.totalReview,
            this.totalSatisfaction,
        )
    }

    fun Transaction.asPaymentData(): PaymentData {
        return PaymentData(
            this.date,
            this.invoiceId,
            this.payment,
            this.status,
            this.time,
            this.total
        )
    }

    fun CartEntity.asCheckout(): Checkout {
        return Checkout(
            this.brand,
            this.description,
            this.image,
            this.productId,
            this.productName,
            this.productPrice,
            this.productRating,
            this.productVariant,
            this.sale,
            this.stock,
            this.store,
            this.totalRating,
            this.totalReview,
            this.totalSatisfaction,
            this.quantity
        )
    }
}
