package com.trainee.ecommerce.core.data.remote.response

import com.google.gson.annotations.SerializedName

data class ReviewResponse(
    val code: Int,
    val data: List<Review>,
    val message: String
)

data class Review(
    @SerializedName("userImage")
    val userImage: String,
    @SerializedName("userName")
    val userName: String,
    @SerializedName("userRating")
    val userRating: Int,
    @SerializedName("userReview")
    val userReview: String
)
