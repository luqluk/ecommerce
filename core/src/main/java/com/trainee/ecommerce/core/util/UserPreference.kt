package com.trainee.ecommerce.core.util

import android.content.SharedPreferences

object UserPreference {

    private inline fun SharedPreferences.editMe(operation: (SharedPreferences.Editor) -> Unit) {
        val editMe = edit()
        operation(editMe)
        editMe.apply()
    }

    var SharedPreferences.position
        get() = getInt(POSITION, 0)
        set(value) {
            editMe {
                it.putInt(POSITION, value)
            }
        }

    var SharedPreferences.name
        get() = getString(NAME, "")
        set(value) {
            editMe {
                it.putString(NAME, value)
            }
        }

    var SharedPreferences.image
        get() = getString(IMAGE, "")
        set(value) {
            editMe {
                it.putString(IMAGE, value)
            }
        }

    var SharedPreferences.token
        get() = getString(TOKEN, "")
        set(value) {
            editMe {
                it.putString(TOKEN, value)
            }
        }

    var SharedPreferences.refreshToken
        get() = getString(REFRESH_TOKEN, "")
        set(value) {
            editMe {
                it.putString(REFRESH_TOKEN, value)
            }
        }

    var SharedPreferences.state
        get() = getBoolean(STATE, false)
        set(value) {
            editMe {
                it.putBoolean(STATE, value)
            }
        }

    var SharedPreferences.is_first
        get() = getBoolean(IS_FIRST, true)
        set(value) {
            editMe {
                it.putBoolean(IS_FIRST, value)
            }
        }

    var SharedPreferences.expire
        get() = getLong(EXPIRE, 0)
        set(value) {
            editMe {
                it.putLong(EXPIRE, value)
            }
        }

    var SharedPreferences.dark
        get() = getBoolean(MODE, false)
        set(value) {
            editMe {
                it.putBoolean(MODE, value)
            }
        }

    var SharedPreferences.lang
        get() = getString(LANG, "en")
        set(value) {
            editMe {
                it.putString(LANG, value)
            }
        }

    private const val POSITION = "position_key"
    private const val NAME = "name_key"
    private const val IMAGE = "image_key"
    private const val TOKEN = "token_key"
    private const val REFRESH_TOKEN = "refresh_token_key"
    private const val EXPIRE = "expire_key"
    private const val STATE = "state_key"
    private const val IS_FIRST = "first_key"
    private const val MODE = "mode"
    private const val LANG = "lang"
}
