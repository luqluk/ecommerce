package com.trainee.ecommerce.core.data.model

data class TokenBody(
    val token: String
)
