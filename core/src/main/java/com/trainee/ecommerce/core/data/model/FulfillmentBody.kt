package com.trainee.ecommerce.core.data.model

data class FulfillmentBody(
    val payment: String,
    val items: List<FulfillmentItems>
)

data class FulfillmentItems(
    val productId: String,
    val variantName: String,
    val quantity: Int
)
