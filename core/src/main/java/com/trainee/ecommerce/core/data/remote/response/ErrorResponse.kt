package com.trainee.ecommerce.core.data.remote.response

data class ErrorResponse(
    val code: Int? = null,
    val message: String? = null
)
