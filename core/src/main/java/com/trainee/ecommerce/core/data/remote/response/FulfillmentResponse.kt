package com.trainee.ecommerce.core.data.remote.response

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class FulfillmentResponse(
    val code: Int,
    @SerializedName("data")
    val data: PaymentData?,
    val message: String
)

@Keep
@Parcelize
data class PaymentData(
    val date: String,
    val invoiceId: String,
    val payment: String,
    val status: Boolean,
    val time: String,
    val total: Int
) : Parcelable
