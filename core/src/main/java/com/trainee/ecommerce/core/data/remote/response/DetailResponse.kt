package com.trainee.ecommerce.core.data.remote.response

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class DetailResponse(
    val code: Int,
    @SerializedName("data")
    val data: DetailProduct,
    val message: String
)

@Keep
@Parcelize
data class DetailProduct(
    @SerializedName("brand")
    val brand: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("image")
    val image: List<String>,

    @SerializedName("productId")
    val productId: String,

    @SerializedName("productName")
    val productName: String,

    @SerializedName("productPrice")
    var productPrice: Int,

    @SerializedName("productRating")
    val productRating: Double,

    @SerializedName("productVariant")
    var productVariant: List<ProductVariant>,

    @SerializedName("sale")
    val sale: Int,

    @SerializedName("stock")
    val stock: Int,

    @SerializedName("store")
    val store: String,

    @SerializedName("totalRating")
    val totalRating: Int,

    @SerializedName("totalReview")
    val totalReview: Int,

    @SerializedName("totalSatisfaction")
    val totalSatisfaction: Int,

    @SerializedName("quantity")
    var quantity: Int = 0,

    @SerializedName("isSelected")
    var isSelected: Boolean = false
) : Parcelable

@Keep
@Parcelize
data class ProductVariant(
    @SerializedName("variantName")
    var variantName: String,

    @SerializedName("variantPrice")
    var variantPrice: Int
) : Parcelable
