package com.trainee.ecommerce.core.data.remote.response

import com.google.gson.annotations.SerializedName

data class RegisterResponse(
    val code: Int,
    val message: String,
    @SerializedName("data") val register: Register
)

data class Register(
    val accessToken: String,
    val refreshToken: String,
    val expiresAt: Long
)
