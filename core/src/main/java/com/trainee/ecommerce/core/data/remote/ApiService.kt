package com.trainee.ecommerce.core.data.remote

import com.trainee.ecommerce.core.data.model.AuthBody
import com.trainee.ecommerce.core.data.model.FulfillmentBody
import com.trainee.ecommerce.core.data.model.RatingBody
import com.trainee.ecommerce.core.data.model.TokenBody
import com.trainee.ecommerce.core.data.remote.response.DetailResponse
import com.trainee.ecommerce.core.data.remote.response.FulfillmentResponse
import com.trainee.ecommerce.core.data.remote.response.LoginResponse
import com.trainee.ecommerce.core.data.remote.response.NewsResponse
import com.trainee.ecommerce.core.data.remote.response.PaymentResponse
import com.trainee.ecommerce.core.data.remote.response.ProductResponse
import com.trainee.ecommerce.core.data.remote.response.ProfileResponse
import com.trainee.ecommerce.core.data.remote.response.RegisterResponse
import com.trainee.ecommerce.core.data.remote.response.ReviewResponse
import com.trainee.ecommerce.core.data.remote.response.SearchResponse
import com.trainee.ecommerce.core.data.remote.response.TokenResponse
import com.trainee.ecommerce.core.data.remote.response.TransactionResponse
import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @POST("login")
    suspend fun login(
        @Body loginBody: AuthBody
    ): LoginResponse

    @GET("top-headlines?country=id&category=science")
    suspend fun getNews(@Query("apiKey") apiKey: String): NewsResponse

    @POST("register")
    suspend fun register(
        @Body registerBody: AuthBody
    ): RegisterResponse

    @POST("products")
    suspend fun product(
        @Query("search") search: String? = null,
        @Query("brand") brand: String? = null,
        @Query("lowest") lowest: Int? = null,
        @Query("highest") highest: Int? = null,
        @Query("sort") sort: String? = null,
        @Query("limit") limit: Int,
        @Query("page") page: Int,
    ): ProductResponse

    @GET("products/{id}")
    suspend fun detailProduct(
        @Path("id") productId: String
    ): DetailResponse

    @GET("payment")
    suspend fun payment(): PaymentResponse

    @GET("transaction")
    suspend fun transaction(): TransactionResponse

    @GET("review/{id}")
    suspend fun review(
        @Path("id") productId: String
    ): ReviewResponse

    @POST("search")
    suspend fun search(
        @Query("query") search: String? = null,
    ): SearchResponse

    @POST("refresh")
    suspend fun refreshToken(
        @Body token: TokenBody
    ): TokenResponse

    @POST("fulfillment")
    suspend fun fulfillment(
        @Body data: FulfillmentBody
    ): FulfillmentResponse

    @POST("rating")
    suspend fun rating(
        @Body data: RatingBody
    ): FulfillmentResponse

    @Multipart
    @POST("profile")
    suspend fun profile(
        @Part userImage: MultipartBody.Part,
        @Part userName: MultipartBody.Part,
    ): ProfileResponse
}
