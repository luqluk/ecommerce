package com.trainee.ecommerce.core.util

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class LocaleChangeReceiver(private val listener: OnLocaleChangeListener) : BroadcastReceiver() {

    interface OnLocaleChangeListener {
        fun onLocaleChange()
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == Intent.ACTION_LOCALE_CHANGED) {
            listener.onLocaleChange()
        }
    }
}
