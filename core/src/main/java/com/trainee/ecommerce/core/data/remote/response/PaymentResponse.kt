package com.trainee.ecommerce.core.data.remote.response

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class PaymentResponse(
    val code: Int,
    @SerializedName("data")
    val data: List<Payment>,
    val message: String
)

data class Payment(
    val title: String,
    val item: List<ItemPayment>
)

@Keep
@Parcelize
data class ItemPayment(
    val image: String,
    val label: String,
    val status: Boolean
) : Parcelable
