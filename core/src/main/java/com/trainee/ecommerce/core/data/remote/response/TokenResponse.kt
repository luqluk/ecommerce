package com.trainee.ecommerce.core.data.remote.response

import com.google.gson.annotations.SerializedName

data class TokenResponse(
    val code: Int,
    val message: String,
    @SerializedName("data")
    val token: Token
)

data class Token(
    val accessToken: String,
    val refreshToken: String,
    val expiresAt: Long
)
