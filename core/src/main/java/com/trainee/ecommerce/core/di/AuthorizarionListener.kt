package com.trainee.ecommerce.core.di

import kotlinx.coroutines.flow.MutableSharedFlow
import javax.inject.Inject

interface AuthorizationListener {
    val isUnauthorized: MutableSharedFlow<Boolean>
    suspend fun setUnauthorized()
}

class AuthorizationListenerImpl @Inject constructor() : AuthorizationListener {
    override val isUnauthorized = MutableSharedFlow<Boolean>()

    override suspend fun setUnauthorized() {
        isUnauthorized.emit(true)
    }
}
