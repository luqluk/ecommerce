package com.trainee.ecommerce.core.data.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.trainee.ecommerce.core.R
import com.trainee.ecommerce.core.data.local.Dao
import com.trainee.ecommerce.core.data.local.NotifDao
import com.trainee.ecommerce.core.data.local.NotificationEntity
import com.trainee.ecommerce.core.data.local.WishlistDao
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.local.WishlistEntity
import com.trainee.ecommerce.core.data.model.FulfillmentBody
import com.trainee.ecommerce.core.data.model.RatingBody
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.data.remote.response.DetailProduct
import com.trainee.ecommerce.core.data.remote.response.DetailResponse
import com.trainee.ecommerce.core.data.remote.response.FulfillmentResponse
import com.trainee.ecommerce.core.data.remote.response.Product
import com.trainee.ecommerce.core.data.remote.response.ReviewResponse
import com.trainee.ecommerce.core.data.remote.response.SearchResponse
import com.trainee.ecommerce.core.util.Event
import com.trainee.ecommerce.core.util.Mapping.asWishlistEntity
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val context: Context,
    private val apiService: ApiService,
    private val productDao: Dao,
    private val wishlistDao: WishlistDao,
    private val notificationDao: NotifDao

) {
    fun getProduct(
        search: String?,
        brand: String?,
        lowest: Int?,
        highest: Int?,
        sort: String?
    ): LiveData<PagingData<Product>> {
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                initialLoadSize = 10,
                prefetchDistance = 1
            ),
            pagingSourceFactory = {
                com.trainee.ecommerce.core.data.ProductPagingSource(
                    apiService,
                    search,
                    brand,
                    lowest,
                    highest,
                    sort
                )
            }
        ).liveData
    }

    fun search(search: String?): LiveData<Result<SearchResponse>> =
        liveData {
            emit(Result.Loading)
            try {
                val response = apiService.search(search)
                emit(Result.Success(response))
            } catch (e: Exception) {
                emit(Result.Error(e))
            }
        }

    fun detailProduct(id: String): LiveData<Result<DetailResponse>> =
        liveData {
            emit(Result.Loading)
            try {
                val response = apiService.detailProduct(id)
                emit(Result.Success(response))
            } catch (e: Exception) {
                emit(Result.Error(e))
            }
        }

    fun reviewProduct(id: String): LiveData<Result<ReviewResponse>> =
        liveData {
            emit(Result.Loading)
            try {
                val response = apiService.review(id)
                emit(Result.Success(response))
            } catch (e: Exception) {
                emit(Result.Error(e))
            }
        }

    fun fulfillment(fulfillmentBody: FulfillmentBody): LiveData<Result<FulfillmentResponse>> =
        liveData {
            emit(Result.Loading)
            try {
                val response = apiService.fulfillment(fulfillmentBody)
                emit(Result.Success(response))
            } catch (e: Exception) {
                emit(Result.Error(e))
            }
        }



    fun rating(data: RatingBody): LiveData<Result<FulfillmentResponse>> =
        liveData {
            emit(Result.Loading)
            try {
                val response = apiService.rating(data)
                emit(Result.Success(response))
            } catch (e: Exception) {
                emit(Result.Error(e))
            }
        }

    suspend fun transaction() = apiService.transaction()

    // LocalDatabase

    fun insertCart(cartEntity: CartEntity): LiveData<Result<Event<String>>> =
        liveData {
            emit(Result.Loading)
            try {
                productDao.insertCart(cartEntity)
                emit((Result.Success(Event(context.getString(R.string.item_added_successfully)))))
            } catch (e: Exception) {
                emit(Result.Error(e))
            }
        }

    fun insertWishlist(detailProduct: DetailProduct): LiveData<Result<Event<String>>> =
        liveData {
            emit(Result.Loading)
            try {
                wishlistDao.insertWishlist(detailProduct.asWishlistEntity())
                emit((Result.Success(Event(context.getString(R.string.item_added_to_wishlist)))))
            } catch (e: Exception) {
                emit(Result.Error(e))
            }
        }

    suspend fun deleteCart(product: CartEntity) {
        val data = listOf(product).toTypedArray()
        productDao.deleteChecked(product)
    }

    suspend fun updateCart(product: List<CartEntity>) {
        val data = product.toTypedArray()
        productDao.update(*data)
    }

    suspend fun updateAllCart(isSelected: Boolean) {
        productDao.updateAll(isSelected)
    }

    fun getAllCart(): LiveData<List<CartEntity>> = productDao.getCartProduct()

    fun getAllWishlist(): LiveData<List<WishlistEntity>> = wishlistDao.getCartWishlist()

    fun deleteWishlist(wishlistEntity: WishlistEntity): LiveData<Result<Event<String>>> =
        liveData {
            emit(Result.Loading)
            try {
                val data = listOf(wishlistEntity).toTypedArray()
                wishlistDao.delete(wishlistEntity)
                emit((Result.Success(Event(context.getString(R.string.item_removed_from_wishlist)))))
            } catch (e: Exception) {
                emit(Result.Error(e))
            }
        }

    fun getAllNotification(): LiveData<List<NotificationEntity>> = notificationDao.getNotification()

    suspend fun updateNotification(notificationEntity: NotificationEntity) {
        val data = listOf(notificationEntity).toTypedArray()
        notificationDao.update(notificationEntity)
    }

    suspend fun insertNotification(notificationEntity: NotificationEntity) =
        notificationDao.insertNotification(
            notificationEntity
        )
}
