package com.trainee.ecommerce.core.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.core.data.remote.response.Product

class ProductPagingSource(
    private val apiService: ApiService,
    private val search: String?,
    private val brand: String?,
    private val lowest: Int?,
    private val highest: Int?,
    private val sort: String?
) : PagingSource<Int, Product>() {
    override fun getRefreshKey(state: PagingState<Int, Product>): Int? {
        return state.anchorPosition?.let { position ->
            val anchorPage = state.closestPageToPosition(position)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Product> {
        return try {
            val page = params.key
                ?: com.trainee.ecommerce.core.data.ProductPagingSource.Companion.INITIAL_PAGE_INDEX
            val response =
                apiService.product(search, brand, lowest, highest, sort, params.loadSize, page)

            val data = response.data.product
            LoadResult.Page(
                data = data,
                prevKey = if (page == 1) null else page - 1,
                nextKey = if (page == response.data.totalPages) null else page + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    private companion object {
        const val INITIAL_PAGE_INDEX = 1
    }
}
