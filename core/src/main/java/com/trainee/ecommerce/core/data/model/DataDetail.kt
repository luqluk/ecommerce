package com.trainee.ecommerce.core.data.model

import com.google.gson.Gson
import com.trainee.ecommerce.core.data.remote.response.DetailProduct

object DataDetail {
    private const val json = "{\n" +
            "        \"productId\": \"95f413a2-37c8-4735-8806-8e5628bafe17\",\n" +
            "        \"productName\": \"ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray\",\n" +
            "        \"productPrice\": 24499000,\n" +
            "        \"image\": [\n" +
            "            \"image1\",\n" +
            "            \"image2\",\n" +
            "            \"image3\"\n" +
            "        ],\n" +
            "        \"brand\": \"Asus\",\n" +
            "        \"description\": \"Description\",\n" +
            "        \"store\": \"AsusStore\",\n" +
            "        \"sale\": 12,\n" +
            "        \"stock\": 2,\n" +
            "        \"totalRating\": 7,\n" +
            "        \"totalReview\": 5,\n" +
            "        \"totalSatisfaction\": 100,\n" +
            "        \"productRating\": 4.0,\n" +
            "        \"productVariant\": [\n" +
            "            {\n" +
            "                \"variantName\": \"RAM 16GB\",\n" +
            "                \"variantPrice\": 0\n" +
            "            },\n" +
            "            {\n" +
            "                \"variantName\": \"RAM 32GB\",\n" +
            "                \"variantPrice\": 1000000\n" +
            "            }\n" +
            "        ]\n" +
            "    }"

    val gson = Gson()
    private val myData = gson.fromJson(json, DetailProduct::class.java)

    fun getDetail(): DetailProduct {
        return myData
    }
}
