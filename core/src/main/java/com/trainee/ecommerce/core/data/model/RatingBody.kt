package com.trainee.ecommerce.core.data.model

data class RatingBody(
    val invoiceId: String,
    val rating: Int,
    val review: String
)
