package com.trainee.ecommerce.core.data.model

data class ProductChip(
    var brand: Int? = null,
    var lowest: Int? = null,
    var highest: Int? = null,
    var sort: Int? = null,
)
