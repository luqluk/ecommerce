package com.trainee.ecommerce.core.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notification")
data class NotificationEntity(
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
    val title: String,
    val body: String,
    val image: String,
    val time: String,
    val date: String,
    val type: String,
    var isNew: Boolean = true
)
