package com.trainee.ecommerce.core.data.repository

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.trainee.ecommerce.core.data.local.AppDatabase
import com.trainee.ecommerce.core.data.model.AuthBody
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.core.data.remote.response.Login
import com.trainee.ecommerce.core.data.remote.response.Register
import com.trainee.ecommerce.core.util.UserPreference.expire
import com.trainee.ecommerce.core.util.UserPreference.image
import com.trainee.ecommerce.core.util.UserPreference.name
import com.trainee.ecommerce.core.util.UserPreference.refreshToken
import com.trainee.ecommerce.core.util.UserPreference.state
import com.trainee.ecommerce.core.util.UserPreference.token
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.trainee.ecommerce.core.data.Result
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class AuthRepository @Inject constructor(
    val apiService: ApiService,
    val userPref: SharedPreferences,
    val database: AppDatabase
) {
    fun register(registerBody: AuthBody): LiveData<Result<Register>> =
        liveData {
            emit(Result.Loading)
            try {
                val response = apiService.register(registerBody)
                val responseBody = response.register

                setUser(responseBody)
                emit(Result.Success(responseBody))
            } catch (e: Exception) {
                emit(Result.Error(e))
            }
        }

    fun login(loginBody: AuthBody): LiveData<Result<Login>> =
        liveData {
            emit(Result.Loading)
            try {
                val response = apiService.login(loginBody)
                val responseBody = response.login

                userPref.name = responseBody.userName
                userPref.image = responseBody.userImage
                userPref.token = responseBody.accessToken
                userPref.refreshToken = responseBody.refreshToken
                userPref.expire = responseBody.expiresAt
                userPref.state = true

                emit(Result.Success(responseBody))
            } catch (e: Exception) {
                emit(Result.Error(e))
            }
        }

    fun deleteUser() {
        userPref.name = ""
        userPref.image = ""
        userPref.token = ""
        userPref.refreshToken = ""
        userPref.expire = 0
        userPref.state = false
    }

    private fun setUser(register: Register) {
        userPref.token = register.accessToken
        userPref.refreshToken = register.refreshToken
        userPref.expire = register.expiresAt
        userPref.state = true
    }

    fun clearTable() {
        runBlocking {
            launch(Dispatchers.IO) {
                database.clearAllTables()
            }
        }
    }
}
