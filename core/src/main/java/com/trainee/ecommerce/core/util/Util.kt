package com.trainee.ecommerce.core.util

import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.util.TypedValue
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.util.PatternsCompat
import com.google.gson.Gson
import com.trainee.ecommerce.core.data.remote.response.ErrorResponse
import retrofit2.HttpException
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.Locale

object Util {

    private const val minimumLength = 8
    private const val byteArray = 1024
    private const val FILENAME_FORMAT = "dd-MMM-yyyy"
    private const val MAXIMAL_SIZE = 1000000

    fun View.show() {
        visibility = View.VISIBLE
    }

    fun View.hide() {
        visibility = View.GONE
    }

    fun isEmailValid(input: String): Boolean {
        return PatternsCompat.EMAIL_ADDRESS.matcher(input).matches()
    }

    fun isPasswordGood(input: String): Boolean {
        return input.length >= minimumLength
    }

    private val timeStamp: String = SimpleDateFormat(
        FILENAME_FORMAT,
        Locale.US
    ).format(System.currentTimeMillis())

    private fun createCustomTempFile(context: Context): File {
        val storageDir: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(timeStamp, ".jpg", storageDir)
    }

    fun uriToFile(selectedImg: Uri, context: Context): File {
        val contentResolver: ContentResolver = context.contentResolver
        val myFile = createCustomTempFile(context)

        val inputStream = contentResolver.openInputStream(selectedImg) as InputStream
        val outputStream: OutputStream = FileOutputStream(myFile)
        val buf = ByteArray(byteArray)
        var len: Int
        while (inputStream.read(buf).also { len = it } > 0) outputStream.write(buf, 0, len)
        outputStream.close()
        inputStream.close()

        return myFile
    }

    fun reduceFileImage(file: File): File {
        val bitmap = BitmapFactory.decodeFile(file.path)

        var streamLength: Int
        var compressQuality = compressQuality
        do {
            val bmpStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream)
            val bmpPicByteArray = bmpStream.toByteArray()
            streamLength = bmpPicByteArray.size
            compressQuality -= compressQualityMinus
        } while (streamLength > MAXIMAL_SIZE)
        bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, FileOutputStream(file))
        return file
    }

    fun String.toCurrencyFormat(): String {
        val localeID = Locale("in", "ID")
        val doubleValue = this.toDoubleOrNull() ?: return this
        val numberFormat = NumberFormat.getCurrencyInstance(localeID)
        numberFormat.minimumFractionDigits = 0
        return numberFormat.format(doubleValue)
    }

    fun Throwable.errorResponse(): String? {

        return when (this) {
            is HttpException -> {
                val errorResponse = this.response()?.errorBody()
                val error = Gson().fromJson(errorResponse?.string(), ErrorResponse::class.java)
                error?.message
            }

            is IOException -> {
                "No Internet Connection"
            }

            is Exception -> {
                "Something went wrong"
            }

            else -> {
                "Something went wrong"
            }
        }
    }

    fun errorColor(context: Context): Int {
        val typedValue = TypedValue()
        context.theme.resolveAttribute(
            com.google.android.material.R.attr.colorError,
            typedValue,
            true
        )
        return ContextCompat.getColor(context, typedValue.resourceId)
    }

    fun successColor(context: Context): Int {
        val typedValue = TypedValue()
        context.theme.resolveAttribute(
            com.google.android.material.R.attr.colorPrimary,
            typedValue,
            true
        )
        return ContextCompat.getColor(context, typedValue.resourceId)
    }

    fun String?.emptyToNull(): String? {
        return if (this.isNullOrEmpty()) {
            null
        } else {
            this
        }
    }

    fun isCurrentLanguageIndonesia(): Boolean {
        val currentLocale = Locale.getDefault()
        val languageCode = currentLocale.language
        return languageCode == "in"
    }

    private const val compressQuality = 100
    private const val compressQualityMinus = 5
}
