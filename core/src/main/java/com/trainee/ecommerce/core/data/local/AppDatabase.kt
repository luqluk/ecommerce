package com.trainee.ecommerce.core.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.trainee.ecommerce.core.data.remote.response.CartEntity

@Database(
    entities = [CartEntity::class, WishlistEntity::class, NotificationEntity::class],
    version = 3,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun detailDao(): Dao
    abstract fun wishlistDao(): WishlistDao
    abstract fun notificationDao(): NotifDao
}
