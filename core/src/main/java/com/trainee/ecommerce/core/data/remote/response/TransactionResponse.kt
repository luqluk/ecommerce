package com.trainee.ecommerce.core.data.remote.response

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class TransactionResponse(
    val code: Int,
    @SerializedName("data")
    val data: List<Transaction>,
    val message: String
)

@Keep
@Parcelize
data class Transaction(
    val date: String,
    val image: String,
    val invoiceId: String,
    val items: List<ItemTransaction>,
    val name: String,
    val payment: String,
    val rating: Int,
    val review: String,
    val status: Boolean,
    val time: String,
    val total: Int
) : Parcelable

@Keep
@Parcelize
data class ItemTransaction(
    val productId: String,
    val quantity: Int,
    val variantName: String
) : Parcelable
