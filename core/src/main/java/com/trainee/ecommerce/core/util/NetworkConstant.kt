package com.trainee.ecommerce.core.util

object NetworkConstant {
    const val REGISTER = "register"
    const val LOGIN = "login"
    const val REFRESH = "refresh"
    const val HEADER_API_KEY = "API_KEY"
    const val HEADER_AUTHORIZATION = "Authorization"
    const val CODE_UNAUTHORIZED = 401
    const val CODE_NOT_FOUND = 404
    const val CODE_INTERNAL_SEVER_ERROR = 500

    //        const val BASE_URL = "http://172.20.10.13:8080/"
    const val BASE_URL = "http://172.17.20.183:8080/"
//    const val BASE_URL = "http://192.168.18.37:8080/     "
}
