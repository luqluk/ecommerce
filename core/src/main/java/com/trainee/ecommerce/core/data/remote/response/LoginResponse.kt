package com.trainee.ecommerce.core.data.remote.response

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class LoginResponse(
    val code: Int,
    val message: String,
    @SerializedName("data") val login: Login
)

data class Login(
    val accessToken: String,
    val refreshToken: String,
    val expiresAt: Long,
    val userName: String,
    val userImage: String
)
