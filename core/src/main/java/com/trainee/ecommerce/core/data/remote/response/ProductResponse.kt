package com.trainee.ecommerce.core.data.remote.response

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class ProductResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val data: DataProduct,
    @SerializedName("message")
    val message: String
)

data class DataProduct(
    @SerializedName("currentItemCount")
    val currentItemCount: Int,
    @SerializedName("items")
    val product: List<Product>,
    @SerializedName("itemsPerPage")
    val itemsPerPage: Int,
    @SerializedName("pageIndex")
    val pageIndex: Int,
    @SerializedName("totalPages")
    val totalPages: Int
)

@Keep
@Parcelize
data class Product(
    @SerializedName("brand")
    val brand: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("productId")
    val productId: String,
    @SerializedName("productName")
    val productName: String,
    @SerializedName("productPrice")
    val productPrice: Int,
    @SerializedName("productRating")
    val productRating: Double,
    @SerializedName("sale")
    val sale: Int,
    @SerializedName("store")
    val store: String
) : Parcelable
