package com.trainee.ecommerce.core.data.remote.response

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
@Entity(tableName = "product")
data class CartEntity(
    @SerializedName("brand")
    val brand: String,

    @SerializedName("description")
    val description: String,

    @field:TypeConverters()
    @SerializedName("image")
    val image: String,

    @field:ColumnInfo(name = "productId")
    @field:PrimaryKey
    @SerializedName("productId")
    val productId: String,

    @SerializedName("productName")
    val productName: String,

    @SerializedName("productPrice")
    var productPrice: Int,

    @SerializedName("productRating")
    val productRating: Double,

    @SerializedName("productVariant")
    val productVariant: String,

    @SerializedName("sale")
    val sale: Int,

    @SerializedName("stock")
    val stock: Int,

    @SerializedName("store")
    val store: String,

    @SerializedName("totalRating")
    val totalRating: Int,

    @SerializedName("totalReview")
    val totalReview: Int,

    @SerializedName("totalSatisfaction")
    val totalSatisfaction: Int,

    @SerializedName("quantity")
    var quantity: Int = 0,

    @SerializedName("isSelected")
    var isSelected: Boolean = false
) : Parcelable
