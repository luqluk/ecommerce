package com.trainee.ecommerce.core.di

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import com.trainee.ecommerce.core.R
import com.trainee.ecommerce.core.data.local.AppDatabase
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.core.data.repository.AuthRepository
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.core.data.repository.ProfileRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    private const val INTERVAL = 3600L

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "app"
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideFirebaseAnalytics(): FirebaseAnalytics {
        return Firebase.analytics
    }

    @Singleton
    @Provides
    fun provideRemoteConfig(): FirebaseRemoteConfig {
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = INTERVAL
        }
        val remoteConfig = Firebase.remoteConfig
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        return remoteConfig
    }

    @Singleton
    @Provides
    fun provideAuthRepository(
        apiService: ApiService,
        sharedPreferences: SharedPreferences,
        database: AppDatabase
    ): AuthRepository {
        return AuthRepository(apiService, sharedPreferences, database)
    }

    @Singleton
    @Provides
    fun provideProfileRepository(
        apiService: ApiService,
        sharedPreferences: SharedPreferences
    ): ProfileRepository {
        return ProfileRepository(apiService, sharedPreferences)
    }

    @Singleton
    @Provides
    fun provideMainRepository(
        @ApplicationContext context: Context,
        apiService: ApiService,
        database: AppDatabase
    ): MainRepository {
        return MainRepository(
            context,
            apiService,
            database.detailDao(),
            database.wishlistDao(),
            database.notificationDao()
        )
    }

}
