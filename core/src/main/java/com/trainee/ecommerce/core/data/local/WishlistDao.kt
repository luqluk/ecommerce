package com.trainee.ecommerce.core.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import androidx.room.Update

@Dao
interface WishlistDao {
    @Query("SELECT * FROM wishlist")
    fun getCartWishlist(): LiveData<List<WishlistEntity>>

    @Insert(onConflict = REPLACE)
    suspend fun insertWishlist(product: WishlistEntity)

    @Query("DELETE FROM wishlist")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(vararg product: WishlistEntity)

    @Update
    suspend fun update(vararg product: WishlistEntity)
}
