package com.trainee.ecommerce.core.data.model

import android.os.Parcelable
import androidx.annotation.Keep
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Checkout(
    @SerializedName("brand")
    val brand: String,

    @SerializedName("description")
    val description: String,

    @field:TypeConverters()
    @SerializedName("image")
    val image: String,

    @SerializedName("productId")
    val productId: String,

    @SerializedName("productName")
    val productName: String,

    @SerializedName("productPrice")
    var productPrice: Int,

    @SerializedName("productRating")
    val productRating: Double,

    @SerializedName("productVariant")
    val productVariant: String,

    @SerializedName("sale")
    val sale: Int,

    @SerializedName("stock")
    val stock: Int,

    @SerializedName("store")
    val store: String,

    @SerializedName("totalRating")
    val totalRating: Int,

    @SerializedName("totalReview")
    val totalReview: Int,

    @SerializedName("totalSatisfaction")
    val totalSatisfaction: Int,

    @SerializedName("quantity")
    var quantity: Int = 0,
) : Parcelable
