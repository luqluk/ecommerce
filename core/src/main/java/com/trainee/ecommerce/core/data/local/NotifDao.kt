package com.trainee.ecommerce.core.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import androidx.room.Update

@Dao
interface NotifDao {
    @Query("SELECT * FROM notification ORDER BY id DESC")
    fun getNotification(): LiveData<List<NotificationEntity>>

    @Insert(onConflict = REPLACE)
    suspend fun insertNotification(notification: NotificationEntity)

    @Query("DELETE FROM notification")
    suspend fun deleteAll()

    @Update
    suspend fun update(vararg notification: NotificationEntity)
}
