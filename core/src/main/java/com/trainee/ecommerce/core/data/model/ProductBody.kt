package com.trainee.ecommerce.core.data.model

data class ProductBody(
    var search: String? = null,
    var brand: String? = null,
    var lowest: Int? = null,
    var highest: Int? = null,
    var sort: String? = null,
)
