package com.trainee.ecommerce.main.cart

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {

    fun getAllCart() = mainRepository.getAllCart()

    fun deleteCheck(detailProduct: CartEntity) {
        viewModelScope.launch {
            mainRepository.deleteCart(detailProduct)
        }
    }

    fun update(detailProduct: List<CartEntity>) {
        viewModelScope.launch {
            mainRepository.updateCart(detailProduct)
        }
    }

    fun updateAll(isSelected: Boolean) {
        viewModelScope.launch {
            mainRepository.updateAllCart(isSelected)
        }
    }
}
