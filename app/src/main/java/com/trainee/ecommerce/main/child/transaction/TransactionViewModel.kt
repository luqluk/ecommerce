package com.trainee.ecommerce.main.child.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trainee.ecommerce.core.data.remote.response.TransactionResponse
import com.trainee.ecommerce.core.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {

    private val _transaction =
        MutableLiveData<com.trainee.ecommerce.core.data.Result<TransactionResponse>>()
    val transaction: LiveData<com.trainee.ecommerce.core.data.Result<TransactionResponse>> =
        _transaction

    init {
        transaction()
    }

    fun transaction() {
        viewModelScope.launch {
            _transaction.value = (com.trainee.ecommerce.core.data.Result.Loading)
            try {
                val response = mainRepository.transaction()
                _transaction.value = (com.trainee.ecommerce.core.data.Result.Success(response))
            } catch (e: Exception) {
                _transaction.value = (com.trainee.ecommerce.core.data.Result.Error(e))
            }
        }
    }
}
