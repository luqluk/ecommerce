package com.trainee.ecommerce.main.profil

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.color.MaterialColors
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.util.Util
import com.trainee.ecommerce.core.util.Util.errorResponse
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.reduceFileImage
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.core.util.Util.uriToFile
import com.trainee.ecommerce.databinding.FragmentProfileBinding
import com.trainee.ecommerce.databinding.ItemDialogBinding
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : Fragment() {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private var getFile: File? = null
    private lateinit var uri: Uri
    private val profileViewModel: ProfileViewModel by viewModels()

    @Inject
    lateinit var userPref: SharedPreferences

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        initEvent()
        observeData()
    }

    private fun observeData() {
        binding.btnPost.setOnClickListener {
            analytics.logEvent("button_click") {
                param("button_name", "Button post profile")
            }
            val name = binding.name.text.toString()

            val inputMethodManager =
                requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(it?.windowToken, 0)

            val nameMultiPart = MultipartBody.Part.createFormData("userName", name)
            val imageMultiPart: MultipartBody.Part
            if (getFile != null) {
                val file = reduceFileImage(getFile as File)
                val requestImageFile = file.asRequestBody("image/*".toMediaType())
                imageMultiPart =
                    MultipartBody.Part.createFormData(
                        "userImage",
                        file.name,
                        requestImageFile
                    )
            } else {
                imageMultiPart =
                    MultipartBody.Part.createFormData(
                        "userImage",
                        ""
                    )
            }

            profileViewModel.updateProfile(imageMultiPart, nameMultiPart)
                .observe(viewLifecycleOwner) { result ->

                    if (result != null) {
                        when (result) {
                            is Result.Loading -> {
                                binding.progressBar.show()
                                binding.btnPost.isEnabled = false
                            }

                            is Result.Success -> {
                                binding.progressBar.hide()
                                findNavController().navigate(R.id.action_profileFragment_to_mainFragment)
                            }

                            is Result.Error -> {
                                binding.progressBar.hide()
                                binding.btnPost.isEnabled = true
                                Toast.makeText(
                                    requireContext(),
                                    result.error.errorResponse().toString(),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                            else -> {}
                        }
                    }
                }
        }
    }

    private fun initEvent() {
        binding.btnPost.isEnabled = false
        binding.name.doOnTextChanged { text, start, before, count ->
            binding.btnPost.isEnabled = !text.isNullOrEmpty()
        }
        val pickMedia =
            registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
                if (uri != null) {
                    val myFile = uriToFile(uri, requireContext())
                    getFile = myFile
                    Glide.with(requireContext()).load(uri).circleCrop().into(binding.profileImage)
                    binding.iconPerson.hide()
                } else {
                    Toast.makeText(requireContext(), "No media selected", Toast.LENGTH_SHORT).show()
                }
            }

        val takePicture =
            registerForActivityResult(ActivityResultContracts.TakePicture()) { isSaved ->
                if (isSaved) {
                    val myFile = uriToFile(uri, requireContext())
                    getFile = myFile
                    Glide.with(requireContext())
                        .load(uri)
                        .circleCrop()
                        .into(binding.profileImage)
                    binding.iconPerson.hide()
                } else {
                    Toast.makeText(requireContext(), "Gagal mengambil gambar", Toast.LENGTH_SHORT)
                        .show()
                }
            }

        binding.profileImage.setOnClickListener {
            val dialogMainBinding = ItemDialogBinding.inflate(layoutInflater)

            val dialog = MaterialAlertDialogBuilder(requireContext())
                .setView(dialogMainBinding.root)
                .create()

            dialog.show()

            dialogMainBinding.btnCamera.setOnClickListener {
                dialog.dismiss()
                val photoFile = File.createTempFile(
                    "IMG_",
                    ".jpg",
                    requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                )

                uri = FileProvider.getUriForFile(
                    requireContext(),
                    "${requireContext().packageName}.provider",
                    photoFile
                )
                takePicture.launch(uri)
            }

            dialogMainBinding.btnGallery.setOnClickListener {
                dialog.dismiss()
                pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
            }
        }
    }

    private fun initView() {
        val color1 = ForegroundColorSpan(
            MaterialColors.getColor(requireView(), com.google.android.material.R.attr.colorPrimary)
        )

        val color2 = ForegroundColorSpan(
            MaterialColors.getColor(requireView(), com.google.android.material.R.attr.colorPrimary)
        )

        val inputString = getString(R.string.register_terms)

        val spannableString = SpannableString(inputString)

        val startIndex1 = if (Util.isCurrentLanguageIndonesia()) idStartIndex1 else enStartIndex1
        val endIndex1 = if (Util.isCurrentLanguageIndonesia()) idEndIndex1 else enEndIndex1
        val startIndex2 = if (Util.isCurrentLanguageIndonesia()) idStartIndex2 else enStartIndex2
        val endIndex2 = if (Util.isCurrentLanguageIndonesia()) idEndIndex2 else enEndIndex2

        spannableString.setSpan(color1, startIndex1, endIndex1, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        spannableString.setSpan(color2, startIndex2, endIndex2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

        binding.term.text = spannableString
    }

    companion object {
        const val enStartIndex1 = 32
        const val enEndIndex1 = 49
        const val enStartIndex2 = 54
        const val enEndIndex2 = 69
        const val idStartIndex1 = 38
        const val idEndIndex1 = 55
        const val idStartIndex2 = 62
        const val idEndIndex2 = 81
    }
}
