package com.trainee.ecommerce.main.cart.checkout.payment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import com.google.gson.Gson
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.remote.response.PaymentResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PaymentViewModel @Inject constructor(
    private val remoteConfig: FirebaseRemoteConfig
) : ViewModel() {

    private val _payment =
        MutableLiveData<Result<PaymentResponse>>()
    val payment: LiveData<Result<PaymentResponse>> =
        _payment

    private val _paymentUpdate =
        MutableLiveData<Result<PaymentResponse>>()
    val paymentUpdate: LiveData<Result<PaymentResponse>> =
        _paymentUpdate

    init {
        remoteConfig()
        remoteConfigUpdate()
    }

    private fun remoteConfig() {
        _payment.value = Result.Loading
        remoteConfig.fetchAndActivate()
            .addOnFailureListener {
                _payment.value = (Result.Error(it))
            }
            .addOnCompleteListener { task ->
                val value = remoteConfig.getString("payment")
                val gson = Gson()
                val data = gson.fromJson(value, PaymentResponse::class.java)
                _payment.value = (Result.Success(data))
            }
    }

    private fun remoteConfigUpdate() {
        remoteConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                if (configUpdate.updatedKeys.contains("payment")) {
                    remoteConfig.activate().addOnCompleteListener {
                        val value = remoteConfig.getString("payment")
                        val gson = Gson()
                        val data = gson.fromJson(value, PaymentResponse::class.java)
                        _payment.value = (Result.Success(data))
                    }
                }
            }

            override fun onError(error: FirebaseRemoteConfigException) {
                _payment.value = (Result.Error(error))
            }
        })
    }
}
