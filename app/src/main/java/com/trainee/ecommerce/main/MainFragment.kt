package com.trainee.ecommerce.main

import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.util.UserPreference.name
import com.trainee.ecommerce.databinding.FragmentMainBinding
import com.trainee.ecommerce.main.child.store.StoreViewModel
import com.trainee.ecommerce.main.child.store.detail.DetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@ExperimentalBadgeUtils
@AndroidEntryPoint
class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private val navHostFragment: NavHostFragment by lazy {
        childFragmentManager.findFragmentById(R.id.nhf_child) as NavHostFragment
    }

    private val navController by lazy {
        navHostFragment.navController
    }

    @Inject
    lateinit var userPref: SharedPreferences

    private val viewModel: DetailViewModel by viewModels()

    private val storViewModel: StoreViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (userPref.name == "") {
            findNavController().navigate(R.id.action_mainFragment_to_profileFragment)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

        binding.topAppBar.setOnMenuItemClickListener { menu ->
            when (menu.itemId) {
                R.id.cart -> {
                    findNavController().navigate(R.id.main_to_cart)
                    true
                }

                R.id.notification -> {
                    findNavController().navigate(R.id.main_to_notification)
                    true
                }

                R.id.more -> {
                    findNavController().navigate(R.id.menuFragment)
                    true
                }

                else -> false
            }
        }

        viewModel.getAllCart().observe(viewLifecycleOwner) {
            val badge = BadgeDrawable.create(requireContext())
            badge.number = it.size

            if (it.isNotEmpty()) {
                BadgeUtils.attachBadgeDrawable(
                    badge,
                    binding.topAppBar,
                    R.id.cart
                )
            }
        }

        viewModel.getNotification().observe(viewLifecycleOwner) {
            val new = it.filter { data -> data.isNew }

            val badge = BadgeDrawable.create(requireContext())
            badge.number = new.size

            if (new.isNotEmpty()) {
                BadgeUtils.attachBadgeDrawable(
                    badge,
                    binding.topAppBar,
                    R.id.notification
                )
            }
        }

        viewModel.getAllWishlist().observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                binding.navigation?.getOrCreateBadge(R.id.wishlistFragment)?.apply {
                    number = it.size
                    isVisible = true
                }

                binding.navigationRail?.getOrCreateBadge(R.id.wishlistFragment)?.apply {
                    number = it.size
                    isVisible = true
                }
            } else {
                binding.navigation?.removeBadge(R.id.wishlistFragment)
                binding.navigationRail?.removeBadge(R.id.wishlistFragment)
            }
        }
    }

    private fun initView() {
        binding.navigation?.setupWithNavController(navController)
        binding.navigationRail?.setupWithNavController(navController)

        binding.navigation?.setOnItemReselectedListener {}
        binding.navigationRail?.setOnItemReselectedListener {}

        binding.navigationView?.setupWithNavController(navController)

        binding.topAppBar.title = userPref.name
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Log.d("TAG", "onConfigurationChanged: ${storViewModel.paramChip.value}")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
