package com.trainee.ecommerce.main.child.store.detail.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.StarOutline
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import coil.compose.AsyncImage
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.remote.response.Review
import com.trainee.ecommerce.core.data.remote.response.ReviewResponse
import com.trainee.ecommerce.main.child.store.detail.DetailViewModel
import com.trainee.ecommerce.main.child.store.detail.ui.theme.ecommerceTheme
import com.trainee.ecommerce.main.child.store.detail.ui.theme.poppins
import dagger.hilt.android.AndroidEntryPoint
@Suppress("FunctionNaming")
@AndroidEntryPoint
class ReviewComposeFragment : Fragment() {

    private val viewModel: DetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                ecommerceTheme {
                    MyApp()
                }
            }
        }
    }

    @Composable
    fun MyApp() {
        val reviewProduct by viewModel.reviewProduct.observeAsState()

        MyApp(
            Modifier.fillMaxSize(),
            reviewProduct
        )
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun MyApp(
        modifier: Modifier = Modifier,
        reviewProduct: Result<ReviewResponse>?
    ) {
        val onBack = {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        Surface(modifier) {
            Scaffold(
                topBar = {
                    Column {
                        TopAppBar(
                            colors = TopAppBarDefaults.smallTopAppBarColors(
                                containerColor = MaterialTheme.colorScheme.background
                            ),
                            title = {
                                Text(
                                    stringResource(id = R.string.buyer_review),
                                    fontFamily = poppins,
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            navigationIcon = {
                                IconButton(onClick = onBack) {
                                    Icon(
                                        imageVector = Icons.Filled.ArrowBack,
                                        contentDescription = "Localized Description"
                                    )
                                }
                            },
                            actions = {
                            }
                        )
                        Divider()
                    }
                }
            ) { innerPadding ->
                Column(
                    Modifier
                        .padding(innerPadding)
                        .fillMaxSize(),
                    verticalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    if (reviewProduct != null) {
                        when (reviewProduct) {
                            is Result.Loading -> {
                                OnLoading()
                            }

                            is Result.Success -> {
                                val list = reviewProduct.data.data
                                LazyColumn(modifier = Modifier.fillMaxWidth()) {
                                    items(list) { index ->
                                        ListItem(index)
                                    }
                                }
                            }

                            is Result.Error -> {
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun ListItem(data: Review) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 8.dp),
        ) {
            Row {
                AsyncImage(
                    modifier = Modifier
                        .padding(end = 8.dp, bottom = 8.dp)
                        .size(36.dp),
                    model = data.userImage,
                    placeholder = painterResource(id = R.drawable.img_thumbnail),
                    error = painterResource(id = R.drawable.img_thumbnail),

                    contentDescription = ""
                )

                Column {
                    Text(
                        text = data.userName,
                        style = TextStyle(
                            fontFamily = poppins,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 12.sp
                        )
                    )
                    Row(modifier = Modifier) {
                        val maxRating = Companion.maxRating
                        val userRating = data.userRating
                        for (i in 1..maxRating) {
                            val isFilled = i <= userRating
                            val icon = if (isFilled) {
                                Icons.Filled.Star
                            } else {
                                Icons.Filled.StarOutline
                            }
                            Icon(
                                imageVector = icon,
                                contentDescription = "",
                                modifier = Modifier.size(12.dp)
                            )
                        }
                    }
                }
            }
            Text(
                text = data.userReview,
                style = TextStyle(
                    fontFamily = poppins,
                    fontSize = 12.sp
                )
            )
        }
        Divider()
    }

    @Composable
    fun OnLoading() {
        Box(
            modifier = Modifier
                .fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun listItemPreview() {
        ListItem(
            Review(
                userImage = "https://i.pravatar.cc/150?img=3",
                userName = "John Doe",
                userRating = 2,
                userReview = "Review"
            )
        )
    }

    @Preview
    @Composable
    fun MyAppPreview() {
        ecommerceTheme {
            MyApp(
                modifier = Modifier.fillMaxSize(),
                reviewProduct =
                Result.Success(
                    ReviewResponse(
                        code = 0,
                        message = "",
                        data = listOf(
                            Review(
                                userImage = "https://i.pravatar.cc/150?img=3",
                                userName = "John Doe",
                                userRating = 2,
                                userReview = "Review"
                            )
                        )
                    )
                )
            )
        }
    }

    companion object {
        private const val maxRating = 5
    }
}
