package com.trainee.ecommerce.main.child.store.search

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.databinding.FragmentSearchBinding
import com.trainee.ecommerce.main.child.store.StoreViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val ARG_SEARCH = "search"

@AndroidEntryPoint
class SearchFragment : DialogFragment() {
    private var _binding: FragmentSearchBinding? = null

    private val binding get() = _binding!!
    private var search = ""
    private lateinit var adapter: SearchAdapter

    private val viewModel: StoreViewModel by activityViewModels()

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val value = it.getString(ARG_SEARCH)
            (value)?.let { searchValue ->
                search = searchValue
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        binding.search.requestFocus()
        showSoftKeyboard(binding.search)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.searching
                .observe(viewLifecycleOwner) { result ->
                    if (result != null) {
                        when (result) {
                            is com.trainee.ecommerce.core.data.Result.Loading -> {
                                binding.progressBar.show()
                            }

                            is com.trainee.ecommerce.core.data.Result.Success -> {
                                analytics.logEvent(FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS) {
                                    param(FirebaseAnalytics.Param.SEARCH_TERM, search)
                                }
                                binding.progressBar.hide()
                                adapter.submitList(result.data.data)

                                if (result.data.data.isEmpty() && binding.search.text?.isNotEmpty() == true) {
                                    binding.errorLayout.show()
                                    binding.descriptionError.text =
                                        getString(R.string.search_empty, search)
                                } else {
                                    binding.errorLayout.hide()
                                }
                            }

                            is com.trainee.ecommerce.core.data.Result.Error -> {
                                binding.progressBar.hide()
                            }
                        }
                    }
                }
        }

        binding.buttonError.setOnClickListener {
            binding.search.text?.clear()
            binding.errorLayout.hide()
            viewModel.resetSearch()
        }

        binding.search.doOnTextChanged { text, _, _, _ ->
            search = text.toString()
            viewModel.setSearch(text.toString())
        }

        binding.search.setOnKeyListener(
            View.OnKeyListener { _, keyCode, event ->
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                    requireActivity().supportFragmentManager.setFragmentResult(
                        "searchKey",
                        bundleOf(
                            "search" to search
                        )
                    )
                    dismiss()
                    return@OnKeyListener true
                }
                false
            }
        )
    }

    private fun initView() {
        if (search.isNotEmpty()) {
            viewModel.setSearch(search)
        }

        binding.search.setText(search)
        binding.rvSearch.layoutManager = LinearLayoutManager(requireContext())
        adapter = SearchAdapter {
            requireActivity().supportFragmentManager.setFragmentResult(
                "searchKey",
                bundleOf(
                    "search" to it
                )
            )
            dismiss()
        }

        binding.rvSearch.adapter = adapter
    }

    private fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = requireContext().getSystemService(InputMethodManager::class.java)
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
        view.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                // Move the cursor to the end of the text
                binding.search.text?.length?.let { binding.search.setSelection(it) }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback = object : OnBackPressedCallback(
            true
        ) {
            override fun handleOnBackPressed() {
                dismiss()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance(search: String?) =
            SearchFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_SEARCH, search)
                }
            }
    }
}
