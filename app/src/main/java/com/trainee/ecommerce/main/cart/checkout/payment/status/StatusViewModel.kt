package com.trainee.ecommerce.main.cart.checkout.payment.status

import androidx.lifecycle.ViewModel
import com.trainee.ecommerce.core.data.model.RatingBody
import com.trainee.ecommerce.core.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class StatusViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {

    fun rating(data: RatingBody) = mainRepository.rating(data)
}
