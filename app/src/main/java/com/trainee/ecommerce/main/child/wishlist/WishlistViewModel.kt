package com.trainee.ecommerce.main.child.wishlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.trainee.ecommerce.core.data.local.WishlistEntity
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {

    private val _input = MutableLiveData<CartEntity>()
    private val input: LiveData<CartEntity> = _input

    private val _deleteWishlist = MutableLiveData<WishlistEntity>()
    private val deleteWishlist: LiveData<WishlistEntity> = _deleteWishlist

    var spanCount: Int = 1

    fun insertCart(detailProduct: CartEntity) {
        _input.postValue(detailProduct)
    }

    val insert = input.switchMap {
        mainRepository.insertCart(it)
    }

    fun getAllCart() = mainRepository.getAllCart()

    val getAllWishlist = mainRepository.getAllWishlist()

    fun deleteWishlist(wishlistEntity: WishlistEntity) {
        _deleteWishlist.postValue(wishlistEntity)
    }

    val delete = deleteWishlist.switchMap {
        mainRepository.deleteWishlist(it)
    }
}
