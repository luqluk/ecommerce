package com.trainee.ecommerce.main.child.store.search

import android.view.View
import com.android.commons.base.BaseAdapter
import com.trainee.ecommerce.databinding.ItemSearchBinding

class SearchAdapter(
    private val onItemClick: (String) -> Unit
) : BaseAdapter<String, ItemSearchBinding>(ItemSearchBinding::inflate) {
    override fun onItemBind(): (String, ItemSearchBinding, View) -> Unit {
        return { data, binding, itemView ->
            binding.textResult.text = data

            itemView.setOnClickListener {
                onItemClick(data)
            }
        }
    }
}
