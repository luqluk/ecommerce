package com.trainee.ecommerce.main.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.trainee.ecommerce.core.data.local.NotificationEntity
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.databinding.FragmentNotificationBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationFragment : Fragment() {

    private var _binding: FragmentNotificationBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: NotificationProductAdapter
    private lateinit var data: List<NotificationEntity>

    private val viewModel: NotificationViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNotificationBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun initView() {
        binding.rvCart.layoutManager = LinearLayoutManager(requireContext())

        adapter = NotificationProductAdapter {
            val data = it.copy(
                isNew = false
            )
            viewModel.update(data)
        }
        binding.rvCart.adapter = adapter
        binding.rvCart.itemAnimator?.changeDuration = 0
    }

    private fun observeData() {
        viewModel.getNotification().observe(viewLifecycleOwner) { entityList ->
            adapter.submitList(entityList)
            data = entityList

            if (entityList.isEmpty()) {
                binding.errorLayout.show()
            } else {
                binding.errorLayout.hide()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.topAppBar.setNavigationOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        initView()
        observeData()
    }
}
