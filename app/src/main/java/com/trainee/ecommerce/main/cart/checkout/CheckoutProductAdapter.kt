package com.trainee.ecommerce.main.cart.checkout

import android.util.TypedValue
import android.view.View
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import com.android.commons.base.BaseAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.android.material.R.attr
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.model.Checkout
import com.trainee.ecommerce.core.util.Util.errorColor
import com.trainee.ecommerce.core.util.Util.toCurrencyFormat
import com.trainee.ecommerce.databinding.ListItemCheckoutBinding

class CheckoutProductAdapter(private val listener: OnItemClickListener) :
    BaseAdapter<Checkout, ListItemCheckoutBinding>(ListItemCheckoutBinding::inflate) {

    override fun onItemBind(): (Checkout, ListItemCheckoutBinding, View) -> Unit {
        return { data, binding, itemView ->
            binding.apply {
                Glide.with(itemView.context).load(data.image).placeholder(R.drawable.img_thumbnail)
                    .transform(RoundedCorners(roundingRadius)).into(icon)

                productName.text = data.productName
                spec.text = data.productVariant
                price.text = data.productPrice.toString().toCurrencyFormat()
                num.text = data.quantity.toString()

                val typedValue = TypedValue()
                itemView.context.theme.resolveAttribute(attr.colorError, typedValue, true)

                val typedValueNormal = TypedValue()
                itemView.context.theme.resolveAttribute(attr.colorOnSurface, typedValueNormal, true)

                val color = if (data.stock < remainingStock) {
                    errorColor(itemView.context)
                } else {
                    ContextCompat.getColor(
                        itemView.context,
                        typedValueNormal.resourceId
                    )
                }

                val textStock = if (data.stock < remainingStock) {
                    itemView.context.getString(R.string.low_stock, data.stock.toString())
                } else {
                    itemView.context.getString(R.string.stock_available, data.stock.toString())
                }

                stock.text = textStock
                stock.setTextColor(color)

                itemView.setOnClickListener {
//                    listener.onItemClick(data)
                }
                var number = data.quantity

                btnPlus.setOnClickListener {
                    if (number < data.stock) {
                        number += 1

                        val dataPlus = data.copy(
                            quantity = number
                        )
                        num.text = number.toString()
                        listener.onPlus(dataPlus)
                    } else {
                        listener.onPlus(null)
                    }
                }

                btnMinus.setOnClickListener {
                    if (number > 1) {
                        number -= 1
                        val dataMinus = data.copy(
                            quantity = number
                        )
                        num.text = number.toString()
                        listener.onMinus(dataMinus)
                    }
                }
            }
        }
    }

    interface OnItemClickListener : AdapterView.OnItemClickListener {
        fun onMinus(data: Checkout)
        fun onPlus(data: Checkout?)
    }

    companion object {
        private const val remainingStock = 10
        private const val roundingRadius = 8
    }
}
