package com.trainee.ecommerce.main.child.transaction

import android.view.View
import com.android.commons.base.BaseAdapter
import com.bumptech.glide.Glide
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.remote.response.Transaction
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.core.util.Util.toCurrencyFormat
import com.trainee.ecommerce.databinding.ListItemTransactionBinding

class TransactionAdapter(private val reviewClick: (Transaction) -> Unit) :
    BaseAdapter<Transaction, ListItemTransactionBinding>(ListItemTransactionBinding::inflate) {
    override fun onItemBind(): (Transaction, ListItemTransactionBinding, View) -> Unit {
        return { item, binding, itemView ->
            binding.apply {
                tvDate.text = item.date
                if (item.status) {
                    chipStatus.text = itemView.context.getString(R.string.success)
                } else {
                    chipStatus.text = itemView.context.getString(R.string.failed)
                }
                Glide.with(itemView.context).load(item.image).placeholder(R.drawable.img_thumbnail)
                    .into(icon)

                productName.text = item.name
                total.text = itemView.context.getString(R.string.items, item.items.size.toString())
                price.text = item.total.toString().toCurrencyFormat()

                if (item.review != "" && item.rating != 0) {
                    btnReview.hide()
                } else {
                    btnReview.show()
                }

                btnReview.setOnClickListener {
                    reviewClick(item)
                }
            }
        }
    }
}
