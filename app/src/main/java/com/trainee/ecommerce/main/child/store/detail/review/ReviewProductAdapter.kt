package com.trainee.ecommerce.main.child.store.detail.review

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.remote.response.Review
import com.trainee.ecommerce.databinding.ListItemReviewBinding

class ReviewProductAdapter :
    ListAdapter<Review, ReviewProductAdapter.ListProductViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListProductViewHolder {
        val binding =
            ListItemReviewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return ListProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ListProductViewHolder, position: Int) {
        val data = getItem(position)
        if (data != null) {
            holder.bind(data)
        }
    }

    class ListProductViewHolder(
        val binding: ListItemReviewBinding
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: Review) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(data.userImage)
                    .placeholder(R.drawable.img_thumbnail)
                    .circleCrop()
                    .into(icon)

                title.text = data.userName
                rating.rating = data.userRating.toFloat()
                review.text = data.userReview
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<Review> =
            object : DiffUtil.ItemCallback<Review>() {
                override fun areItemsTheSame(oldProduct: Review, newProduct: Review): Boolean {
                    return oldProduct.userName == newProduct.userName
                }

                @SuppressLint("DiffUtilEquals")
                override fun areContentsTheSame(oldProduct: Review, newProduct: Review): Boolean {
                    return oldProduct == newProduct
                }
            }
    }
}
