package com.trainee.ecommerce.main.child.store.detail

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.local.WishlistEntity
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.data.remote.response.DetailProduct
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.core.util.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    @ApplicationContext private val context: Context,
    private val mainRepository: MainRepository,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _id = MutableLiveData<String?>()
    private val id: LiveData<String?> = _id

    private val _input = MutableLiveData<CartEntity>()
    private val input: LiveData<CartEntity> = _input

    private val _inputWishlist = MutableLiveData<DetailProduct>()
    private val inputWishlist: LiveData<DetailProduct> = _inputWishlist

    private val _deleteWishlist = MutableLiveData<WishlistEntity>()
    private val deleteWishlist: LiveData<WishlistEntity> = _deleteWishlist

    var checkedId = 0

    init {
        getDetailProduct()
        getReview()
    }

    fun getDetailProduct() {
        val id = savedStateHandle.get<String>("id") ?: ""
        _id.postValue(id)
    }

    fun getReview() {
        val id = savedStateHandle.get<String>("id") ?: ""
        _id.postValue(id)
    }

    val reviewProduct = id.switchMap {
        mainRepository.reviewProduct(it.toString())
    }

    val detailProduct = id.switchMap {
        mainRepository.detailProduct(it.toString())
    }

    fun insertCart(detailProduct: CartEntity) {
        _input.postValue(detailProduct)
    }

    fun update(detailProduct: List<CartEntity>): LiveData<Result<Event<String>>> = liveData {
        emit(Result.Loading)
        try {
            mainRepository.updateCart(detailProduct)
            emit(
                Result.Success(
                    Event(context.getString(R.string.item_updated_successfully))
                )
            )
        } catch (e: Exception) {
            emit(Result.Error(e))
        }
    }

    val insert = input.switchMap {
        mainRepository.insertCart(it)
    }

    fun insertWishlist(detailProduct: DetailProduct) {
        _inputWishlist.postValue(detailProduct)
    }

    val insertWishlist = inputWishlist.switchMap {
        mainRepository.insertWishlist(it)
    }

    fun getAllCart() = mainRepository.getAllCart()

    fun getNotification() = mainRepository.getAllNotification()

    fun getAllWishlist() = mainRepository.getAllWishlist()

    fun deleteWishlist(wishlistEntity: WishlistEntity) {
        _deleteWishlist.postValue(wishlistEntity)
    }

    val delete = deleteWishlist.switchMap {
        mainRepository.deleteWishlist(it)
    }
}
