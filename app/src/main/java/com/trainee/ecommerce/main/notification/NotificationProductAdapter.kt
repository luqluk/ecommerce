package com.trainee.ecommerce.main.notification

import android.annotation.SuppressLint
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.local.NotificationEntity
import com.trainee.ecommerce.databinding.ListItemNotificationBinding

class NotificationProductAdapter(private val onClick: (NotificationEntity) -> Unit) :
    ListAdapter<NotificationEntity, NotificationProductAdapter.ListNotificationViewHolder>(
        DIFF_CALLBACK
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListNotificationViewHolder {
        val binding =
            ListItemNotificationBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return ListNotificationViewHolder(binding, onClick)
    }

    override fun onBindViewHolder(holder: ListNotificationViewHolder, position: Int) {
        val data = getItem(position)

        if (data != null) {
            holder.bind(data)
        }
    }

    class ListNotificationViewHolder(
        val binding: ListItemNotificationBinding,
        private val onclick: (NotificationEntity) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: NotificationEntity) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(data.image)
                    .placeholder(R.drawable.img_thumbnail)
                    .transform(RoundedCorners(roundingRadius))
                    .into(icon)

                titleNotification.text = data.title
                messageNotification.text = data.body
                date.text = data.date
                time.text = data.time
                typeNotification.text = data.type

                val typedValueNormal = TypedValue()
                itemView.context.theme.resolveAttribute(
                    com.google.android.material.R.attr.colorPrimaryContainer,
                    typedValueNormal,
                    true
                )

                val typedValue = TypedValue()
                itemView.context.theme.resolveAttribute(
                    com.google.android.material.R.attr.colorSurfaceContainerLowest,
                    typedValue,
                    true
                )

                if (data.isNew) {
                    cardView.setBackgroundColor(
                        ContextCompat.getColor(
                            itemView.context,
                            typedValueNormal.resourceId
                        )
                    )
                } else {
                    cardView.setBackgroundColor(
                        ContextCompat.getColor(
                            itemView.context,
                            typedValue.resourceId
                        )
                    )
                }

                itemView.setOnClickListener { onclick(data) }
            }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<NotificationEntity> =
            object : DiffUtil.ItemCallback<NotificationEntity>() {
                override fun areItemsTheSame(
                    oldProduct: NotificationEntity,
                    newProduct: NotificationEntity
                ): Boolean {
                    return oldProduct.id == newProduct.id
                }

                @SuppressLint("DiffUtilEquals")
                override fun areContentsTheSame(
                    oldProduct: NotificationEntity,
                    newProduct: NotificationEntity
                ): Boolean {
                    return oldProduct == newProduct
                }
            }
        private const val roundingRadius = 8
    }
}
