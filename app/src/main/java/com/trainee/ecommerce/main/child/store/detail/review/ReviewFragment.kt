package com.trainee.ecommerce.main.child.store.detail.review

//
// @AndroidEntryPoint
// class ReviewFragment : Fragment() {
//
//    private val viewModel: DetailViewModel by viewModels()
//
//    private var _binding: FragmentReviewBinding? = null
//
//    private val binding get() = _binding!!
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//    }
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View {
//        _binding = FragmentReviewBinding.inflate(inflater, container, false)
//
//        return binding.root
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        binding.topAppBar.setNavigationOnClickListener {
//            requireActivity().onBackPressedDispatcher.onBackPressed()
//        }
//
//        val linearLayoutManager = LinearLayoutManager(requireContext())
//        binding.rvReview.layoutManager = linearLayoutManager
//        val adapter = ReviewProductAdapter()
//
//        viewModel.reviewProduct.observe(viewLifecycleOwner) { result ->
//            if (result != null) {
//                when (result) {
//                    is Result.Loading -> {
//                        binding.progressBar.show()
//                        binding.rvReview.hide()
//                    }
//
//                    is Result.Success -> {
//                        adapter.submitList(result.data.data)
//                        binding.rvReview.adapter = adapter
//                        binding.progressBar.hide()
//                        binding.rvReview.show()
//                    }
//
//                    is Result.Error -> {
//                        binding.errorLayout.show()
//                        binding.rvReview.hide()
//                        binding.progressBar.hide()
//                    }
//                }
//            }
//        }
//    }
// }
