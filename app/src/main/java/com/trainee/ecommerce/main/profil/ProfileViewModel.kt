package com.trainee.ecommerce.main.profil

import androidx.lifecycle.ViewModel
import com.trainee.ecommerce.core.data.repository.ProfileRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import okhttp3.MultipartBody
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val profileRepository: ProfileRepository
) : ViewModel() {

    fun updateProfile(image: MultipartBody.Part, name: MultipartBody.Part) =
        profileRepository.profile(image, name)
}
