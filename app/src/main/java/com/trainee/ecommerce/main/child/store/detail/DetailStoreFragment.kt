package com.trainee.ecommerce.main.child.store.detail

// @AndroidEntryPoint
// class DetailStoreFragment : Fragment() {
//    private var _binding: FragmentDetailStoreBinding? = null
//    private val binding get() = _binding!!
//
//    private val viewModel: DetailViewModel by viewModels()
//
//    private var pagerAdapter: DetailImagePagerAdapter? = null
//
//    private lateinit var dataProduct: DetailProduct
//    private var dataProductExist: CartEntity? = null
//    private lateinit var listData: List<CartEntity>
//    private var price: Int = 0
//    private var addPrice: Int = 0
//    private var isFavorite: Boolean = false
//    private lateinit var productId: String
//    private var lastSelectedChipId: Int = -1
//
//    @Inject
//    lateinit var analytics: FirebaseAnalytics
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View {
//        _binding = FragmentDetailStoreBinding.inflate(inflater, container, false)
//
//        return binding.root
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        productId = arguments?.getString("id").toString()
//        viewModel.checkedId = arguments?.getInt("type") ?: 0
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        binding.topAppBar.setNavigationOnClickListener {
//            requireActivity().onBackPressedDispatcher.onBackPressed()
//        }
//
//        initEvent()
//        collectUiState()
//    }
//
//    private fun initEvent() {
//        var selectedChip: Chip?
//        var chipText = ""
//
//        binding.btnFavorite.setOnClickListener {
//            val productVariant = ProductVariant(
//                variantPrice = if (addPrice == 0) dataProduct.productPrice else addPrice,
//                variantName = chipText
//            )
//
//            val data = dataProduct.copy(
//                productVariant = listOf(productVariant)
//            )
//
//            if (isFavorite) {
//                binding.btnFavorite.icon =
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.ic_favorite
//                    )
//                viewModel.deleteWishlist(data.asWishlistEntity())
//
//                isFavorite = false
//            } else {
//                binding.btnFavorite.icon =
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.ic_favorite_filled
//                    )
//                viewModel.insertWishlist(data)
//
//                isFavorite = true
//            }
//        }
//
//        binding.btnBuy.setOnClickListener {
//            val productVariant = ProductVariant(
//                variantPrice = if (addPrice == 0) dataProduct.productPrice else addPrice,
//                variantName = chipText
//            )
//
//            val data = dataProduct.copy(
//                productVariant = listOf(productVariant),
//                quantity = 1
//            )
//
//            if (data.quantity > dataProduct.stock) {
//                Snackbar.make(
//                    binding.root,
//                    getString(R.string.stock_not_available),
//                    Snackbar.LENGTH_LONG
//                )
//                    .setBackgroundTint(errorColor(requireContext()))
//                    .show()
//            } else {
//                findNavController().navigate(
//                    R.id.action_detailStoreFragment_to_checkoutFragment,
//                    Bundle().apply {
//                        putParcelableArrayList("data", ArrayList(listOf(data.asCartEntity())))
//                    }
//                )
//            }
//        }
//        binding.btnSeeAll.setOnClickListener {
//            findNavController().navigate(
//                DetailStoreFragmentComposeDirections.detailToReview(
//                    dataProduct.productId
//                )
//            )
//        }
//
//        binding.chipGroup.setOnCheckedChangeListener { _, checkedId ->
//            if (checkedId == -1) {
//                lastSelectedChipId.let { binding.chipGroup.check(it) }
//            } else {
//                lastSelectedChipId = checkedId
//                selectedChip = binding.root.findViewById(checkedId)
//                chipText = selectedChip?.text.toString()
//
//                Log.d("TAG", "initEvent: $checkedId")
//                addPrice = price + dataProduct.productVariant[checkedId].variantPrice
//                binding.productPrice.text = addPrice.toString().toCurrencyFormat()
//            }
//
//            viewModel.checkedId = checkedId
//        }
//
//        binding.btnCart.setOnClickListener {
//            val productVariant = ProductVariant(
//                variantPrice = if (addPrice == 0) dataProduct.productPrice else addPrice,
//                variantName = chipText
//            )
//
//            dataProductExist = listData.find { it.productId == dataProduct.productId }
//
//            val quantity = dataProductExist?.quantity?.plus(1) ?: 1
//
//            val data = dataProduct.copy(
//                productVariant = listOf(productVariant),
//                quantity = quantity
//            )
//
//            if (data.quantity > dataProduct.stock) {
//                Snackbar.make(
//                    binding.root,
//                    getString(R.string.stock_not_available),
//                    Snackbar.LENGTH_LONG
//                )
//                    .setBackgroundTint(errorColor(requireContext()))
//                    .show()
//                return@setOnClickListener
//            } else {
//                if (dataProductExist != null) {
//                    viewModel.update(listOf(data.asCartEntity()))
//                        .observe(viewLifecycleOwner) { result ->
//                            if (result != null) {
//                                when (result) {
//                                    is Result.Loading -> {
//                                    }
//
//                                    is Result.Success -> {
//                                        result.data.getContentIfNotHandled()?.let {
//                                            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
//                                                .setBackgroundTint(successColor(requireContext()))
//                                                .show()
//                                        }
//                                    }
//
//                                    is Result.Error -> {
//                                    }
//                                }
//                            }
//                        }
//                } else {
//                    val itemCart = Bundle().apply {
//                        putParcelable(FirebaseAnalytics.Param.ITEMS, data.asCartEntity())
//                    }
//
//                    analytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
//                        param(FirebaseAnalytics.Param.ITEMS, itemCart)
//                    }
//
//                    viewModel.insertCart(data.asCartEntity())
//                }
//            }
//        }
//
//        binding.btnShare.setOnClickListener {
//            val shareIntent: Intent = Intent().apply {
//                action = Intent.ACTION_SEND
//                putExtra(
//                    Intent.EXTRA_TEXT,
//                    "Product : ${dataProduct.productName}\n" +
//                        "Price : ${addPrice.toString().toCurrencyFormat()}\n" +
//                        "Link : http://com.trainee.ecommerce/product/${dataProduct.productId}/${viewModel.checkedId}"
//                )
//                type = "text/plain"
//            }
//            startActivity(Intent.createChooser(shareIntent, null))
//        }
//    }
//
//    private fun initView(data: DetailProduct) {
//        dataProduct = data
//        binding.apply {
//            progressBar.hide()
//            productName.text = data.productName
//            productPrice.text = data.productPrice.toString().toCurrencyFormat()
//            price = data.productPrice
//            productSale.text =
//                getString(R.string.sales_detail, data.sale.toString())
//            productRating.text =
//                getString(
//                    R.string.rating,
//                    data.productRating.toString(),
//                    data.totalRating.toString()
//                )
//            productDescription.text = data.description
//            rating.text = data.productRating.toString()
//            satisfaction.text = getString(
//                R.string.satisfaction,
//                data.totalSatisfaction.toString()
//            )
//            rateReview.text = getString(
//                R.string.rate_review,
//                data.totalRating.toString(),
//                data.totalReview.toString()
//            )
//
//            val listImage = data.image
//
//            if (listImage.size == 1) {
//                tabIndicator.hide()
//            }
//
//            pagerAdapter = DetailImagePagerAdapter(requireContext(), listImage)
//            screenViewpager.adapter = pagerAdapter
//
//            tabIndicator.setupWithViewPager(binding.screenViewpager)
//
//            for ((index, variant) in data.productVariant.withIndex()) {
//                addChip(variant.variantName, index)
//            }
//            binding.chipGroup.check(viewModel.checkedId)
//        }
//    }
//
//    private fun collectUiState() {
//        viewModel.detailProduct.observe(viewLifecycleOwner) { result ->
//            if (result != null) {
//                when (result) {
//                    is Result.Loading -> {
//                        binding.progressBar.show()
//                        binding.content.hide()
//                        binding.errorLayout.hide()
//                    }
//
//                    is Result.Success -> {
//                        binding.progressBar.hide()
//                        binding.errorLayout.hide()
//                        initView(result.data.data)
//                        binding.content.show()
//                    }
//
//                    is Result.Error -> {
//                        binding.progressBar.hide()
//                        binding.content.hide()
//                        binding.errorLayout.show()
//                        binding.titleError.text = "500"
//                        binding.descriptionError.text = result.error.errorResponse().toString()
//                    }
//                }
//            }
//        }
//
//        viewModel.getAllCart().observe(viewLifecycleOwner) { list ->
//            listData = list
//        }
//
//        viewModel.getAllWishlist().observe(viewLifecycleOwner) { list ->
//            val data = list.find { it.productId == productId }
//            if (data != null) {
//                binding.btnFavorite.icon =
//                    ContextCompat.getDrawable(
//                        requireContext(),
//                        R.drawable.ic_favorite_filled
//                    )
//                isFavorite = true
//            }
//        }
//
//        viewModel.insert.observe(viewLifecycleOwner) { result ->
//            if (result != null) {
//                when (result) {
//                    is Result.Loading -> {
//                    }
//
//                    is Result.Success -> {
//                        result.data.getContentIfNotHandled()?.let {
//                            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
//                                .setBackgroundTint(successColor(requireContext()))
//                                .show()
//                        }
//                    }
//
//                    is Result.Error -> {
//                    }
//                }
//            }
//        }
//
//        viewModel.insertWishlist.observe(viewLifecycleOwner) { result ->
//            if (result != null) {
//                when (result) {
//                    is Result.Loading -> {}
//                    is Result.Success -> {
//                        result.data.getContentIfNotHandled()?.let {
//                            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
//                                .setBackgroundTint(successColor(requireContext()))
//                                .show()
//                        }
//                    }
//
//                    is Result.Error -> {}
//                }
//            }
//        }
//
//        viewModel.delete.observe(viewLifecycleOwner) { result ->
//            if (result != null) {
//                when (result) {
//                    is Result.Loading -> {}
//                    is Result.Success -> {
//                        result.data.getContentIfNotHandled()?.let {
//                            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
//                                .setBackgroundTint(errorColor(requireContext()))
//                                .show()
//                        }
//                    }
//
//                    is Result.Error -> {}
//                }
//            }
//        }
//    }
//
//    private fun addChip(text: String?, index: Int) {
//        if (text != "" && text != null) {
//            val chipDrawable = ChipDrawable.createFromAttributes(
//                requireContext(),
//                null,
//                0,
//                com.google.android.material.R.style.Widget_Material3_Chip_Suggestion
//            )
//            val chip = Chip(requireContext())
//            chip.text = text
//            chip.id = index
//            chip.setEnsureMinTouchTargetSize(false)
//            chip.setChipDrawable(chipDrawable)
//            binding.chipGroup.addView(chip)
//        }
//    }
// }
