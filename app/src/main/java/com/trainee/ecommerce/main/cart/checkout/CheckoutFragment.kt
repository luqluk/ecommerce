package com.trainee.ecommerce.main.cart.checkout

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.model.Checkout
import com.trainee.ecommerce.core.data.model.FulfillmentBody
import com.trainee.ecommerce.core.data.model.FulfillmentItems
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.data.remote.response.ItemPayment
import com.trainee.ecommerce.core.util.Mapping.asCheckout
import com.trainee.ecommerce.core.util.Util.errorColor
import com.trainee.ecommerce.core.util.Util.errorResponse
import com.trainee.ecommerce.core.util.Util.toCurrencyFormat
import com.trainee.ecommerce.databinding.FragmentCheckoutBinding
import com.trainee.ecommerce.main.cart.checkout.payment.ChoosePaymentFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CheckoutFragment : Fragment(), CheckoutProductAdapter.OnItemClickListener {
    private var _binding: FragmentCheckoutBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: CheckoutProductAdapter
    private val viewModel: CheckoutViewModel by viewModels()
    private lateinit var paymentName: String
    private val checkoutData = ArrayList<Checkout>()

    @Inject
    lateinit var analytics: FirebaseAnalytics

    private val navHostFragment: NavHostFragment by lazy {
        requireActivity().supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
    }

    private val navController by lazy {
        navHostFragment.navController
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCheckoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        paymentListener()
        initEvent()
        binding.btnBuy.setOnClickListener {
            val items = adapter.currentList.map {
                FulfillmentItems(it.productId, it.productVariant, it.quantity)
            }
            val fulfillmentData = FulfillmentBody(paymentName, items)

            viewModel.fulfillment(fulfillmentData).observe(viewLifecycleOwner) { result ->
                if (result != null) {
                    when (result) {
                        is Result.Loading -> {
                            binding.progressBar.visibility = View.VISIBLE
                            binding.btnBuy.visibility = View.INVISIBLE
                        }

                        is Result.Success -> {
                            val itemProduct =
                                checkoutData.map {
                                    Bundle().apply {
                                        putString(FirebaseAnalytics.Param.ITEM_ID, it.productId)
                                        putString("image", it.image)
                                        putString(FirebaseAnalytics.Param.ITEM_NAME, it.productName)
                                        putString(FirebaseAnalytics.Param.ITEM_BRAND, it.brand)
                                        putInt("sale", it.sale)
                                        putString("store", it.store)
                                        putInt(FirebaseAnalytics.Param.PRICE, it.productPrice)
                                        putDouble("rating", it.productRating)
                                    }
                                }

                            analytics.logEvent(FirebaseAnalytics.Event.PURCHASE) {
                                param(FirebaseAnalytics.Param.ITEM_LIST_NAME, "Product")
                                param(FirebaseAnalytics.Param.CURRENCY, "IDR")
                                result.data.data?.total?.toDouble()
                                    ?.let { total -> param(FirebaseAnalytics.Param.VALUE, total) }
                                param(FirebaseAnalytics.Param.ITEMS, itemProduct.toTypedArray())
                            }

                            binding.progressBar.visibility = View.GONE
                            val bundle = bundleOf("data" to result.data.data, "status" to true)

                            navController.navigate(
                                R.id.action_checkoutFragment_to_statusFragment,
                                bundle
                            )
                        }

                        is Result.Error -> {
                            binding.progressBar.visibility = View.GONE
                            binding.btnBuy.visibility = View.VISIBLE
                            val t = result.error.errorResponse()

                            Snackbar.make(binding.root, t.toString(), Snackbar.LENGTH_LONG)
                                .setBackgroundTint(errorColor(requireContext()))
                                .show()
                        }
                    }
                }
            }
        }
    }

    private fun initView() {
        val bundle = arguments
        if (bundle != null) {
            val myList = bundle.getParcelableArrayList<CartEntity>("data")

            myList?.forEach {
                checkoutData.add(it.asCheckout())
            }
            binding.rvCheckout.layoutManager = LinearLayoutManager(requireContext())
            adapter = CheckoutProductAdapter(this)
            binding.rvCheckout.adapter = adapter
            binding.rvCheckout.itemAnimator?.changeDuration = 0
            setSum(checkoutData)
            adapter.submitList(checkoutData)
        }
    }

    private fun initEvent() {
        binding.topAppBar.setNavigationOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.payment.setOnClickListener {
            val paymentFragment = ChoosePaymentFragment()
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(android.R.id.content, paymentFragment)
                .addToBackStack(null)
            transaction.commit()
        }
    }

    private fun paymentListener() {
        requireActivity().supportFragmentManager.setFragmentResultListener(
            "payment",
            this
        ) { _, bundle ->

            val value = bundle.getParcelable<ItemPayment>("data")
            if (value != null) {
                paymentName = value.label
                binding.apply {
                    btnBuy.isEnabled = true
                    label.text = value.label
                    Glide.with(requireContext())
                        .load(value.image)
                        .into(icon)
                }
            }
        }
    }

    private fun setSum(myList: ArrayList<Checkout>?) {
        var num = 0
        myList?.forEach {
            num += it.productPrice * it.quantity
        }
        binding.price.text = num.toString().toCurrencyFormat()
    }

    private fun newList(data: Checkout): List<Checkout> {
        return adapter.currentList.map {
            if (it.productId == data.productId) {
                it.quantity = data.quantity
            }
            it
        }
    }

    override fun onMinus(data: Checkout) {
        val list = newList(data)
        setSum(ArrayList(list))
    }

    override fun onPlus(data: Checkout?) {
        if (data != null) {
            val list = newList(data)
            setSum(ArrayList(list))
        } else {
            Snackbar.make(
                binding.root,
                getString(R.string.stock_not_available),
                Snackbar.LENGTH_LONG
            )
                .setBackgroundTint(errorColor(requireContext()))
                .show()
        }
    }

    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        Log.d("TAG", "onItemClick: ")
    }
}
