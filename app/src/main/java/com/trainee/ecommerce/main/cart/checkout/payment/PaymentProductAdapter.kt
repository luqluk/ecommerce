package com.trainee.ecommerce.main.cart.checkout.payment

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.commons.base.BaseAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.remote.response.ItemPayment
import com.trainee.ecommerce.core.data.remote.response.Payment
import com.trainee.ecommerce.databinding.ListItemPaymentBinding
import com.trainee.ecommerce.databinding.TitleItemPaymentBinding

class PaymentProductAdapter(
    private val onClick: (ItemPayment) -> Unit
) :
    BaseAdapter<Payment, TitleItemPaymentBinding>(TitleItemPaymentBinding::inflate) {

    override fun onItemBind(): (Payment, TitleItemPaymentBinding, View) -> Unit {
        return { data, binding, itemView ->
            binding.apply {
                productName.text = data.title
                val childAdapter = ChildAdapter(onClick)
                childAdapter.submitList(data.item)
                childRv.layoutManager =
                    LinearLayoutManager(itemView.context)
                childRv.adapter = childAdapter
            }
        }
    }
}

class ChildAdapter(private val onClick: (ItemPayment) -> Unit) :
    BaseAdapter<ItemPayment, ListItemPaymentBinding>(ListItemPaymentBinding::inflate) {

    override fun onItemBind(): (ItemPayment, ListItemPaymentBinding, View) -> Unit {
        return { data, binding, itemView ->
            binding.apply {
                Glide.with(itemView.context)
                    .load(data.image)
                    .placeholder(R.drawable.img_thumbnail)
                    .transform(RoundedCorners(roundingRadius))
                    .into(icon)

                productName.text = data.label

                if (!data.status) {
                    content.apply {
                        alpha = alphaFalse
                        isEnabled = false
                    }
                }

                itemView.setOnClickListener {
                    onClick(data)
                }
            }
        }
    }

    companion object {
        private const val roundingRadius = 8
        private const val alphaFalse = 0.5f
    }
}
