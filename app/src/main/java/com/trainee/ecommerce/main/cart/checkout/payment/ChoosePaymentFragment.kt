package com.trainee.ecommerce.main.cart.checkout.payment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.remote.response.PaymentResponse
import com.trainee.ecommerce.databinding.FragmentChoosePaymentBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ChoosePaymentFragment : DialogFragment() {

    private var _binding: FragmentChoosePaymentBinding? = null
    private val binding get() = _binding!!

    private lateinit var data: PaymentResponse
    private lateinit var parentAdapter: PaymentProductAdapter

    @Inject
    lateinit var analytics: FirebaseAnalytics

    @Inject
    lateinit var remoteConfig: FirebaseRemoteConfig

    private val viewModel: PaymentViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChoosePaymentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.topAppBar.setNavigationOnClickListener {
            dismiss()
        }

        binding.progressBar.visibility = View.VISIBLE
        binding.rvPayment.layoutManager = LinearLayoutManager(requireContext())
        binding.rvPayment.itemAnimator?.changeDuration = 0
        parentAdapter = PaymentProductAdapter {
            val itemPayment = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.label)
                putString("image", it.image)
                putBoolean("status", it.status)
            }

            analytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO) {
                param(FirebaseAnalytics.Param.ITEMS, itemPayment)
            }

            requireActivity().supportFragmentManager.setFragmentResult(
                "payment",
                bundleOf(
                    "data" to it
                )
            )
            dismiss()
        }

        binding.rvPayment.adapter = parentAdapter

        observeData()
    }

    private fun observeData() {
        viewModel.paymentUpdate.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }

                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE

                    parentAdapter.submitList(data.data)
                }

                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(
                        requireContext(),
                        result.error.message,
                        Toast.LENGTH_SHORT,
                    ).show()
                }
            }
        }

        viewModel.payment.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }

                is Result.Success -> {
                    binding.progressBar.visibility = View.GONE

                    parentAdapter.submitList(result.data.data)
                }

                is Result.Error -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(
                        requireContext(),
                        result.error.message,
                        Toast.LENGTH_SHORT,
                    ).show()
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback = object : OnBackPressedCallback(
            true
        ) {
            override fun handleOnBackPressed() {
                dismiss()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }
}
