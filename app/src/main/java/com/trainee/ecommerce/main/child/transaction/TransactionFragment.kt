package com.trainee.ecommerce.main.child.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.util.Mapping.asPaymentData
import com.trainee.ecommerce.core.util.Util.errorResponse
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.databinding.FragmentTransactionBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionFragment : Fragment() {

    private var _binding: FragmentTransactionBinding? = null
    private val binding get() = _binding!!

    private val viewModel: TransactionViewModel by viewModels()
    private lateinit var adapter: TransactionAdapter

    private val navHostFragment: NavHostFragment by lazy {
        requireActivity().supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
    }

    private val navController by lazy {
        navHostFragment.navController
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTransactionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@TransactionFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            onBackPressedCallback
        )

        initView()
        observeData()
    }

    private fun initView() {
        binding.rvTransaction.layoutManager = LinearLayoutManager(requireContext())
        binding.rvTransaction.itemAnimator?.changeDuration = 0
        adapter = TransactionAdapter {
            val transaction = it.asPaymentData()

            val bundle = bundleOf(
                "data" to transaction,
                "rating" to it.rating,
                "review" to it.review
            )

            navController.navigate(R.id.action_mainFragment_to_statusFragment, bundle)
        }
        binding.rvTransaction.adapter = adapter
    }

    private fun observeData() {
        viewModel.transaction.observe(viewLifecycleOwner) { result ->
            if (result != null) {
                when (result) {
                    is Result.Loading -> {
                        binding.errorLayout.hide()
                        binding.progressBar.visibility = View.VISIBLE
                    }

                    is Result.Success -> {
                        binding.errorLayout.hide()
                        binding.progressBar.hide()
                        binding.rvTransaction.show()
                        adapter.submitList(result.data.data)

                        if (result.data.data.isEmpty()) {
                            binding.errorLayout.show()
                            binding.descriptionError.text = getString(R.string.empty_transaction)
                        } else {
                            binding.errorLayout.hide()
                        }
                    }

                    is Result.Error -> {
                        onError(result.error.errorResponse().toString())
                        binding.errorLayout.show()
                        binding.rvTransaction.hide()
                        binding.progressBar.hide()
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.transaction()
    }

    private fun onError(message: String) {
        binding.titleError.text = getString(R.string.empty)
        binding.descriptionError.text =
            message
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
