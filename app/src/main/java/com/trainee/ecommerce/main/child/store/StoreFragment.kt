package com.trainee.ecommerce.main.child.store

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.paging.LoadState
import androidx.paging.map
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.window.layout.WindowMetricsCalculator
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.util.NetworkConstant.CODE_INTERNAL_SEVER_ERROR
import com.trainee.ecommerce.core.util.NetworkConstant.CODE_NOT_FOUND
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.core.util.Util.toCurrencyFormat
import com.trainee.ecommerce.databinding.FragmentStoreBinding
import com.trainee.ecommerce.main.child.store.detail.DetailStoreFragmentComposeDirections
import com.trainee.ecommerce.main.child.store.filter.FilterStoreBottomSheet
import com.trainee.ecommerce.main.child.store.search.SearchFragment
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

enum class WindowSizeClass { COMPACT, MEDIUM, EXPANDED }

@AndroidEntryPoint
class StoreFragment : Fragment() {

    private var _binding: FragmentStoreBinding? = null
    private val binding get() = _binding!!

    private val viewModel: StoreViewModel by activityViewModels()
    private lateinit var adapter: ListProductAdapter
    private var isList: Boolean = true

    private var search: String? = null

    private lateinit var gridLayoutManager: GridLayoutManager

    private lateinit var footerAdapter: LoadingStateAdapter
    private var grid = 1
    private var param1: String? = null
    private var param2: String? = null
    private var param3: String? = null
    private var param4: String? = null

    @Inject
    lateinit var pref: SharedPreferences

    @Inject
    lateinit var analytics: FirebaseAnalytics

    private val navHostFragment: NavHostFragment by lazy {
        requireActivity().supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
    }

    private val navController by lazy {
        navHostFragment.navController
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStoreBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@StoreFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            onBackPressedCallback
        )

        initView()
        initEvent()
        collectChip()
        collectUiState()
        searchListener()

        binding.search.setOnClickListener {
            val searchFragment = SearchFragment.newInstance(viewModel.param.value?.search)
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(android.R.id.content, searchFragment)
                .addToBackStack(null)
            transaction.commit()
        }
        binding.buttonError.setOnClickListener {
            if (binding.buttonError.text == getString(R.string.refresh)) {
                adapter.refresh()
            } else {
                binding.chipGroupFilter.removeAllViews()
                binding.search.text?.clear()

                viewModel.reset()
            }
        }

        binding.swipe.setOnRefreshListener {
            binding.swipe.isRefreshing = false
            adapter.refresh()
        }

        binding.chipFilter.setOnClickListener {
            val modalBottomSheet = FilterStoreBottomSheet()
            modalBottomSheet.show(childFragmentManager, FilterStoreBottomSheet.TAG)
        }

        binding.layoutButton.setOnClickListener {
            val icon = if (isList) {
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_grid)
            } else ContextCompat.getDrawable(requireContext(), R.drawable.ic_list)
            binding.layoutButton.setImageDrawable(icon)

            val span = if (isList) grid else 1
            gridLayoutManager.spanCount = span
            adapter.toggleLayoutMode()

            viewModel.spanCount = span

            isList = !isList
            viewModel.isList = isList
        }
    }

    private fun initEvent() {
        footerAdapter = LoadingStateAdapter()
        val concatAdapter = adapter.withLoadStateFooter(
            footer = footerAdapter
        )

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if ((position == adapter.itemCount) && footerAdapter.itemCount > 0 && viewModel.spanCount == 2) {
                    medium
                } else if ((position == adapter.itemCount) && footerAdapter.itemCount > 0 &&
                    viewModel.spanCount == expanded
                ) {
                    expanded
                } else {
                    1
                }
            }
        }

        if (viewModel.spanCount == 1) {
            adapter.currentLayoutMode = ListProductAdapter.LayoutMode.LIST
        } else {
            adapter.currentLayoutMode = ListProductAdapter.LayoutMode.GRID
        }

        binding.rvProduct.adapter = concatAdapter

        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                binding.rvProduct.scrollToPosition(0)
            }

            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                binding.rvProduct.scrollToPosition(0)
            }

            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                binding.rvProduct.scrollToPosition(0)
            }

            override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                binding.rvProduct.scrollToPosition(0)
            }

            override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) {
                binding.rvProduct.scrollToPosition(0)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.param.value == null) {
            binding.chipGroupFilter.removeAllViews()
        }
    }

    private fun initView() {
        val metrics = WindowMetricsCalculator.getOrCreate()
            .computeCurrentWindowMetrics(requireActivity())

        val widthDp = metrics.bounds.width() / requireContext().resources.displayMetrics.density
        val widthWindowSizeClass = when {
            widthDp < compactDp -> WindowSizeClass.COMPACT
            widthDp < mediumDp -> WindowSizeClass.MEDIUM
            else -> WindowSizeClass.EXPANDED
        }

        grid = when (widthWindowSizeClass) {
            WindowSizeClass.COMPACT -> {
                2
            }

            WindowSizeClass.MEDIUM -> {
                2
            }

            WindowSizeClass.EXPANDED -> {
                expanded
            }
        }

        adapter = ListProductAdapter {
            val itemProduct = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, it.productId)
                putString("image", it.image)
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.productName)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, it.brand)
                putInt("sale", it.sale)
                putString("store", it.store)
                putInt(FirebaseAnalytics.Param.PRICE, it.productPrice)
                putDouble("rating", it.productRating)
            }

            analytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                param(FirebaseAnalytics.Param.ITEM_LIST_NAME, "Product")
                param(FirebaseAnalytics.Param.ITEMS, itemProduct)
            }
            navController.navigate(
                DetailStoreFragmentComposeDirections.mainToDetail(
                    it.productId,
                    0
                )
            )
        }
        isList = viewModel.isList
        viewModel.spanCount = if (isList) 1 else grid

        gridLayoutManager = GridLayoutManager(requireContext(), viewModel.spanCount)

        binding.rvProduct.layoutManager = gridLayoutManager

        if (viewModel.isList) {
            adapter.setLayout(ListProductAdapter.LayoutMode.LIST)
        } else {
            gridLayoutManager.spanCount = grid
            adapter.setLayout(ListProductAdapter.LayoutMode.GRID)
        }

        val icon =
            if (!viewModel.isList) {
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_grid)
            } else ContextCompat.getDrawable(requireContext(), R.drawable.ic_list)
        binding.layoutButton.setImageDrawable(icon)
    }

    private fun collectUiState() {
        viewModel.product.observe(viewLifecycleOwner) { product ->

            adapter.submitData(viewLifecycleOwner.lifecycle, product)

            product.map {
                val data = it

                val itemFilter = Bundle().apply {
                    putString(FirebaseAnalytics.Param.ITEM_ID, data.productId)
                    putString("image", data.image)
                    putString(FirebaseAnalytics.Param.ITEM_NAME, data.productName)
                    putString(FirebaseAnalytics.Param.ITEM_BRAND, data.brand)
                    putInt("sale", data.sale)
                    putString("store", data.store)
                    putInt(FirebaseAnalytics.Param.PRICE, data.productPrice)
                    putDouble("rating", data.productRating)
                }

                analytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST) {
                    param(FirebaseAnalytics.Param.ITEM_LIST_ID, "filter")
                    param(FirebaseAnalytics.Param.ITEM_LIST_NAME, "filter list")
                    param(FirebaseAnalytics.Param.ITEMS, itemFilter)
                }
            }
        }

        adapter.addLoadStateListener { loadState ->
            when (val state = loadState.source.refresh) {
                is LoadState.NotLoading -> {
                    load(false)
                    binding.filter.show()
                    binding.errorLayout.hide()
                }

                is LoadState.Loading -> {
                    load(true)
                    binding.filter.hide()
                    binding.errorLayout.hide()
                }

                is LoadState.Error -> {
                    load(false)
                    onError(state.error)
                    binding.rvProduct.hide()
                }
            }
        }
    }

    private fun collectChip() {
        viewModel.paramChip.observe(viewLifecycleOwner) { data ->
            binding.chipGroupFilter.removeAllViews()

            val sort = data?.sort?.let { getString(it) }
            val brand = data?.brand?.let { getString(it) }

            addChip(sort, viewModel.param1Checked).setOnClickListener {
                viewModel.param1Checked = !viewModel.param1Checked
                val value: String? = if (viewModel.param.value?.sort != null) null else sort

                viewModel.setQuery(
                    search = viewModel.param.value?.search,
                    sort = value,
                    brand = viewModel.param.value?.brand,
                    lowest = viewModel.param.value?.lowest,
                    highest = viewModel.param.value?.highest
                )
            }

            addChip(brand, viewModel.param2Checked).setOnClickListener {
                viewModel.param2Checked = !viewModel.param2Checked
                val value: String? = if (viewModel.param.value?.brand != null) null else brand

                viewModel.setQuery(
                    search = viewModel.param.value?.search,
                    brand = value,
                    sort = viewModel.param.value?.sort,
                    lowest = viewModel.param.value?.lowest,
                    highest = viewModel.param.value?.highest
                )
            }
            if (data?.lowest != null) {
                addChip("< ${data.lowest.toString().toCurrencyFormat()}", viewModel.param3Checked)
                    .setOnClickListener {
                        viewModel.param3Checked = !viewModel.param3Checked
                        val value: String? =
                            if (viewModel.param.value?.lowest != null) null else data.lowest.toString()

                        viewModel.setQuery(
                            search = viewModel.param.value?.search,
                            brand = viewModel.param.value?.brand,
                            sort = viewModel.param.value?.sort,
                            lowest = value?.toIntOrNull(),
                            highest = viewModel.param.value?.highest
                        )
                    }
            }
            if (data?.highest != null) {
                addChip("> ${data.highest.toString().toCurrencyFormat()}", viewModel.param4Checked)
                    .setOnClickListener {
                        viewModel.param4Checked = !viewModel.param4Checked
                        val value: String? =
                            if (viewModel.param.value?.highest != null) null else data.highest.toString()

                        viewModel.setQuery(
                            search = viewModel.param.value?.search,
                            brand = viewModel.param.value?.brand,
                            sort = viewModel.param.value?.sort,
                            lowest = viewModel.param.value?.lowest,
                            highest = value?.toIntOrNull()
                        )
                    }
            }
        }
    }

    private fun searchListener() {
        requireActivity().supportFragmentManager.setFragmentResultListener(
            "searchKey",
            this
        ) { _, bundle ->

            val value = bundle.getString("search")
            search = value

            binding.search.setText(value)
            viewModel.setQuery(
                search,
                viewModel.param.value?.brand,
                viewModel.param.value?.lowest,
                viewModel.param.value?.highest,
                viewModel.param.value?.sort,
            )
        }

        requireActivity().supportFragmentManager.setFragmentResultListener(
            "filterKey",
            this
        ) { _, bundle ->

            val sort = bundle.getString("sort")
            Log.d("sort", sort.toString())
            param1 = sort
            param2 = bundle.getString("brand")
            param3 = bundle.getString("low")
            param4 = bundle.getString("high")
        }
    }

    private fun onError(t: Throwable) {
        binding.errorLayout.show()
        binding.filter.hide()

        if (t is HttpException) {
            if (t.code() == CODE_NOT_FOUND) {
                binding.titleError.text = getString(R.string.empty)
                binding.descriptionError.text = getString(R.string.empty_desc)
                binding.buttonError.text = getString(R.string.reset)
            } else if (t.code() == CODE_INTERNAL_SEVER_ERROR) {
                binding.titleError.text = t.code().toString()
                binding.descriptionError.text =
                    getString(R.string.internal_error_desc)
                binding.buttonError.text = getString(R.string.refresh)
            }
        } else if (t is IOException) {
            binding.titleError.text = getString(R.string.connection)
            binding.descriptionError.text = getString(R.string.connection_desc)
            binding.buttonError.text = getString(R.string.refresh)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun addChip(text: String?, isChecked: Boolean): Chip {
        val chip = Chip(requireContext())
        if (text != "" && text != null) {
            chip.text = text
            chip.isChecked = isChecked
            binding.chipGroupFilter.addView(chip)
        }
        return chip
    }

    private fun load(isLoading: Boolean) {
        binding.apply {
            val layout = if (isList) list else grid

            layout.isVisible = isLoading

            rvProduct.isGone = isLoading
        }
    }

    companion object {
        const val expanded = 4
        const val medium = 2
        const val compactDp = 600f
        const val mediumDp = 840f
    }
}
