package com.trainee.ecommerce.main.cart

import android.util.TypedValue
import android.view.View
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import com.android.commons.base.BaseAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.android.material.R.attr
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.util.Util.errorColor
import com.trainee.ecommerce.core.util.Util.toCurrencyFormat
import com.trainee.ecommerce.databinding.ListItemCartBinding

class CartProductAdapter(private val listener: OnItemClickListener) :
    BaseAdapter<CartEntity, ListItemCartBinding>(ListItemCartBinding::inflate) {

    interface OnItemClickListener : AdapterView.OnItemClickListener {
        fun onDeleteClicked(data: CartEntity)
        fun onCheckboxChanges(data: CartEntity)
        fun onMinus(data: CartEntity)
        fun onPlus(data: CartEntity)
        fun onItemClick(data: CartEntity)
    }

    companion object {

        private const val roundingRadius = 8
        private const val remainingStock = 10
    }

    override fun onItemBind(): (CartEntity, ListItemCartBinding, View) -> Unit {
        return { data, binding, itemView ->
            binding.apply {
                Glide.with(itemView.context).load(data.image).placeholder(R.drawable.img_thumbnail)
                    .transform(RoundedCorners(roundingRadius)).into(icon)

                productName.text = data.productName
                spec.text = data.productVariant
                val dataPrice = data.productPrice.toString()
                price.text = dataPrice.toCurrencyFormat()
                checkCart.isChecked = data.isSelected
                num.text = data.quantity.toString()

                val typedValueNormal = TypedValue()
                itemView.context.theme.resolveAttribute(attr.colorOnSurface, typedValueNormal, true)

                val color = if (data.stock < remainingStock) {
                    errorColor(itemView.context)
                } else {
                    ContextCompat.getColor(itemView.context, typedValueNormal.resourceId)
                }

                val textStock = if (data.stock < remainingStock) {
                    itemView.context.getString(R.string.low_stock, data.stock.toString())
                } else {
                    itemView.context.getString(R.string.stock_available, data.stock.toString())
                }

                stock.text = textStock
                stock.setTextColor(color)

                binding.checkCart.setOnClickListener {
                    val product = data.copy(
                        isSelected = checkCart.isChecked
                    )
                    listener.onCheckboxChanges(product)
                }

                itemView.setOnClickListener {
                    listener.onItemClick(data)
                }
                var number = data.quantity

                btnPlus.setOnClickListener {
                    val dataPlus = data.copy(
                        quantity = number + 1
                    )

                    listener.onPlus(dataPlus)
                }

                btnMinus.setOnClickListener {
                    if (number > 1) {
                        number -= 1
                        val dataMinus = data.copy(
                            quantity = number
                        )
                        listener.onMinus(dataMinus)
                    }
                }

                binding.btnDelete.setOnClickListener {
                    listener.onDeleteClicked(data)
                }
            }
        }
    }
}
