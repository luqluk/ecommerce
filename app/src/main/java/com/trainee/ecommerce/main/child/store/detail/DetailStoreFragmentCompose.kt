package com.trainee.ecommerce.main.child.store.detail

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.Share
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.InputChip
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import coil.compose.AsyncImage
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.local.WishlistEntity
import com.trainee.ecommerce.core.data.model.DataDetail
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.data.remote.response.DetailProduct
import com.trainee.ecommerce.core.data.remote.response.DetailResponse
import com.trainee.ecommerce.core.data.remote.response.ProductVariant
import com.trainee.ecommerce.core.util.Event
import com.trainee.ecommerce.core.util.Mapping.asCartEntity
import com.trainee.ecommerce.core.util.Mapping.asWishlistEntity
import com.trainee.ecommerce.core.util.Util.toCurrencyFormat
import com.trainee.ecommerce.main.child.store.detail.ui.theme.ecommerceTheme
import com.trainee.ecommerce.main.child.store.detail.ui.theme.poppins
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Suppress("FunctionNaming")
@AndroidEntryPoint
class DetailStoreFragmentCompose : Fragment() {

    private val viewModel: DetailViewModel by viewModels()
    private lateinit var data: DetailProduct
    private lateinit var cart: List<CartEntity>
    private lateinit var wishlist: List<WishlistEntity>
    private var isFavorite: Boolean = false
    private lateinit var productId: String
    private var checkedId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        productId = arguments?.getString("id").toString()
        checkedId = arguments?.getInt("type") ?: checkedId
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                ecommerceTheme {
                    MyApp()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("DetailStoreFragmentCompose", "onResume: ${viewModel.checkedId} ")
    }

    @OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
    @Composable
    fun MyApp() {
        val detailProduct by viewModel.detailProduct.observeAsState(initial = Result.Loading)
        val checkedId = rememberSaveable { mutableIntStateOf(checkedId) }
        val getAllCart by viewModel.getAllCart().observeAsState()
        val getAllWishlist by viewModel.getAllWishlist().observeAsState()
        val insert by viewModel.insert.observeAsState()
        val insertWishlist by viewModel.insertWishlist.observeAsState()
        val deleteWishlist by viewModel.delete.observeAsState()
        val snackBarHostState = remember { SnackbarHostState() }
        val scope = rememberCoroutineScope()
        val windowSize = calculateWindowSizeClass(activity = requireActivity())

        MyApp(
            modifier = Modifier.fillMaxSize(),
            detailProduct,
            checkedId,
            getAllCart,
            getAllWishlist,
            insert,
            insertWishlist,
            deleteWishlist,
            snackBarHostState,
            scope,
            windowSize.widthSizeClass

        )
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun MyApp(
        modifier: Modifier = Modifier,
        detailProduct: Result<DetailResponse>?,
        checkedId: MutableState<Int>,
        getAllCart: List<CartEntity>?,
        getAllWishlist: List<WishlistEntity>?,
        insert: Result<Event<String>>?,
        insertWishlist: Result<Event<String>>?,
        deleteWishlist: Result<Event<String>>?,
        snackBarHostState: SnackbarHostState,
        scope: CoroutineScope,
        windowSizeClass: WindowWidthSizeClass
    ) {
        val price = rememberSaveable { mutableIntStateOf(0) }
        val isSuccess = remember { mutableStateOf(false) }

        val onBack = {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        Surface(modifier) {
            Scaffold(snackbarHost = {
                SnackbarHost(snackBarHostState) { data ->
                    val isError = (data.visuals as? SnackbarVisualsWithError)?.isError ?: false
                    val color = if (isError) {
                        MaterialTheme.colorScheme.error
                    } else {
                        MaterialTheme.colorScheme.primary
                    }

                    Snackbar(
                        containerColor = color,
                        modifier = Modifier.padding(12.dp),
                    ) {
                        Text(
                            text = data.visuals.message,
                            style = TextStyle(
                                fontFamily = poppins
                            )
                        )
                    }
                }
            }, topBar = {
                Column {
                    TopAppBar(modifier = Modifier.drawBehind {}, title = {
                        Text(
                            stringResource(id = R.string.product_detail),
                            fontFamily = poppins,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        )
                    }, navigationIcon = {
                        IconButton(onClick = onBack) {
                            Icon(
                                imageVector = Icons.Filled.ArrowBack,
                                contentDescription = "Localized Description"
                            )
                        }
                    }, actions = {})
                    Divider()
                }
            }, bottomBar = {
                when (windowSizeClass) {
                    WindowWidthSizeClass.Compact -> {
                        BottomBar(
                            price,
                            snackBarHostState,
                            scope,
                            isSuccess,
                            checkedId,
                            snackBarHostState
                        )
                    }

                    WindowWidthSizeClass.Medium -> {
                        BottomBarLandscape(
                            price,
                            snackBarHostState,
                            scope,
                            isSuccess,
                            checkedId,
                            snackBarHostState
                        )
                    }

                    WindowWidthSizeClass.Expanded -> {
                        BottomBarLandscape(
                            price,
                            snackBarHostState,
                            scope,
                            isSuccess,
                            checkedId,
                            snackBarHostState
                        )
                    }
                }
            }) { innerPadding ->
                getAllWishlist?.let { list ->
                    wishlist = list
                    isFavorite = wishlist.any { it.productId == productId }
                }

                if (detailProduct != null) {
                    when (detailProduct) {
                        is Result.Loading -> {
                            OnLoading()
                        }

                        is Result.Success -> {
                            isSuccess.value = true
                            data = detailProduct.data.data
                            price.intValue = data.productPrice

                            when (windowSizeClass) {
                                WindowWidthSizeClass.Compact -> {
                                    Column(
                                        Modifier
                                            .padding(innerPadding)
                                            .fillMaxSize()
                                            .verticalScroll(rememberScrollState()),
                                        verticalArrangement = Arrangement.spacedBy(8.dp)
                                    ) {
                                        OnSuccess(price, checkedId, snackBarHostState)
                                    }
                                }

                                WindowWidthSizeClass.Medium -> {
                                    Column(
                                        Modifier
                                            .padding(innerPadding)
                                            .fillMaxSize()
                                            .verticalScroll(rememberScrollState()),
                                        verticalArrangement = Arrangement.spacedBy(8.dp)
                                    ) {
                                        OnSuccess(price, checkedId, snackBarHostState)
                                    }
                                }

                                WindowWidthSizeClass.Expanded -> {
                                    Column(
                                        Modifier
                                            .padding(innerPadding)
                                            .fillMaxSize(),
                                        verticalArrangement = Arrangement.spacedBy(8.dp)
                                    ) {
                                        OnSuccessLandscape(price, checkedId, snackBarHostState)
                                    }
                                }
                            }
                        }

                        is Result.Error -> {
                            OnError(innerPadding)
                        }
                    }
                }

                getAllCart?.let {
                    cart = it
                }

                insert?.let { result ->
                    when (result) {
                        is Result.Loading -> {}

                        is Result.Success -> {
                            result.data.getContentIfNotHandled()?.let { message ->
                                scope.launch {
                                    snackBarHostState.showSnackbar(
                                        message = message,
                                        duration = SnackbarDuration.Short,
                                    )
                                }
                            }
                        }

                        is Result.Error -> {}
                    }
                }

                insertWishlist?.let { result ->
                    when (result) {
                        is Result.Loading -> {}

                        is Result.Success -> {
                            result.data.getContentIfNotHandled()?.let { message ->
                                scope.launch {
                                    snackBarHostState.showSnackbar(
                                        SnackbarVisualsWithError(
                                            message,
                                            false
                                        )
                                    )
                                }
                            }
                        }

                        is Result.Error -> {}
                    }
                }

                deleteWishlist?.let { result ->
                    when (result) {
                        is Result.Loading -> {}

                        is Result.Success -> {
                            result.data.getContentIfNotHandled()?.let { message ->
                                scope.launch {
                                    snackBarHostState.showSnackbar(
                                        SnackbarVisualsWithError(
                                            message,
                                            true
                                        )
                                    )
                                }
                            }
                        }

                        is Result.Error -> {}
                    }
                }
            }
        }
    }

    @Composable
    fun OnSuccess(
        price: MutableState<Int>,
        checkedId: MutableState<Int>,
        snackBarHostState: SnackbarHostState
    ) {
        ImagePager()
        ProductDetail(price, checkedId, snackBarHostState)
        Variant(price, checkedId)
        Description()
        Review()
    }

    @Composable
    fun OnSuccessLandscape(
        price: MutableState<Int>,
        checkedId: MutableState<Int>,
        snackBarHostState: SnackbarHostState
    ) {
        Row {
            ImagePagerLandscape()
            Column(
                Modifier.verticalScroll(rememberScrollState())
            ) {
                ProductDetail(price, checkedId, snackBarHostState)
                Variant(price, checkedId)
                Description()
                Review()
            }
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    private fun ImagePager() {
        val pageCount = data.image.size
        val pagerState = rememberPagerState(pageCount = {
            pageCount
        })

        Box {
            HorizontalPager(state = pagerState) { page ->
                AsyncImage(
                    model = data.image[page],
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    placeholder = painterResource(id = R.drawable.img_thumbnail),
                    error = painterResource(id = R.drawable.img_thumbnail),
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .aspectRatio(1F)
                )
            }
            Column(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(8.dp)
            ) {
                Indicator(
                    pagerState = pagerState,
                    itemCount = pageCount,
                )
            }
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    private fun ImagePagerLandscape() {
        val pageCount = data.image.size
        val pagerState = rememberPagerState(pageCount = {
            pageCount
        })

        Box(
            Modifier
                .padding(16.dp)
                .fillMaxHeight(1f)
                .aspectRatio(1f)
        ) {
            HorizontalPager(state = pagerState) { page ->
                AsyncImage(
                    model = data.image[page],
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    placeholder = painterResource(id = R.drawable.img_thumbnail),
                    error = painterResource(id = R.drawable.img_thumbnail),
                    modifier = Modifier
                        .fillMaxHeight(1f)
                        .aspectRatio(1F)
                )
            }
            Column(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(8.dp)
            ) {
                Indicator(
                    pagerState = pagerState,
                    itemCount = pageCount,
                )
            }
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun Indicator(
        pagerState: PagerState,
        itemCount: Int,
    ) {
        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            repeat(itemCount) { iteration ->
                val color =
                    if (pagerState.currentPage == iteration) MaterialTheme.colorScheme.primary else Color.LightGray
                Box(
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .clip(CircleShape)
                        .background(color)
                        .size(8.dp)

                )
            }
        }
    }

    @Composable
    fun ProductDetail(
        price: MutableState<Int>,
        checkedId: MutableState<Int>,
        snackBarHostState: SnackbarHostState
    ) {
        Column(
            Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
        ) {
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                val priceSum =
                    data.productPrice.plus(data.productVariant[checkedId.value].variantPrice)
                Text(
                    text = priceSum.toString().toCurrencyFormat(),
                    style = TextStyle(
                        fontFamily = poppins,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 20.sp,
                    ),
                    modifier = Modifier.weight(1f)
                )
                var shareEnabled by remember { mutableStateOf(true) }
                val launcher =
                    rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                        // you can use the ActivityResult(it) here
                        shareEnabled = true
                    }
                IconButton(onClick = {
                    val shareIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(
                            Intent.EXTRA_TEXT,
                            "Product : ${this@DetailStoreFragmentCompose.data.productName}\n" + "Price : ${
                                price.value.toString().toCurrencyFormat()
                            }\n" + "Link : http://com.trainee.ecommerce/product/$productId/${checkedId.value}"
                        )
                        type = "text/plain"
                    }

                    Intent.createChooser(shareIntent, null)

                    shareEnabled = false

                    launcher.launch(shareIntent)
                }) {
                    Icon(
                        imageVector = Icons.Filled.Share,
                        contentDescription = "Localized Description",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }

                var icon by remember {
                    mutableStateOf(
                        if (isFavorite) Icons.Filled.Favorite else Icons.Filled.FavoriteBorder
                    )
                }
                IconButton(
                    onClick = {
                        val productVariant = ProductVariant(
                            variantPrice = if (price.value == 0) data.productPrice else price.value,
                            variantName = data.productVariant[checkedId.value].variantName
                        )

                        val dataProduct = data.copy(
                            productVariant = listOf(productVariant)
                        )

                        if (isFavorite) {
                            icon = Icons.Outlined.FavoriteBorder
                            viewModel.deleteWishlist(dataProduct.asWishlistEntity())
                        } else {
                            viewModel.insertWishlist(dataProduct)
                            icon = Icons.Filled.Favorite
                        }
                        snackBarHostState.currentSnackbarData?.dismiss()

                        isFavorite = !isFavorite
                    },
                    modifier = Modifier.padding(0.dp)
                ) {
                    Icon(
                        imageVector = icon,
                        contentDescription = "Localized Description",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }
            }

            Text(
                text = data.productName,
                style = TextStyle(
                    fontFamily = poppins,
                    fontSize = 14.sp,
                )
            )
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = stringResource(id = R.string.sales_detail, data.sale),
                    style = TextStyle(
                        fontFamily = poppins,
                        fontSize = 12.sp,
                        platformStyle = PlatformTextStyle(includeFontPadding = false)
                    ),
                )
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .padding(start = 8.dp)
                        .clip(RoundedCornerShape(4.dp))
                        .border(1.dp, Color.LightGray, RoundedCornerShape(4.dp))
                        .padding(horizontal = 8.dp, vertical = 2.dp)
                ) {
                    Icon(
                        imageVector = Icons.Filled.Star,
                        contentDescription = "Localized Description",
                        Modifier.size(15.dp)
                    )
                    Text(
                        text = stringResource(
                            id = R.string.rating,
                            data.productRating,
                            data.totalRating
                        ),
                        modifier = Modifier.padding(start = 4.dp),
                        style = TextStyle(
                            fontFamily = poppins,
                            fontSize = 12.sp,
                            platformStyle = PlatformTextStyle(includeFontPadding = false)
                        )
                    )
                }
            }
        }
        Divider()
    }

    @OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
    @Composable
    fun Variant(price: MutableState<Int>, checkedId: MutableState<Int>) {
        Column(
            Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
        ) {
            Text(
                text = stringResource(id = R.string.choose_variant),
                style = TextStyle(
                    fontFamily = poppins,
                    fontWeight = FontWeight.Medium,
                    fontSize = 16.sp,
                )
            )

            FlowRow(
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start),
            ) {
                data.productVariant.forEachIndexed { index, productVariant ->
                    InputChip(
                        onClick = {
                            checkedId.value = index
                            this@DetailStoreFragmentCompose.checkedId = index
                            price.value =
                                data.productPrice.plus(data.productVariant[index].variantPrice)
                        },
                        selected = checkedId.value == index,
                        label = { Text(productVariant.variantName) }
                    )
                }
            }
        }
        Divider()
    }

    @Composable
    fun Description() {
        Column(
            Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
        ) {
            Text(
                text = stringResource(id = R.string.text_description_product),
                style = TextStyle(
                    fontFamily = poppins,
                    fontWeight = FontWeight.Medium,
                    fontSize = 16.sp,
                )
            )
            Text(
                text = data.description,
                modifier = Modifier.padding(top = 8.dp),
                style = TextStyle(
                    fontFamily = poppins,
                    fontSize = 14.sp,
                )
            )
        }
        Divider()
    }

    @Composable
    fun Review() {
        Column(
            Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = stringResource(id = R.string.buyer_review),
                    style = TextStyle(
                        fontFamily = poppins,
                        fontWeight = FontWeight.Medium,
                        fontSize = 16.sp,
                    ),
                    modifier = Modifier.weight(1f)
                )

                TextButton(onClick = {
                    findNavController().navigate(
                        DetailStoreFragmentComposeDirections.detailToReview(
                            data.productId
                        )
                    )
                }) {
                    Text(
                        text = stringResource(id = R.string.see_all),
                        style = TextStyle(
                            fontFamily = poppins,
                            fontWeight = FontWeight.Medium,
                            fontSize = 12.sp
                        )
                    )
                }
            }
            Row(
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Icon(
                        imageVector = Icons.Filled.Star,
                        contentDescription = "Localized Description",
                        Modifier.size(24.dp)
                    )
                    Text(
                        text = data.productRating.toString(),
                        modifier = Modifier.padding(start = 4.dp),
                        style = TextStyle(
                            fontFamily = poppins,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 20.sp,
                            platformStyle = PlatformTextStyle(includeFontPadding = false)
                        )
                    )
                    Text(
                        text = "/5.0",
                        modifier = Modifier
                            .padding(bottom = 4.dp)
                            .align(Alignment.Bottom),
                        style = TextStyle(
                            fontFamily = poppins,
                            fontSize = 10.sp,
                            platformStyle = PlatformTextStyle(includeFontPadding = false)
                        )
                    )
                }

                Column(
                    modifier = Modifier.padding(start = 16.dp),
                ) {
                    Text(
                        text = stringResource(id = R.string.satisfaction, data.totalSatisfaction),

                        style = TextStyle(
                            fontFamily = poppins,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 12.sp,
                            platformStyle = PlatformTextStyle(includeFontPadding = false)
                        )
                    )

                    Text(
                        text = stringResource(
                            id = R.string.rate_review,
                            data.totalRating,
                            data.totalReview
                        ),
                        style = TextStyle(
                            fontFamily = poppins,
                            fontSize = 12.sp,
                            platformStyle = PlatformTextStyle(includeFontPadding = false)
                        )
                    )
                }
            }
        }
    }

    @Composable
    fun OnLoading() {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    }

    @Composable
    fun BottomBar(
        price: MutableState<Int>,
        snackbarHostState: SnackbarHostState,
        scope: CoroutineScope,
        isSuccess: MutableState<Boolean>,
        checkedId: MutableState<Int>,
        snackBarHostState: SnackbarHostState
    ) {
        Column {
            if (isSuccess.value) {
                Divider()
                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                ) {
                    OutlinedButton(
                        onClick = {
                            val variant = data.productVariant[checkedId.value].variantPrice
                            val productVariant = ProductVariant(
                                variantPrice = data.productPrice.plus(variant),
                                variantName = data.productVariant[checkedId.value].variantName
                            )

                            val dataProduct = data.copy(
                                productVariant = listOf(productVariant),
                                quantity = 1
                            )
                            if (dataProduct.quantity > data.stock) {
                                scope.launch {
                                    snackbarHostState.showSnackbar(
                                        SnackbarVisualsWithError(
                                            requireContext().resources.getString(R.string.stock_not_available),
                                            true
                                        )
                                    )
                                }
                            } else {
                                findNavController().navigate(
                                    R.id.action_detailStoreFragment_to_checkoutFragment,
                                    Bundle().apply {
                                        putParcelableArrayList(
                                            "data",
                                            ArrayList(listOf(dataProduct.asCartEntity()))
                                        )
                                    }
                                )
                            }
                        },
                        modifier = Modifier.weight(1f)
                    ) {
                        Text(
                            stringResource(id = R.string.buy_now),
                            style = TextStyle(
                                fontFamily = poppins,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 14.sp
                            )
                        )
                    }
                    Spacer(modifier = Modifier.width(8.dp))
                    Button(
                        onClick = {
                            snackBarHostState.currentSnackbarData?.dismiss()

                            val productVariant = ProductVariant(
                                variantPrice = if (price.value == 0) data.productPrice else price.value,
                                variantName = data.productVariant[checkedId.value].variantName
                            )

                            val productExist = cart.find { it.productId == data.productId }

                            val quantity = productExist?.quantity?.plus(1) ?: 1

                            val dataProduct = data.copy(
                                productVariant = listOf(productVariant),
                                quantity = quantity
                            )

                            if (dataProduct.quantity > data.stock) {
                                scope.launch {
                                    snackbarHostState.showSnackbar(
                                        SnackbarVisualsWithError(
                                            requireContext().resources.getString(R.string.stock_not_available),
                                            true
                                        )
                                    )
                                }
                            } else {
                                if (productExist != null) {
                                    viewModel.update(listOf(dataProduct.asCartEntity()))
                                        .observe(viewLifecycleOwner) { result ->
                                            if (result != null) {
                                                when (result) {
                                                    is Result.Loading -> {}

                                                    is Result.Success -> {
                                                        result.data.getContentIfNotHandled()?.let {
                                                            scope.launch {
                                                                snackbarHostState.showSnackbar(
                                                                    SnackbarVisualsWithError(
                                                                        it,
                                                                        false
                                                                    )
                                                                )
                                                            }
                                                        }
                                                    }

                                                    is Result.Error -> {}
                                                }
                                            }
                                        }
                                } else {
                                    viewModel.insertCart(dataProduct.asCartEntity())
                                }
                            }
                        },
                        modifier = Modifier.weight(1f)
                    ) {
                        Text(
                            stringResource(id = R.string.plus_cart),
                            style = TextStyle(
                                fontFamily = poppins,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 14.sp
                            )
                        )
                    }
                }
            }
        }
    }

    @Composable
    fun BottomBarLandscape(
        price: MutableState<Int>,
        snackbarHostState: SnackbarHostState,
        scope: CoroutineScope,
        isSuccess: MutableState<Boolean>,
        checkedId: MutableState<Int>,
        snackBarHostState: SnackbarHostState
    ) {
        Column {
            if (isSuccess.value) {
                Divider()

                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                ) {
                    Spacer(modifier = Modifier.weight(2f))
                    OutlinedButton(
                        onClick = {
                            val variant = data.productVariant[checkedId.value].variantPrice
                            val productVariant = ProductVariant(
                                variantPrice = data.productPrice.plus(variant),
                                variantName = data.productVariant[checkedId.value].variantName
                            )

                            val dataProduct = data.copy(
                                productVariant = listOf(productVariant),
                                quantity = 1
                            )
                            if (dataProduct.quantity > data.stock) {
                                scope.launch {
                                    snackbarHostState.showSnackbar(
                                        SnackbarVisualsWithError(
                                            requireContext().resources.getString(R.string.stock_not_available),
                                            true
                                        )
                                    )
                                }
                            } else {
                                findNavController().navigate(
                                    R.id.action_detailStoreFragment_to_checkoutFragment,
                                    Bundle().apply {
                                        putParcelableArrayList(
                                            "data",
                                            ArrayList(listOf(dataProduct.asCartEntity()))
                                        )
                                    }
                                )
                            }
                        },
                        modifier = Modifier
                            .padding(end = 8.dp)
                            .weight(1f)
                    ) {
                        Text(
                            stringResource(id = R.string.buy_now),
                            style = TextStyle(
                                fontFamily = poppins,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 14.sp
                            )
                        )
                    }

                    Button(
                        onClick = {
                            snackBarHostState.currentSnackbarData?.dismiss()

                            val productVariant = ProductVariant(
                                variantPrice = if (price.value == 0) data.productPrice else price.value,
                                variantName = data.productVariant[checkedId.value].variantName
                            )

                            val productExist = cart.find { it.productId == data.productId }

                            val quantity = productExist?.quantity?.plus(1) ?: 1

                            val dataProduct = data.copy(
                                productVariant = listOf(productVariant),
                                quantity = quantity
                            )

                            if (dataProduct.quantity > data.stock) {
                                scope.launch {
                                    snackbarHostState.showSnackbar(
                                        SnackbarVisualsWithError(
                                            requireContext().resources.getString(R.string.stock_not_available),
                                            true
                                        )
                                    )
                                }
                            } else {
                                if (productExist != null) {
                                    viewModel.update(listOf(dataProduct.asCartEntity()))
                                        .observe(viewLifecycleOwner) { result ->
                                            if (result != null) {
                                                when (result) {
                                                    is Result.Loading -> {}

                                                    is Result.Success -> {
                                                        result.data.getContentIfNotHandled()?.let {
                                                            scope.launch {
                                                                snackbarHostState.showSnackbar(
                                                                    SnackbarVisualsWithError(
                                                                        it,
                                                                        false
                                                                    )
                                                                )
                                                            }
                                                        }
                                                    }

                                                    is Result.Error -> {}
                                                }
                                            }
                                        }
                                } else {
                                    viewModel.insertCart(dataProduct.asCartEntity())
                                }
                            }
                        },
                        modifier = Modifier.weight(1f)
                    ) {
                        Text(
                            stringResource(id = R.string.plus_cart),
                            style = TextStyle(
                                fontFamily = poppins,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 14.sp
                            )
                        )
                    }
                }
            }
        }
    }

    @Composable
    fun OnError(innerPadding: PaddingValues) {
        Column(
            Modifier
                .padding(innerPadding)
                .fillMaxSize()
                .padding(16.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                modifier = Modifier.size(128.dp),
                painter = painterResource(id = R.drawable.img_error),
                contentDescription = ""
            )
            Text(
                text = stringResource(id = R.string.error),
                style = TextStyle(
                    fontFamily = poppins,
                    fontWeight = FontWeight.Medium,
                    fontSize = 40.sp,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false
                    )
                ),
            )
            Text(
                textAlign = TextAlign.Center,
                text = stringResource(id = R.string.empty_desc),
                style = TextStyle(
                    fontFamily = poppins,
                    fontSize = 16.sp
                ),
            )
            Button(onClick = { viewModel.getDetailProduct() }) {
                Text(
                    text = stringResource(id = R.string.refresh),
                    style = TextStyle(
                        fontFamily = poppins,
                        fontWeight = FontWeight.Medium,
                        fontSize = 14.sp
                    )
                )
            }
        }
    }

    @Preview
    @Composable
    fun MyAppPreview() {
        ecommerceTheme {
            val index = remember { mutableIntStateOf(0) }
            MyApp(
                modifier = Modifier.fillMaxSize(),
                detailProduct = Result.Success(
                    DetailResponse(
                        code = 0,
                        message = "",
                        data = DataDetail.getDetail()
                    )
                ),
                checkedId = index,
                getAllCart = null,
                getAllWishlist = null,
                insert = null,
                insertWishlist = null,
                deleteWishlist = null,
                snackBarHostState = SnackbarHostState(),
                scope = rememberCoroutineScope(),
                windowSizeClass = WindowWidthSizeClass.Compact

            )
        }
    }

    @Preview(heightDp = 360, widthDp = 800)
    @Composable
    fun MyAppPreviewLandscape() {
        ecommerceTheme {
            val index = remember { mutableIntStateOf(0) }
            MyApp(
                modifier = Modifier.fillMaxSize(),
                detailProduct = Result.Success(
                    DetailResponse(
                        code = 0,
                        message = "",
                        data = DataDetail.getDetail()
                    )
                ),
                checkedId = index,
                getAllCart = null,
                getAllWishlist = null,
                insert = null,
                insertWishlist = null,
                deleteWishlist = null,
                snackBarHostState = SnackbarHostState(),
                scope = rememberCoroutineScope(),
                windowSizeClass = WindowWidthSizeClass.Medium
            )
        }
    }
}
