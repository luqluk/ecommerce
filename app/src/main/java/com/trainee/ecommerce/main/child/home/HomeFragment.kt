package com.trainee.ecommerce.main.child.home

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.fragment.app.Fragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.trainee.ecommerce.MainActivity
import com.trainee.ecommerce.core.util.UserPreference.dark
import com.trainee.ecommerce.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var userPreferences: SharedPreferences

    @Inject
    lateinit var analytics: FirebaseAnalytics
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnLogout.setOnClickListener {
            analytics.logEvent("button_click") {
                param("button_name", "Button logout")
            }

            val activity = activity as MainActivity
            activity.logout()
        }

        val currentLocale = AppCompatDelegate.getApplicationLocales()[0]?.language
        binding.languageSwitch.isChecked = currentLocale == "in"

        binding.languageSwitch.setOnCheckedChangeListener { button, isChecked ->
            val newLocale = if (isChecked) "in" else "en"
            val appLocale = LocaleListCompat.forLanguageTags(newLocale)
            AppCompatDelegate.setApplicationLocales(appLocale)
        }

        binding.modeSwitch.isChecked = userPreferences.dark

        binding.modeSwitch.setOnClickListener {
            if (binding.modeSwitch.isChecked) {
                userPreferences.dark = true
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                userPreferences.dark = false
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
