package com.trainee.ecommerce.main.child.wishlist

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.local.WishlistEntity
import com.trainee.ecommerce.core.util.Util.toCurrencyFormat
import com.trainee.ecommerce.databinding.GridItemWishlistBinding
import com.trainee.ecommerce.databinding.ListItemWishlistBinding

class WishlistProductAdapter(
    private val listener: OnItemClickListener
) :
    ListAdapter<WishlistEntity, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    enum class LayoutMode {
        GRID, LIST
    }

    var currentLayoutMode = LayoutMode.LIST

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (getItemViewType(viewType)) {
            VIEW_TYPE_GRID -> {
                val binding =
                    GridItemWishlistBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                GridProductViewHolder(binding, listener)
            }

            VIEW_TYPE_LIST -> {
                val binding =
                    ListItemWishlistBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                ListProductViewHolder(binding, listener)
            }

            else -> {
                throw IllegalArgumentException("Invalid view type")
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (currentLayoutMode) {
            LayoutMode.GRID -> VIEW_TYPE_GRID
            LayoutMode.LIST -> VIEW_TYPE_LIST
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = getItem(position)
        if (data != null) {
            when (holder) {
                is ListProductViewHolder -> {
                    holder.bind(data)
                }

                is GridProductViewHolder -> {
                    holder.bind(data)
                }
            }
        }
    }

    class ListProductViewHolder(
        val binding: ListItemWishlistBinding,
        private val listener: OnItemClickListener
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: WishlistEntity) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(data.image)
                    .placeholder(R.drawable.img_thumbnail)
                    .into(icon)

                title.text = data.productName
                price.text = data.productPrice.toString().toCurrencyFormat()
                seller.text = data.store
                review.text = itemView.context.getString(
                    R.string.sales,
                    data.productRating.toString(),
                    data.sale.toString()
                )

                itemView.setOnClickListener {
                    listener.onClick(data)
                }

                binding.btnDelete.setOnClickListener {
                    listener.onDeleteClicked(data)
                }

                binding.btnCart.setOnClickListener {
                    listener.onAddedCart(data)
                }
            }
        }
    }

    class GridProductViewHolder(
        val binding: GridItemWishlistBinding,
        private val listener: OnItemClickListener
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: WishlistEntity) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(data.image)
                    .placeholder(R.drawable.img_thumbnail)
                    .into(icon)

                title.text = data.productName
                price.text = data.productPrice.toString().toCurrencyFormat()
                seller.text = data.store
                review.text = itemView.context.getString(
                    R.string.sales,
                    data.productRating.toString(),
                    data.sale.toString()
                )

                itemView.setOnClickListener {
                    listener.onClick(data)
                }

                binding.btnDelete.setOnClickListener {
                    listener.onDeleteClicked(data)
                }

                binding.btnCart.setOnClickListener {
                    listener.onAddedCart(data)
                }
            }
        }
    }

    fun toggleLayoutMode() {
        currentLayoutMode =
            if (currentLayoutMode == LayoutMode.GRID) LayoutMode.LIST else LayoutMode.GRID
    }

    interface OnItemClickListener : AdapterView.OnItemClickListener {
        fun onDeleteClicked(data: WishlistEntity)
        fun onAddedCart(data: WishlistEntity)
        fun onClick(data: WishlistEntity)
    }

    companion object {

        const val VIEW_TYPE_GRID = 0
        const val VIEW_TYPE_LIST = 1

        val DIFF_CALLBACK: DiffUtil.ItemCallback<WishlistEntity> =
            object : DiffUtil.ItemCallback<WishlistEntity>() {
                override fun areItemsTheSame(
                    oldProduct: WishlistEntity,
                    newProduct: WishlistEntity
                ): Boolean {
                    return oldProduct.productId == newProduct.productId
                }

                @SuppressLint("DiffUtilEquals")
                override fun areContentsTheSame(
                    oldProduct: WishlistEntity,
                    newProduct: WishlistEntity
                ): Boolean {
                    return oldProduct == newProduct
                }
            }
    }
}
