package com.trainee.ecommerce.main.child.store

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.remote.response.Product
import com.trainee.ecommerce.core.util.Util.toCurrencyFormat
import com.trainee.ecommerce.databinding.GridItemProductBinding
import com.trainee.ecommerce.databinding.ListItemProductBinding

class ListProductAdapter(
    private val onItemClick: (Product) -> Unit
) :
    PagingDataAdapter<Product, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    enum class LayoutMode {
        GRID, LIST
    }

    var currentLayoutMode = LayoutMode.LIST

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (getItemViewType(viewType)) {
            VIEW_TYPE_GRID -> {
                val binding =
                    GridItemProductBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                GridProductViewHolder(binding, onItemClick)
            }

            VIEW_TYPE_LIST -> {
                val binding =
                    ListItemProductBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                ListProductViewHolder(binding, onItemClick)
            }

            else -> {
                throw IllegalArgumentException("Invalid view type")
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (currentLayoutMode) {
            LayoutMode.GRID -> VIEW_TYPE_GRID
            LayoutMode.LIST -> VIEW_TYPE_LIST
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = getItem(position)
        if (data != null) {
            when (holder) {
                is ListProductViewHolder -> {
                    holder.bind(data)
                }

                is GridProductViewHolder -> {
                    holder.bind(data)
                }
            }
        }
    }

    class ListProductViewHolder(
        val binding: ListItemProductBinding,
        val onItemClick: (Product) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: Product) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(data.image)
                    .placeholder(R.drawable.img_thumbnail)
                    .into(icon)

                title.text = data.productName
                price.text = data.productPrice.toString().toCurrencyFormat()
                seller.text = data.store
                review.text = itemView.context.getString(
                    R.string.sales,
                    data.productRating.toString(),
                    data.sale.toString()
                )

                itemView.setOnClickListener {
                    onItemClick(data)
                }
            }
        }
    }

    class GridProductViewHolder(
        val binding: GridItemProductBinding,
        val onItemClick: (Product) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: Product) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(data.image)
                    .placeholder(R.drawable.img_thumbnail)
                    .into(icon)

                title.text = data.productName
                price.text = data.productPrice.toString().toCurrencyFormat()
                seller.text = data.store
                review.text = itemView.context.getString(
                    R.string.sales,
                    data.productRating.toString(),
                    data.sale.toString()
                )

                itemView.setOnClickListener {
                    onItemClick(data)
                }
            }
        }
    }

    fun toggleLayoutMode() {
        currentLayoutMode =
            if (currentLayoutMode == LayoutMode.GRID) LayoutMode.LIST else LayoutMode.GRID
    }

    fun setLayout(layout: LayoutMode) {
        currentLayoutMode = if (layout == LayoutMode.GRID) LayoutMode.GRID else LayoutMode.LIST
    }

    companion object {

        const val VIEW_TYPE_GRID = 0
        const val VIEW_TYPE_LIST = 1

        val DIFF_CALLBACK: DiffUtil.ItemCallback<Product> =
            object : DiffUtil.ItemCallback<Product>() {
                override fun areItemsTheSame(oldProduct: Product, newProduct: Product): Boolean {
                    return oldProduct.productId == newProduct.productId
                }

                @SuppressLint("DiffUtilEquals")
                override fun areContentsTheSame(oldProduct: Product, newProduct: Product): Boolean {
                    return oldProduct == newProduct
                }
            }
    }
}
