package com.trainee.ecommerce.main.cart.checkout.payment.status

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.model.RatingBody
import com.trainee.ecommerce.core.data.remote.response.PaymentData
import com.trainee.ecommerce.core.util.Util
import com.trainee.ecommerce.core.util.Util.errorResponse
import com.trainee.ecommerce.core.util.Util.toCurrencyFormat
import com.trainee.ecommerce.databinding.FragmentStatusBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StatusFragment : Fragment() {

    private var _binding: FragmentStatusBinding? = null
    private val binding get() = _binding!!

    private val viewModel: StatusViewModel by viewModels()

    private lateinit var valueInvoice: String
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStatusBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

        binding.btnFinish.setOnClickListener {
            val data = RatingBody(
                rating = binding.ratingBar.rating.toInt(),
                review = binding.etReview.text.toString(),
                invoiceId = valueInvoice
            )
            viewModel.rating(data).observe(viewLifecycleOwner) { result ->
                if (result != null) {
                    when (result) {
                        is Result.Loading -> {
                            binding.progressBar.visibility = View.VISIBLE
                            binding.btnFinish.visibility = View.INVISIBLE
                        }

                        is Result.Success -> {
                            binding.progressBar.visibility = View.GONE
                            binding.btnFinish.visibility = View.VISIBLE
                            if (popBackStack()) {
                                findNavController().navigate(StatusFragmentDirections.statusToMain())
                            } else {
                                findNavController().navigateUp()
                            }
                        }

                        is Result.Error -> {
                            binding.progressBar.visibility = View.GONE
                            binding.btnFinish.visibility = View.VISIBLE

                            Snackbar.make(
                                binding.root,
                                result.error.errorResponse().toString(),
                                Snackbar.LENGTH_LONG
                            )
                                .setBackgroundTint(Util.errorColor(requireContext()))
                                .show()
                        }
                    }
                }
            }
        }
        val callback = object : OnBackPressedCallback(
            true
        ) {
            override fun handleOnBackPressed() {
                if (popBackStack()) {
                    findNavController().navigate(StatusFragmentDirections.statusToMain())
                } else {
                    findNavController().popBackStack()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            callback
        )
    }

    private fun initView() {
        val bundle = arguments
        val value = bundle?.getParcelable<PaymentData>("data")
        val rating = bundle?.getInt("rating") ?: 0
        val review = bundle?.getString("review")
        if (value != null) {
            valueInvoice = value.invoiceId
            binding.apply {
                idTransaction.text = value.invoiceId
                ratingBar.rating = rating.toFloat()
                etReview.setText(review)
                if (value.status) {
                    status.text = getString(R.string.success)
                } else {
                    status.text = getString(R.string.failed)
                }
                date.text = value.date
                time.text = value.time
                method.text = value.payment
                total.text = value.total.toString().toCurrencyFormat()
            }
        }
    }

    fun popBackStack(): Boolean {
        return findNavController().previousBackStackEntry?.destination?.id == R.id.checkoutFragment
    }
}
