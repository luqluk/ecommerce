package com.trainee.ecommerce.main.child.store.filter

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.util.Util.emptyToNull
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.databinding.FragmentFilterStoreBottomSheetBinding
import com.trainee.ecommerce.main.child.store.StoreViewModel

class FilterStoreBottomSheet : BottomSheetDialogFragment() {

    private var _binding: FragmentFilterStoreBottomSheetBinding? = null

    private val binding get() = _binding!!
    private var sort: Int? = null
    private var sorts: String? = null
    private var brand: String? = null
    private var lowestData: Int? = 0
    private var highestData: Int? = 0

    private val viewModel: StoreViewModel by activityViewModels()

    val analytics: FirebaseAnalytics = Firebase.analytics
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sort = viewModel.paramChip.value?.sort
        sorts = viewModel.param.value?.sort.toString()
        brand = viewModel.param.value?.brand.toString()
        lowestData = viewModel.param.value?.lowest
        highestData = viewModel.param.value?.highest
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        if (dialog is BottomSheetDialog) {
            dialog.behavior.skipCollapsed = true
            dialog.behavior.state = STATE_EXPANDED
        }
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFilterStoreBottomSheetBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        checkChipsByList(binding.chipGroup, sorts.toString())
        checkChipsByList(binding.chipGroup2, brand.toString())
        if (lowestData != null) binding.lowest.setText(lowestData.toString())
        if (highestData != null) binding.highest.setText(highestData.toString())

        updateState()

        initEvent()
        initAction()
    }

    private fun initAction() {
        binding.apply {
            btnShow.setOnClickListener {
                analytics.logEvent("button_click") { param("button_name", "Button show filter") }

                val sortChip = chipGroup.findViewById<Chip>(chipGroup.checkedChipId)
                val sort = sortChip?.text ?: ""
                val categoryChip = chipGroup2.findViewById<Chip>(chipGroup2.checkedChipId)
                val category = categoryChip?.text ?: ""
                val lowestText = lowest.text
                val highestText = highest.text

                val chipTextToStringResourceIdMap = mapOf(
                    chipReview to R.string.review,
                    chipSale to R.string.sale,
                    chipLowest to R.string.lowest_price,
                    chipHighest to R.string.highest_price,
                    chipApple to R.string.apple,
                    chipAsus to R.string.asus,
                    chipDell to R.string.dell,
                    chipLenovo to R.string.lenovo,
                )

                val sortId = chipTextToStringResourceIdMap[sortChip]
                val categoryId = chipTextToStringResourceIdMap[categoryChip]

                val filter = Bundle()
                filter.putString(FirebaseAnalytics.Param.ITEM_BRAND, category.toString())
                filter.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, sort.toString())
                filter.putString(FirebaseAnalytics.Param.ITEM_CATEGORY2, lowestText.toString())
                filter.putString(FirebaseAnalytics.Param.ITEM_CATEGORY3, highestText.toString())

                analytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                    param(FirebaseAnalytics.Param.ITEM_LIST_ID, "filter")
                    param(FirebaseAnalytics.Param.ITEM_LIST_NAME, "filter")
                    param(FirebaseAnalytics.Param.ITEMS, filter)
                }

                requireActivity().supportFragmentManager.setFragmentResult(
                    "filterKey",
                    bundleOf(
                        "sort" to sort,
                        "brand" to category,
                        "low" to lowestText.toString(),
                        "high" to highestText.toString()
                    )
                )

                viewModel.setQuery(
                    search = viewModel.param.value?.search,
                    sort = sort.toString().emptyToNull(),
                    brand = category.toString().emptyToNull(),
                    lowest = lowestText.toString().toIntOrNull(),
                    highest = highestText.toString().toIntOrNull()
                )

                viewModel.setChip(
                    sort = sortId,
                    brand = categoryId,
                    lowest = lowestText.toString().toIntOrNull(),
                    highest = highestText.toString().toIntOrNull()
                )
                viewModel.resetChecked()

                dismiss()
            }
        }
    }

    private fun initEvent() {
        binding.apply {
            chipGroup.setOnCheckedStateChangeListener { _, _ ->
                updateState()
            }
            chipGroup2.setOnCheckedStateChangeListener { _, _ ->
                updateState()
            }

            lowest.doOnTextChanged { _, _, _, _ ->
                updateState()
            }

            highest.doOnTextChanged { _, _, _, _ ->
                updateState()
            }

            btnReset.setOnClickListener {
                chipGroup.clearCheck()
                chipGroup2.clearCheck()
                lowest.text?.clear()
                highest.text?.clear()
            }
        }
    }

    private fun updateState() {
        binding.apply {
            if (chipNotEmpty() &&
                highest.text.isNullOrEmpty() && lowest.text.isNullOrEmpty()
            ) {
                btnReset.hide()
            } else {
                btnReset.show()
            }
        }
    }

    private fun chipNotEmpty() = binding.chipGroup.checkedChipId == View.NO_ID &&
        binding.chipGroup2.checkedChipId == View.NO_ID

    private fun checkChipsByList(chipGroup: ChipGroup, chipTexts: String) {
        for (i in 0 until chipGroup.childCount) {
            val child = chipGroup.getChildAt(i) as Chip

            if (chipTexts.contains(child.text.toString())) {
                child.isChecked = true
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val TAG = "FilterStoreBottomSheet"
    }
}
