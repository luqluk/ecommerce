package com.trainee.ecommerce.main.child.wishlist

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.local.WishlistEntity
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.util.Mapping.asCartEntity
import com.trainee.ecommerce.core.util.Util
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.core.util.Util.successColor
import com.trainee.ecommerce.databinding.FragmentWishlistBinding
import com.trainee.ecommerce.main.child.store.detail.DetailStoreFragmentComposeDirections
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WishlistFragment : Fragment(), WishlistProductAdapter.OnItemClickListener {

    private var _binding: FragmentWishlistBinding? = null
    private val binding get() = _binding!!
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var adapter: WishlistProductAdapter
    private val viewModel: WishlistViewModel by viewModels()
    private lateinit var listData: List<CartEntity>

    private val navHostFragment: NavHostFragment by lazy {
        requireActivity().supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
    }

    private val navController by lazy {
        navHostFragment.navController
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWishlistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@WishlistFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            onBackPressedCallback
        )

        initView()
        initEvent()
        collectUiState()
    }

    private fun initEvent() {
        binding.layoutButton.setOnClickListener {
            val isList = adapter.currentLayoutMode == WishlistProductAdapter.LayoutMode.LIST
            val icon = if (isList) {
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_grid)
            } else ContextCompat.getDrawable(requireContext(), R.drawable.ic_list)
            binding.layoutButton.setImageDrawable(icon)

            val span = if (isList) 2 else 1
            gridLayoutManager.spanCount = span
            adapter.toggleLayoutMode()

            viewModel.spanCount = gridLayoutManager.spanCount
        }
    }

    private fun initView() {
        gridLayoutManager = GridLayoutManager(requireContext(), viewModel.spanCount)
        binding.rvProduct.layoutManager = gridLayoutManager

        adapter = WishlistProductAdapter(this)

        if (viewModel.spanCount == 1) {
            adapter.currentLayoutMode = WishlistProductAdapter.LayoutMode.LIST
        } else {
            adapter.currentLayoutMode = WishlistProductAdapter.LayoutMode.GRID
        }

        binding.rvProduct.adapter = adapter
    }

    private fun collectUiState() {
        viewModel.getAllWishlist.observe(viewLifecycleOwner) { product ->
            adapter.submitList(product)

            binding.num.text = getString(R.string.num_wishlist, product.size.toString())

            if (product.isEmpty()) {
                binding.errorLayout.show()
                binding.top.hide()
                binding.rvProduct.hide()
                binding.descriptionError.text = getString(R.string.no_thing_in_wishlist)
            } else {
                binding.errorLayout.hide()
                binding.top.show()
                binding.rvProduct.show()
            }
        }

        viewModel.delete.observe(viewLifecycleOwner) { result ->
            if (result != null) {
                when (result) {
                    is Result.Loading -> {}
                    is Result.Success -> {
                        result.data.getContentIfNotHandled()?.let {
                            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
                                .setBackgroundTint(Util.errorColor(requireContext()))
                                .show()
                        }
                    }

                    is Result.Error -> {}
                }
            }
        }

        viewModel.getAllCart().observe(viewLifecycleOwner) { list ->
            listData = list
        }
        viewModel.insert.observe(viewLifecycleOwner) { result ->
            if (result != null) {
                when (result) {
                    is Result.Loading -> {}
                    is Result.Success -> {
                        result.data.getContentIfNotHandled()?.let {
                            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
                                .setBackgroundTint(successColor(requireContext()))
                                .show()
                        }
                    }

                    is Result.Error -> {}
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDeleteClicked(data: WishlistEntity) {
        viewModel.deleteWishlist(data)
    }

    override fun onAddedCart(data: WishlistEntity) {
        val isExist = listData.find { it.productId == data.productId }
        val quantity = if (isExist != null) isExist.quantity + 1 else 1
        val dataCopy = data.asCartEntity().copy(
            quantity = quantity
        )

        if (dataCopy.quantity > data.stock) {
            Snackbar.make(
                binding.root,
                getString(R.string.stock_not_available),
                Snackbar.LENGTH_LONG
            )
                .setBackgroundTint(Util.errorColor(requireContext()))
                .show()
        } else {
            viewModel.insertCart(dataCopy)
        }
    }

    override fun onClick(data: WishlistEntity) {
        val type = if (data.productVariant == "RAM 16GB") 0 else 1
        navController.navigate(
            DetailStoreFragmentComposeDirections.mainToDetail(
                data.productId,
                type
            )
        )
    }

    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        Log.d("TAG", "onItemClick: ")
    }
}
