package com.trainee.ecommerce.main.cart

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.util.Util
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.core.util.Util.toCurrencyFormat
import com.trainee.ecommerce.databinding.FragmentCartBinding
import com.trainee.ecommerce.main.child.store.detail.DetailStoreFragmentComposeDirections
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CartFragment : Fragment(), CartProductAdapter.OnItemClickListener {

    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CartViewModel by viewModels()
    private lateinit var adapter: CartProductAdapter
    private lateinit var data: List<CartEntity>
    private var price: Int = 0

    @Inject
    lateinit var analytics: FirebaseAnalytics
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.topAppBar.setNavigationOnClickListener {
            requireActivity().onBackPressedDispatcher.onBackPressed()
        }

        binding.rvCart.layoutManager = LinearLayoutManager(requireContext())
        binding.price.text = price.toString().toCurrencyFormat()
        adapter = CartProductAdapter(this)
        binding.rvCart.adapter = adapter
        binding.rvCart.itemAnimator?.changeDuration = 0

        binding.checkAll.setOnClickListener {
            viewModel.updateAll(binding.checkAll.isChecked)
        }

        binding.deleteAll.setOnClickListener {
            analytics.logEvent("button_click") {
                param("button_name", "Button delete all")
            }

            deleteCheckedItems()
        }

        binding.btnBuy.setOnClickListener {
            analytics.logEvent("button_click") {
                param("button_name", "Button buy")
            }

            findNavController().navigate(
                R.id.action_cartFragment_to_checkoutFragment,
                Bundle().apply {
                    val data = adapter.currentList.filter { it.isSelected }
                    putParcelableArrayList("data", ArrayList(data))
                }
            )
        }
        observeData()
    }

    private fun observeData() {
        viewModel.getAllCart().observe(viewLifecycleOwner) { entityList ->
            val itemCart =
                entityList.map {
                    Bundle().apply {
                        val data = it
                        putString(FirebaseAnalytics.Param.ITEM_ID, data.productId)
                        putString("image", data.image)
                        putString(FirebaseAnalytics.Param.ITEM_NAME, data.productName)
                        putString(FirebaseAnalytics.Param.ITEM_BRAND, data.brand)
                        putString(FirebaseAnalytics.Param.ITEM_VARIANT, data.productVariant)
                        putString("description", data.description)
                        putInt("sale", data.sale)
                        putString("store", data.store)
                        putInt(FirebaseAnalytics.Param.PRICE, data.productPrice)
                        putInt("stock", data.stock)
                        putDouble("rating", data.productRating)
                        putInt("total_rating", data.totalRating)
                        putInt("total_review", data.totalReview)
                        putInt("total_satisfaction", data.totalSatisfaction)
                    }
                }

            analytics.logEvent(FirebaseAnalytics.Event.VIEW_CART) {
                param(FirebaseAnalytics.Param.ITEMS, itemCart.toTypedArray())
            }

            adapter.submitList(entityList)

            data = entityList

            val allChecked = entityList.all { it.isSelected }
            val anyCheck = entityList.any { it.isSelected }

            binding.checkAll.isChecked = allChecked

            binding.deleteAll.isVisible = anyCheck
            binding.btnBuy.isEnabled = anyCheck

            var num = 0
            entityList.forEach {
                if (it.isSelected) {
                    num += it.productPrice * it.quantity
                }
            }
            binding.price.text = num.toString().toCurrencyFormat()
            if (entityList.isEmpty()) {
                binding.errorLayout.show()
                binding.top.hide()
                binding.bottom.hide()
                binding.descriptionError.text = getString(R.string.no_thing_in_cart)
            }
        }
    }

    override fun onCheckboxChanges(data: CartEntity) {
        viewModel.update(listOf(data))
    }

    override fun onMinus(data: CartEntity) {
        if (data.quantity <= data.stock) {
            viewModel.update(listOf(data))
        } else {
            Snackbar.make(
                binding.root,
                getString(R.string.stock_not_available),
                Snackbar.LENGTH_LONG
            ).show()
        }
    }

    override fun onPlus(data: CartEntity) {
        if (data.quantity <= data.stock) {
            viewModel.update(listOf(data))
        } else {
            Snackbar.make(
                binding.root,
                getString(R.string.stock_not_available),
                Snackbar.LENGTH_LONG
            )
                .setBackgroundTint(Util.errorColor(requireContext()))
                .show()
        }
    }

    override fun onItemClick(data: CartEntity) {
        Log.d("TAG", data.productVariant)
        val type = if (data.productVariant == "RAM 16GB") 0 else 1
        findNavController().navigate(
            DetailStoreFragmentComposeDirections.mainToDetail(data.productId, type),
        )
    }

    private fun deleteCheckedItems() {
        for (item in adapter.currentList) {
            if (item.isSelected) {
                val itemCart = Bundle().apply {
                    putString(FirebaseAnalytics.Param.ITEM_ID, item.productId)
                    putString("image", item.image)
                    putString(FirebaseAnalytics.Param.ITEM_NAME, item.productName)
                    putString(FirebaseAnalytics.Param.ITEM_BRAND, item.brand)
                    putString(FirebaseAnalytics.Param.ITEM_VARIANT, item.productVariant)
                    putString("description", item.description)
                    putInt("sale", item.sale)
                    putString("store", item.store)
                    putInt(FirebaseAnalytics.Param.PRICE, item.productPrice)
                    putInt("stock", item.stock)
                    putDouble("rating", item.productRating)
                    putInt("total_rating", item.totalRating)
                    putInt("total_review", item.totalReview)
                    putInt("total_satisfaction", item.totalSatisfaction)
                }

                analytics.logEvent(FirebaseAnalytics.Event.REMOVE_FROM_CART) {
                    param(FirebaseAnalytics.Param.ITEMS, itemCart)
                }

                viewModel.deleteCheck(item)
            }
        }
    }

    override fun onDeleteClicked(data: CartEntity) {
        viewModel.deleteCheck(data)
    }

    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        Log.d("TAG", "onItemClick: ")
    }
}
