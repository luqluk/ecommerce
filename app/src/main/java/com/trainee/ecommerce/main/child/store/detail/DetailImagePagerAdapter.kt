package com.trainee.ecommerce.main.child.store.detail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.trainee.ecommerce.databinding.ItemProductImageBinding

class DetailImagePagerAdapter(var context: Context, private var listScreen: List<String>) :
    PagerAdapter() {

    override fun instantiateItem(viewGroup: ViewGroup, position: Int): Any {
        val layoutInflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val binding = ItemProductImageBinding.inflate(layoutInflater)

        Glide.with(context)
            .load(listScreen[position])
            .into(binding.image)

        viewGroup.addView(binding.root)
        return binding.root
    }

    override fun getCount(): Int = listScreen.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as ConstraintLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as ConstraintLayout)
    }
}
