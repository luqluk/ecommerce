package com.trainee.ecommerce.main.cart.checkout

import androidx.lifecycle.ViewModel
import com.trainee.ecommerce.core.data.model.FulfillmentBody
import com.trainee.ecommerce.core.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CheckoutViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {

    fun fulfillment(data: FulfillmentBody) = mainRepository.fulfillment(data)
}
