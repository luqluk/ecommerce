package com.trainee.ecommerce.main.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trainee.ecommerce.core.data.local.NotificationEntity
import com.trainee.ecommerce.core.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {
    fun getNotification() = mainRepository.getAllNotification()

    fun update(notificationEntity: NotificationEntity) {
        viewModelScope.launch {
            mainRepository.updateNotification(notificationEntity)
        }
    }
}
