package com.trainee.ecommerce.main.child.store.detail.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import com.trainee.ecommerce.R

val poppins = FontFamily(
    Font(R.font.poppins_regular, FontWeight.Normal),
    Font(R.font.poppins_italic, FontWeight.Normal, FontStyle.Italic),
    Font(R.font.poppins_medium, FontWeight.Medium),
    Font(R.font.poppins_semi_bold, FontWeight.SemiBold),
    Font(R.font.poppins_bold, FontWeight.Bold)
)

val Typography = Typography(

    displayLarge = Typography().displayLarge.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false,
        )
    ),

    displayMedium = Typography().displayMedium.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false
        )
    ),

    displaySmall = Typography().displaySmall.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false
        )
    ),

    headlineLarge = Typography().headlineLarge.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false,
        )
    ),

    headlineMedium = Typography().headlineMedium.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false
        )
    ),

    headlineSmall = Typography().headlineSmall.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false
        )
    ),

    titleLarge = Typography().titleLarge.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false,
        )
    ),

    titleMedium = Typography().titleMedium.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false
        )
    ),

    titleSmall = Typography().titleSmall.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false
        )
    ),

    bodyLarge = Typography().bodyLarge.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false,
        )
    ),

    bodyMedium = Typography().bodyMedium.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false
        )
    ),

    bodySmall = Typography().bodySmall.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false
        )
    ),

    labelLarge = Typography().labelLarge.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false,
        )
    ),

    labelMedium = Typography().labelMedium.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false
        )
    ),

    labelSmall = Typography().labelSmall.copy(
        fontFamily = poppins,
        platformStyle = PlatformTextStyle(
            includeFontPadding = false
        )
    ),

    /* Other default text styles to override
    titleLarge = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 22.sp,
        lineHeight = 28.sp,
        letterSpacing = 0.sp
    ),
    labelSmall = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Medium,
        fontSize = 11.sp,
        lineHeight = 16.sp,
        letterSpacing = 0.5.sp
    )
     */
)
