package com.trainee.ecommerce.main.child.store

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.trainee.ecommerce.core.data.model.ProductBody
import com.trainee.ecommerce.core.data.model.ProductChip
import com.trainee.ecommerce.core.data.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {

    private val _param = MutableLiveData<ProductBody?>()
    val param: LiveData<ProductBody?> = _param

    private val _search = MutableLiveData<String?>()
    val search: LiveData<String?> = _search

    var spanCount: Int = 1
    var isList: Boolean = true

    private val _paramChip = MutableLiveData<ProductChip?>()
    val paramChip: LiveData<ProductChip?> = _paramChip

    var param1Checked: Boolean = true
    var param2Checked: Boolean = true
    var param3Checked: Boolean = true
    var param4Checked: Boolean = true

    fun resetChecked() {
        param1Checked = true
        param2Checked = true
        param3Checked = true
        param4Checked = true
    }

    fun setChip(
        brand: Int? = null,
        lowest: Int? = null,
        highest: Int? = null,
        sort: Int? = null
    ) {
        _paramChip.postValue(ProductChip(brand, lowest, highest, sort))
    }

    fun setQuery(
        search: String? = null,
        brand: String? = null,
        lowest: Int? = null,
        highest: Int? = null,
        sort: String? = null
    ) {
        _param.postValue(ProductBody(search, brand, lowest, highest, sort))
    }

    fun reset() {
        _param.postValue(null)
    }

    fun resetParam() {
        _paramChip.postValue(null)
    }

    fun setSearch(search: String? = null) {
        _search.postValue(search)
    }

    fun resetSearch() {
        _search.postValue(null)
    }

    init {
        setQuery()
    }

    val product =
        param.switchMap {
            mainRepository.getProduct(
                it?.search,
                it?.brand,
                it?.lowest,
                it?.highest,
                it?.sort
            ).cachedIn(viewModelScope)
        }

    @FlowPreview
    @OptIn(ExperimentalCoroutinesApi::class)
    val searching = search.asFlow()
        .debounce(INTERVAL)
        .distinctUntilChanged()
        .flatMapLatest {
            mainRepository.search(it).asFlow()
        }.asLiveData()

    companion object {
        const val INTERVAL = 1000L
    }
}
