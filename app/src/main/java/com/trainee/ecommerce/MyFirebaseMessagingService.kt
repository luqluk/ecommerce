package com.trainee.ecommerce

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.trainee.ecommerce.core.data.local.NotificationEntity
import com.trainee.ecommerce.core.data.repository.MainRepository
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MyFirebaseMessagingService : FirebaseMessagingService() {

    @Inject
    lateinit var mainRepository: MainRepository

    @OptIn(DelicateCoroutinesApi::class)
    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        val notificationTitle = message.data["title"] ?: ""
        val notificationBody = message.data["body"] ?: ""
        var image = message.data["image"] ?: ""

        GlobalScope.launch {
            mainRepository.insertNotification(
                NotificationEntity(
                    title = notificationTitle,
                    body = notificationBody,
                    image = image,
                    time = message.data["time"] ?: "",
                    date = message.data["date"] ?: "",
                    type = message.data["type"] ?: "",
                    isNew = true
                )
            )
        }

        createNotificationChannel()

        val pendingIntent = NavDeepLinkBuilder(this)
            .setGraph(R.navigation.app_navigation)
            .setDestination(R.id.notificationFragment)
            .createPendingIntent()

        val soundUri =
            Uri.parse("android.resource://${applicationContext.packageName}/${R.raw.tokopedia}")

        val notificationBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(notificationTitle)
            .setContentText(notificationBody)
            .setContentIntent(pendingIntent)
            .setLargeIcon(
                if (image.isEmpty()) {
                    Glide.with(this).asBitmap().load(R.drawable.img_thumbnail)
                        .submit().get()
                } else Glide.with(this).asBitmap().load(image).submit().get()
            )
            .setSmallIcon(R.drawable.ic_notification)
            .setSound(soundUri)
            .setStyle(NotificationCompat.BigTextStyle().bigText(notificationBody))
            .setAutoCancel(true)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = System.currentTimeMillis().toInt()
        notificationManager.notify(notificationId, notificationBuilder.build())
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = CHANNEL_ID
            val channelName = "Notification Channel"
            val channelDescription = "My Notification Channel"

            val channel =
                NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = channelDescription

            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    companion object {
        private const val CHANNEL_ID = "my_channel_id"
    }
}
