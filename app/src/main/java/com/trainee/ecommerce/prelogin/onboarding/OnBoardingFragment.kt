package com.trainee.ecommerce.prelogin.onboarding

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.fragment.findNavController
import com.android.commons.base.BaseFragment
import com.google.android.material.tabs.TabLayout
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.util.UserPreference.is_first
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.databinding.FragmentOnBoardingBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OnBoardingFragment :
    BaseFragment<FragmentOnBoardingBinding>(FragmentOnBoardingBinding::inflate) {

    private var introViewPagerAdapter: IntroViewPagerAdapter? = null
    private var position: Int = 0

    @Inject
    lateinit var userPref: SharedPreferences

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            val mList: ArrayList<Int> = ArrayList()
            mList.add(R.drawable.img_1)
            mList.add(R.drawable.img_2)
            mList.add(R.drawable.img_3)

            introViewPagerAdapter = IntroViewPagerAdapter(requireContext(), mList)
            screenViewpager.adapter = introViewPagerAdapter

            tabIndicator.setupWithViewPager(screenViewpager)

            btnSkip.setOnClickListener {
                analytics.logEvent("button_click") {
                    param("button_name", "Button skip")
                }

                userPref.is_first = false
                findNavController().navigate(R.id.action_onBoardingFragment_to_loginFragment)
            }

            btnJoin.setOnClickListener {
                analytics.logEvent("button_click") {
                    param("button_name", "Button join")
                }

                userPref.is_first = false
                findNavController().navigate(R.id.action_onBoardingFragment_to_registerFragment)
            }

            btnNext.setOnClickListener {
                analytics.logEvent("button_click") {
                    param("button_name", "Button next")
                }

                position = screenViewpager.currentItem
                if (position < mList.size) {
                    position++
                    screenViewpager.currentItem = position
                }
            }

            tabIndicator.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    if (tab.position == mList.size - 1) {
                        btnNext.hide()
                    } else {
                        btnNext.show()
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {
                    Log.d("TAG", "onTabUnselected: ")
                }

                override fun onTabReselected(tab: TabLayout.Tab) {
                    Log.d("TAG", "onTabReselected: ")
                }
            })
        }
    }
}
