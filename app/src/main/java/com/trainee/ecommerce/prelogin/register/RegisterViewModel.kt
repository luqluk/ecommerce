package com.trainee.ecommerce.prelogin.register

import androidx.lifecycle.ViewModel
import com.trainee.ecommerce.core.data.model.AuthBody
import com.trainee.ecommerce.core.data.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(private val authRepository: AuthRepository) :
    ViewModel() {
    fun register(registerBody: AuthBody) = authRepository.register(registerBody)
}
