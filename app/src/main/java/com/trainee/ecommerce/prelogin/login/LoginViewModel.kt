package com.trainee.ecommerce.prelogin.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trainee.ecommerce.core.data.model.AuthBody
import com.trainee.ecommerce.core.data.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel() {

    fun login(loginBody: AuthBody) = authRepository.login(loginBody)

    fun logout() {
        viewModelScope.launch {
            authRepository.deleteUser()
            authRepository.clearTable()
        }
    }
}
