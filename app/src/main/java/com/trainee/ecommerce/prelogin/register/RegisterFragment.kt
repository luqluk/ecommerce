package com.trainee.ecommerce.prelogin.register

import android.content.Context
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.android.commons.base.BaseFragment
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.color.MaterialColors
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.ktx.messaging
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.model.AuthBody
import com.trainee.ecommerce.core.util.Util
import com.trainee.ecommerce.core.util.Util.errorResponse
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.isCurrentLanguageIndonesia
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.databinding.FragmentRegisterBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RegisterFragment : BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {

    private val registerViewModel: RegisterViewModel by viewModels()

    private var token = ""

    @Inject
    lateinit var analytics: FirebaseAnalytics

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        initEvent()
        observeData()
    }

    private fun observeData() {
        binding.btnRegister.setOnClickListener {
            analytics.logEvent("button_click") {
                param("button_name", "Button register")
            }

            val email = binding.email.text.toString()
            val password = binding.password.text.toString()

            val inputMethodManager =
                requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(it?.windowToken, 0)

            registerViewModel.register(AuthBody(email, password, token))
                .observe(viewLifecycleOwner) { result ->
                    if (result != null) {
                        when (result) {
                            is Result.Loading -> {
                                binding.progressBar.show()
                                binding.btnRegister.isEnabled = false
                            }

                            is Result.Success -> {
                                Firebase.messaging.subscribeToTopic("promo")
                                    .addOnCompleteListener { task ->
                                        var msg = "Subscribed to topic"
                                        if (!task.isSuccessful) {
                                            msg = "Subscribe failed"
                                        }
                                        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT)
                                            .show()
                                    }

                                analytics.logEvent(FirebaseAnalytics.Event.SIGN_UP) {
                                    param(FirebaseAnalytics.Param.METHOD, "email")
                                }
                                binding.progressBar.hide()
                                findNavController().navigate(R.id.prelogin_to_main)
                            }

                            is Result.Error -> {
                                binding.progressBar.hide()
                                binding.btnRegister.isEnabled = true
                                val error = result.error.errorResponse().toString()
                                Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
        }
    }

    private fun initEvent() {
        var emailInput = ""
        var passwordInput = ""
        binding.email.doOnTextChanged { text, _, _, _ ->
            emailInput = text.toString()
            if (!Util.isEmailValid(emailInput)) {
                binding.etEmail.isErrorEnabled = true
                binding.etEmail.error = resources.getString(R.string.email_invalid)
                buttonEnable(false)
            } else if (Util.isEmailValid(emailInput) && !Util.isPasswordGood(passwordInput)) {
                binding.etEmail.isErrorEnabled = false
                buttonEnable(false)
            } else {
                if (binding.password.text.toString().isEmpty()) {
                    buttonEnable(false)
                } else {
                    buttonEnable(true)
                }
                binding.etEmail.isErrorEnabled = false
            }

            if (emailInput.isEmpty()) {
                binding.etEmail.isErrorEnabled = false
                buttonEnable(false)
            }
        }

        binding.password.doOnTextChanged { text, _, _, _ ->
            passwordInput = text.toString()
            if (!Util.isPasswordGood(passwordInput)) {
                binding.etPassword.isErrorEnabled = true
                binding.etPassword.error =
                    resources.getString(R.string.password_minimum_8_character)
                buttonEnable(false)
            } else if (!Util.isEmailValid(emailInput) && Util.isPasswordGood(passwordInput)) {
                binding.etPassword.isErrorEnabled = false
                buttonEnable(false)
            } else {
                if (binding.email.text.toString().isEmpty()) {
                    buttonEnable(false)
                } else {
                    buttonEnable(true)
                }
                binding.etPassword.isErrorEnabled = false
            }
            if (passwordInput.isEmpty()) {
                binding.etPassword.isErrorEnabled = false
                buttonEnable(false)
            }
        }

        binding.btnLogin.setOnClickListener {
            analytics.logEvent("button_click") {
                param("button_name", "Button login")
            }

            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
    }

    private fun initView() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                token = task.result
            }
        )
        val color1 = ForegroundColorSpan(
            MaterialColors.getColor(requireView(), com.google.android.material.R.attr.colorPrimary)
        )

        val color2 = ForegroundColorSpan(
            MaterialColors.getColor(requireView(), com.google.android.material.R.attr.colorPrimary)
        )

        val inputString = getString(R.string.register_terms)

        val spannableString = SpannableString(inputString)

        val startIndex1 = if (isCurrentLanguageIndonesia()) idStartIndex1 else enStartIndex1
        val endIndex1 = if (isCurrentLanguageIndonesia()) idEndIndex1 else enEndIndex1
        val startIndex2 = if (isCurrentLanguageIndonesia()) idStartIndex2 else enStartIndex2
        val endIndex2 = if (isCurrentLanguageIndonesia()) idEndIndex2 else enEndIndex2

        spannableString.setSpan(color1, startIndex1, endIndex1, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        spannableString.setSpan(color2, startIndex2, endIndex2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

        binding.term.text = spannableString
    }

    private fun buttonEnable(isEnable: Boolean) {
        binding.btnRegister.isEnabled = isEnable
    }

    companion object {
        const val enStartIndex1 = 32
        const val enEndIndex1 = 49
        const val enStartIndex2 = 54
        const val enEndIndex2 = 69
        const val idStartIndex1 = 38
        const val idEndIndex1 = 55
        const val idStartIndex2 = 62
        const val idEndIndex2 = 81
    }
}
