package com.trainee.ecommerce.prelogin.login

import android.content.Context.INPUT_METHOD_SERVICE
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.R.attr
import com.google.android.material.color.MaterialColors
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.ktx.messaging
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.model.AuthBody
import com.trainee.ecommerce.core.util.UserPreference.is_first
import com.trainee.ecommerce.core.util.UserPreference.state
import com.trainee.ecommerce.core.util.Util
import com.trainee.ecommerce.core.util.Util.errorResponse
import com.trainee.ecommerce.core.util.Util.hide
import com.trainee.ecommerce.core.util.Util.isEmailValid
import com.trainee.ecommerce.core.util.Util.isPasswordGood
import com.trainee.ecommerce.core.util.Util.show
import com.trainee.ecommerce.databinding.FragmentLoginBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val loginViewModel: LoginViewModel by viewModels()

    @Inject
    lateinit var userPref: SharedPreferences

    @Inject
    lateinit var analytics: FirebaseAnalytics

    private var token = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (userPref.is_first) {
            findNavController().navigate(R.id.action_loginFragment_to_onBoardingFragment)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        initEvent()
        observeData()
    }

    private fun observeData() {
        binding.btnLogin.setOnClickListener {
            val email = binding.email.text.toString()
            val password = binding.password.text.toString()

            analytics.logEvent("button_click") {
                param("button_name", "Button login")
            }

            val inputMethodManager =
                requireActivity().getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(it?.windowToken, 0)

            loginViewModel.login(AuthBody(email, password, token))
                .observe(viewLifecycleOwner) { result ->
                    if (result != null) {
                        when (result) {
                            is Result.Loading -> {
                                binding.progressBar.show()
                                binding.btnLogin.isEnabled = false
                            }

                            is Result.Success -> {
                                Firebase.messaging.subscribeToTopic("promo")
                                    .addOnCompleteListener { task ->
                                        var msg = "Subscribed to topic"
                                        if (!task.isSuccessful) {
                                            msg = "Subscribe failed"
                                        }
                                        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT)
                                            .show()
                                    }
                                analytics.logEvent(FirebaseAnalytics.Event.LOGIN) {
                                    param(FirebaseAnalytics.Param.METHOD, "email")
                                }
                                binding.progressBar.hide()
                                findNavController().navigate(R.id.prelogin_to_main)
                            }

                            is Result.Error -> {
                                binding.progressBar.hide()
                                binding.btnLogin.isEnabled = true
                                val t = result.error.errorResponse()

                                Toast.makeText(requireContext(), t, Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    }
                }
        }
    }

    private fun initEvent() {
        var emailInput = ""
        var passwordInput = ""
        binding.email.doOnTextChanged { text, _, _, _ ->
            emailInput = text.toString()
            if (!isEmailValid(emailInput)) {
                binding.etEmail.isErrorEnabled = true
                binding.etEmail.error = resources.getString(R.string.email_invalid)
                buttonEnable(false)
            } else if (isEmailValid(emailInput) && !isPasswordGood(passwordInput)) {
                binding.etEmail.isErrorEnabled = false
                buttonEnable(false)
            } else {
                if (binding.password.text.toString().isEmpty()) {
                    buttonEnable(false)
                } else {
                    buttonEnable(true)
                }
                binding.etEmail.isErrorEnabled = false
            }

            if (emailInput.isEmpty()) {
                binding.etEmail.isErrorEnabled = false
                buttonEnable(false)
            }
        }

        binding.password.doOnTextChanged { text, _, _, _ ->
            passwordInput = text.toString()
            if (!isPasswordGood(passwordInput)) {
                binding.etPassword.isErrorEnabled = true
                binding.etPassword.error =
                    resources.getString(R.string.password_minimum_8_character)
                buttonEnable(false)
            } else if (!isEmailValid(emailInput) && isPasswordGood(passwordInput)) {
                binding.etPassword.isErrorEnabled = false
                buttonEnable(false)
            } else {
                if (binding.email.text.toString().isEmpty()) {
                    buttonEnable(false)
                } else {
                    buttonEnable(true)
                }
                binding.etPassword.isErrorEnabled = false
            }
            if (passwordInput.isEmpty()) {
                binding.etPassword.isErrorEnabled = false
                buttonEnable(false)
            }
        }

        binding.btnRegister.setOnClickListener {
            analytics.logEvent("button_click") {
                param("button_name", "Button register")
            }
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    private fun initView() {
        if (userPref.state) {
            findNavController().navigate(R.id.prelogin_to_main)
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }

                token = task.result

                val msg = token
                Log.d("TAG", msg)
            }
        )

        val color1 = ForegroundColorSpan(
            MaterialColors.getColor(requireView(), attr.colorPrimary)
        )

        val color2 = ForegroundColorSpan(
            MaterialColors.getColor(requireView(), attr.colorPrimary)
        )

        val inputString = getString(R.string.login_terms)

        val spannableString = SpannableString(inputString)

        val startIndex1 = if (Util.isCurrentLanguageIndonesia()) idStartIndex else enStartIndex
        val endIndex1 = if (Util.isCurrentLanguageIndonesia()) idEndIndex else enEndIndex
        val startIndex2 = if (Util.isCurrentLanguageIndonesia()) idStartIndex2 else enStartIndex2
        val endIndex2 = if (Util.isCurrentLanguageIndonesia()) idEndIndex2 else enEndIndex2

        spannableString.setSpan(color1, startIndex1, endIndex1, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        spannableString.setSpan(color2, startIndex2, endIndex2, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

        binding.term.text = spannableString
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun buttonEnable(isEnable: Boolean) {
        binding.btnLogin.isEnabled = isEnable
    }

    companion object {
        const val enStartIndex = 29
        const val enEndIndex = 46
        const val idStartIndex = 37
        const val idEndIndex = 54
        const val enStartIndex2 = 51
        const val enEndIndex2 = 66
        const val idStartIndex2 = 61
        const val idEndIndex2 = 80
    }
}
