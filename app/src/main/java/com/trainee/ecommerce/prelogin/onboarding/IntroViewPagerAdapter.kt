package com.trainee.ecommerce.prelogin.onboarding

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.PagerAdapter
import com.trainee.ecommerce.databinding.ItemOnBoardingBinding

class IntroViewPagerAdapter(var context: Context, private var listScreen: List<Int>) :
    PagerAdapter() {

    override fun instantiateItem(viewGroup: ViewGroup, position: Int): Any {
        val layoutInflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val binding = ItemOnBoardingBinding.inflate(layoutInflater)
        binding.image.setImageResource(listScreen[position])

        viewGroup.addView(binding.root)
        return binding.root
    }

    override fun getCount(): Int = listScreen.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as ConstraintLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as ConstraintLayout)
    }
}
