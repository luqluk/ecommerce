package com.trainee.ecommerce

import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import com.trainee.ecommerce.core.di.AuthorizationListener
import com.trainee.ecommerce.core.util.UserPreference.dark
import com.trainee.ecommerce.core.util.UserPreference.token
import com.trainee.ecommerce.databinding.ActivityMainBinding
import com.trainee.ecommerce.main.child.store.StoreViewModel
import com.trainee.ecommerce.prelogin.login.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val navHostFragment: NavHostFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
    }

    private val navController by lazy {
        navHostFragment.navController
    }

    @Inject
    lateinit var userPref: SharedPreferences

    private val viewModel: LoginViewModel by viewModels()
    private val storeViewModel: StoreViewModel by viewModels()

    @Inject
    lateinit var authorizationListener: AuthorizationListener

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)
        askNotificationPermission()
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        if (userPref.dark) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }

        if (userPref.token == "") {
            logout()
        }

        lifecycleScope.launch {
            authorizationListener.isUnauthorized.collect {
                if (it) {
                    logout()
                }
            }
        }
    }

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission(),
    ) {
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }

    private fun askNotificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.POST_NOTIFICATIONS
                ) !=
                PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissionLauncher.launch(android.Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    fun logout() {
        viewModel.logout()
        storeViewModel.reset()
        storeViewModel.resetParam()
        navController.navigate(R.id.main_to_prelogin)
    }
}
