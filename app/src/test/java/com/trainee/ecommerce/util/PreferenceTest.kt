package com.trainee.ecommerce.util

import android.content.Context
import android.content.SharedPreferences
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import com.trainee.ecommerce.core.util.UserPreference.dark
import com.trainee.ecommerce.core.util.UserPreference.expire
import com.trainee.ecommerce.core.util.UserPreference.image
import com.trainee.ecommerce.core.util.UserPreference.is_first
import com.trainee.ecommerce.core.util.UserPreference.lang
import com.trainee.ecommerce.core.util.UserPreference.name
import com.trainee.ecommerce.core.util.UserPreference.position
import com.trainee.ecommerce.core.util.UserPreference.refreshToken
import com.trainee.ecommerce.core.util.UserPreference.state
import com.trainee.ecommerce.core.util.UserPreference.token
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class PreferenceTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var preference: SharedPreferences

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        preference = context.getSharedPreferences(
            context.packageName + "userPreferences",
            Context.MODE_PRIVATE
        )
    }

    @Test
    fun `set and get position`() {
        preference.position = 1
        val expectedPosition = preference.position
        assertEquals(expectedPosition, 1)
    }

    @Test
    fun `set and get name`() {
        preference.name = "name"
        val expectedName = preference.name
        assertEquals(expectedName, "name")
    }

    @Test
    fun `set and get image`() {
        preference.image = "image"
        val expectedImage = preference.image
        assertEquals(expectedImage, "image")
    }

    @Test
    fun `set and get token`() {
        preference.token = "token"
        val expectedToken = preference.token
        assertEquals(expectedToken, "token")
    }

    @Test
    fun `set and get refresh token`() {
        preference.refreshToken = "refresh"
        val expectedRefresh = preference.refreshToken
        assertEquals(expectedRefresh, "refresh")
    }

    @Test
    fun `set and get state`() {
        preference.state = true
        val expectedState = preference.state
        assertEquals(expectedState, true)
    }

    @Test
    fun `set and get is first`() {
        preference.is_first = false
        val expectedFirst = preference.is_first
        assertEquals(expectedFirst, false)
    }

    @Test
    fun `set and get expire`() {
        preference.expire = 1000
        val expectedExpire = preference.expire
        assertEquals(expectedExpire, 1000)
    }

    @Test
    fun `set and get dark`() {
        preference.dark = true
        val expectedDark = preference.dark
        assertEquals(expectedDark, true)
    }

    @Test
    fun `set and get lang`() {
        preference.lang = "en"
        val expectedLang = preference.lang
        assertEquals(expectedLang, "en")
    }
}
