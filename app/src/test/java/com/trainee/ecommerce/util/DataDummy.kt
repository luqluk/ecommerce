package com.trainee.ecommerce.util

import com.trainee.ecommerce.core.data.local.NotificationEntity
import com.trainee.ecommerce.core.data.local.WishlistEntity
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.data.remote.response.DataProduct
import com.trainee.ecommerce.core.data.remote.response.DetailProduct
import com.trainee.ecommerce.core.data.remote.response.DetailResponse
import com.trainee.ecommerce.core.data.remote.response.FulfillmentResponse
import com.trainee.ecommerce.core.data.remote.response.Login
import com.trainee.ecommerce.core.data.remote.response.LoginResponse
import com.trainee.ecommerce.core.data.remote.response.PaymentData
import com.trainee.ecommerce.core.data.remote.response.Product
import com.trainee.ecommerce.core.data.remote.response.ProductResponse
import com.trainee.ecommerce.core.data.remote.response.ProductVariant
import com.trainee.ecommerce.core.data.remote.response.Profile
import com.trainee.ecommerce.core.data.remote.response.ProfileResponse
import com.trainee.ecommerce.core.data.remote.response.Register
import com.trainee.ecommerce.core.data.remote.response.RegisterResponse
import com.trainee.ecommerce.core.data.remote.response.Review
import com.trainee.ecommerce.core.data.remote.response.ReviewResponse
import com.trainee.ecommerce.core.data.remote.response.SearchResponse
import com.trainee.ecommerce.core.data.remote.response.TransactionResponse

object DataDummy {

    fun generateDummyRegister(): RegisterResponse {
        val register = Register(
            "123",
            "321",
            0
        )
        return RegisterResponse(200, "Success", register)
    }

    fun generateDummyLogin(): LoginResponse {
        return LoginResponse(
            200,
            "OK",
            Login(
                "123",
                "321",
                0,
                "name",
                "image"
            )
        )
    }

    fun generateDummyProfile(): ProfileResponse {
        return ProfileResponse(
            code = 200,
            message = "OK",
            profile = Profile(
                userName = "name",
                userImage = "image"
            )
        )
    }

    fun generateDummyProduct(): ProductResponse {
        return ProductResponse(
            code = 200,
            message = "OK",
            data = DataProduct(
                currentItemCount = 10,
                itemsPerPage = 10,
                pageIndex = 1,
                totalPages = 4,
                product = listOf(
                    Product(
                        brand = "Asus",
                        image = "images",
                        productId = "3cecf357-d246-4eeb-ba70-c75d797fb957",
                        productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                        productPrice = 24499000,
                        productRating = 5.0,
                        sale = 12,
                        store = "AsusStore"
                    )
                )

            )
        )
    }

    fun generateDummySearch(): SearchResponse {
        return SearchResponse(
            code = 200,
            message = "OK",
            data = listOf(
                "Dell Gaming G15",
                "DELL XPS 13",
                "DELL XPS 15",
                "DELL ALIENWARE M15",
                "DELL ALIENWARE X15",
                "LAPTOP DELL LATITUDE",
                "Laptop Dell Latitude",
                "DELL VOSTRO 14"
            )
        )
    }

    fun generateDummyNotification(): NotificationEntity {
        return NotificationEntity(
            0,
            "title",
            "message",
            "image",
            "time",
            "date",
            "info",
            true,
        )
    }

    fun generateDummyDetail(): DetailResponse {
        return DetailResponse(
            code = 200,
            message = "OK",
            data = DetailProduct(
                brand = "brand",
                description = "desc",
                image = listOf("image1", "image2", "image3"),
                productId = "id",
                productName = "name",
                productPrice = 100,
                productRating = 5.0,
                productVariant = listOf(
                    ProductVariant(
                        variantName = "RAM 16GB",
                        variantPrice = 0
                    ),
                    ProductVariant(
                        variantName = "RAM 32GB",
                        variantPrice = 1000000
                    )
                ),
                sale = 1,
                stock = 1,
                store = "store",
                totalRating = 1,
                totalReview = 1,
                totalSatisfaction = 100,
                quantity = 0,
                isSelected = false
            )
        )
    }

    fun generateDummyFulfillment(): FulfillmentResponse {
        return FulfillmentResponse(
            code = 200,
            message = "OK",
            data = PaymentData(
                date = "date",
                invoiceId = "id",
                payment = "payment",
                status = true,
                time = "time",
                total = 100
            )
        )
    }

    fun generateDummyReview(): ReviewResponse {
        return ReviewResponse(
            code = 200,
            message = "OK",
            data = listOf(
                Review(
                    userName = "name",
                    userImage = "image",
                    userRating = 4,
                    userReview = "review",
                )
            )
        )
    }

    fun generateDummyTransaction(): TransactionResponse {
        return TransactionResponse(
            code = 200,
            message = "OK",
            data = listOf(
                com.trainee.ecommerce.core.data.remote.response.Transaction(
                    date = "date",
                    image = "image",
                    invoiceId = "id",
                    items = listOf(
                        com.trainee.ecommerce.core.data.remote.response.ItemTransaction(
                            productId = "id",
                            quantity = 1,
                            variantName = "RAM 16GB"
                        )
                    ),
                    name = "name",
                    payment = "payment",
                    rating = 1,
                    review = "review",
                    status = true,
                    time = "time",
                    total = 100
                )
            )
        )
    }

    fun generateDummyCart(): CartEntity {
        return CartEntity(
            "brand",
            "description",
            "image",
            "id",
            "name",
            100,
            5.0,
            "RAM 16GB",
            10,
            10,
            "store",
            10,
            10,
            100,
            1,
            true
        )
    }

    fun generateDummyWishlist(): WishlistEntity {
        return WishlistEntity(
            "brand",
            "description",
            "image",
            "id",
            "name",
            100,
            5.0,
            "RAM 16GB",
            10,
            10,
            "store",
            10,
            10,
            100,
            true
        )
    }
}
