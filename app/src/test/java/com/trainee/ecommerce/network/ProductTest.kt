package com.trainee.ecommerce.network

import com.trainee.ecommerce.core.data.model.FulfillmentBody
import com.trainee.ecommerce.core.data.model.FulfillmentItems
import com.trainee.ecommerce.core.data.model.RatingBody
import com.trainee.ecommerce.core.data.model.TokenBody
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.core.data.remote.response.FulfillmentResponse
import com.trainee.ecommerce.core.data.remote.response.ItemTransaction
import com.trainee.ecommerce.core.data.remote.response.Profile
import com.trainee.ecommerce.core.data.remote.response.ProfileResponse
import com.trainee.ecommerce.core.data.remote.response.Review
import com.trainee.ecommerce.core.data.remote.response.ReviewResponse
import com.trainee.ecommerce.core.data.remote.response.Token
import com.trainee.ecommerce.core.data.remote.response.TokenResponse
import com.trainee.ecommerce.core.data.remote.response.Transaction
import com.trainee.ecommerce.core.data.remote.response.TransactionResponse
import com.trainee.ecommerce.util.DataDummy.generateDummyDetail
import com.trainee.ecommerce.util.DataDummy.generateDummyFulfillment
import com.trainee.ecommerce.util.DataDummy.generateDummyProduct
import com.trainee.ecommerce.util.DataDummy.generateDummySearch
import com.trainee.ecommerce.util.TestExtension.enqueueResponse
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@RunWith(JUnit4::class)
class ProductTest {

    private lateinit var apiService: ApiService

    private val mockWebServer = MockWebServer()

    @Before
    fun setup() {
        mockWebServer.start()

        val client = OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.SECONDS)
            .readTimeout(1, TimeUnit.SECONDS)
            .writeTimeout(1, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        apiService = retrofit.create(ApiService::class.java)
    }

    @Test
    fun `when get product return data`() = runTest {
        mockWebServer.enqueueResponse("product.json", 200)

        val actualResponse = apiService.product(
            search = "search",
            brand = "brand",
            lowest = 0,
            highest = 0,
            sort = "sort",
            limit = 10,
            page = 1
        )
        val expectedResponse = generateDummyProduct()

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun `when get productDetail return data`() = runTest {
        mockWebServer.enqueueResponse("product_detail.json", 200)

        val actualResponse = apiService.detailProduct("id")
        val expectedResponse = generateDummyDetail()

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun `when get review return data`() = runTest {
        mockWebServer.enqueueResponse("review.json", 200)

        val actualResponse = apiService.review("id")
        val expectedResponse = ReviewResponse(
            code = 200,
            message = "OK",
            data = listOf(
                Review(
                    userName = "name",
                    userImage = "image",
                    userRating = 4,
                    userReview = "review",
                )
            )
        )

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun `when get transaction return data`() {
        mockWebServer.enqueueResponse("transaction.json", 200)

        runBlocking {
            val actualResponse = apiService.transaction()
            val expectedResponse = TransactionResponse(
                code = 200,
                message = "OK",
                data = listOf(
                    Transaction(
                        date = "date",
                        image = "image",
                        invoiceId = "id",
                        items = listOf(
                            ItemTransaction(
                                productId = "id",
                                quantity = 1,
                                variantName = "RAM 16GB"
                            )
                        ),
                        name = "name",
                        payment = "payment",
                        rating = 1,
                        review = "review",
                        status = true,
                        time = "time",
                        total = 100
                    )
                )
            )

            assertEquals(expectedResponse, actualResponse)
        }
    }

    @Test
    fun `when get search return data`() = runTest {
        mockWebServer.enqueueResponse("search.json", 200)

        val actualResponse = apiService.search("search")
        val expectedResponse = generateDummySearch()
        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun `when refresh token return data`() = runTest {
        mockWebServer.enqueueResponse("success_register.json", 200)

        val actualResponse = apiService.refreshToken(TokenBody("token"))
        val expectedResponse = TokenResponse(
            code = 200,
            message = "Success",
            token = Token(
                accessToken = "123",
                refreshToken = "321",
                expiresAt = 0
            )
        )

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun `when fulfillment return data`() = runTest {
        mockWebServer.enqueueResponse("fulfillment.json", 200)

        val actualResponse =
            apiService.fulfillment(FulfillmentBody("id", listOf(FulfillmentItems("id", "name", 1))))
        val expectedResponse = generateDummyFulfillment()

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun `when rating return data`() = runTest {
        mockWebServer.enqueueResponse("rating.json", 200)

        val actualResponse = apiService.rating(RatingBody("id", 1, "review"))
        val expectedResponse = FulfillmentResponse(
            code = 200,
            message = "Fulfillment rating and review success",
            data = null
        )

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun `when update profile return data`() = runTest {
        mockWebServer.enqueueResponse("profile.json", 200)

        val nameMultiPart = MultipartBody.Part.createFormData("userName", "name")
        val imageMultiPart = MultipartBody.Part.createFormData(
            "userImage",
            "image"
        )

        val actualResponse = apiService.profile(imageMultiPart, nameMultiPart)
        val expectedResponse = ProfileResponse(
            code = 200,
            message = "OK",
            profile = Profile(
                userName = "name",
                userImage = "image"
            )
        )

        assertEquals(expectedResponse, actualResponse)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }
}
