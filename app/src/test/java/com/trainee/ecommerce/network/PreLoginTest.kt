package com.trainee.ecommerce.network

import com.trainee.ecommerce.core.data.model.AuthBody
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.util.DataDummy.generateDummyLogin
import com.trainee.ecommerce.util.DataDummy.generateDummyRegister
import com.trainee.ecommerce.util.TestExtension.enqueueResponse
import kotlinx.coroutines.test.runTest
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@RunWith(JUnit4::class)
class PreLoginTest {

    private lateinit var apiService: ApiService

    private val mockWebServer = MockWebServer()

    @Before
    fun setup() {
        mockWebServer.start()

        val client = OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.SECONDS)
            .readTimeout(1, TimeUnit.SECONDS)
            .writeTimeout(1, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        apiService = retrofit.create(ApiService::class.java)
    }

    @Test
    fun `when login should return success`() = runTest {
        mockWebServer.enqueueResponse("success_login.json", 200)

        val actualResponse = apiService.login(AuthBody("email", "password", ""))
        val expectedResponse = generateDummyLogin()

        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun `when register should return success`() = runTest {
        mockWebServer.enqueueResponse("success_register.json", 200)

        val actualResponse = apiService.register(AuthBody("email", "password", ""))

        assertEquals(generateDummyRegister(), actualResponse)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }
}
