package com.trainee.ecommerce.prelogin.register

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.model.AuthBody
import com.trainee.ecommerce.core.data.remote.response.Register
import com.trainee.ecommerce.core.data.repository.AuthRepository
import com.trainee.ecommerce.util.DataDummy
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class RegisterViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var authRepository: AuthRepository
    private lateinit var registerViewModel: RegisterViewModel
    private val dummyDetailRegister = DataDummy.generateDummyRegister()

    @Before
    fun setUp() {
        registerViewModel = RegisterViewModel(authRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val mainDispatcherRule = DispatcherRule()

    @Test
    fun `When Register Should Not Null and Return Success`() {
        val expectedResponse = MutableLiveData<Result<Register>>()
        expectedResponse.value = Result.Success(dummyDetailRegister.register)

        whenever(authRepository.register(AuthBody())).thenReturn(expectedResponse)
        val actualResponse = registerViewModel.register(AuthBody()).getOrAwaitValue()
        assertNotNull(actualResponse)
        assertTrue(actualResponse is Result.Success)
        assertEquals(
            (expectedResponse.value as Result.Success).data,
            (actualResponse as Result.Success).data
        )
    }
}
