package com.trainee.ecommerce.prelogin.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.model.AuthBody
import com.trainee.ecommerce.core.data.remote.response.Login
import com.trainee.ecommerce.core.data.repository.AuthRepository
import com.trainee.ecommerce.util.DataDummy
import com.trainee.ecommerce.util.MainDispatcherRule
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var authRepository: AuthRepository
    private lateinit var loginViewModel: LoginViewModel
    private val dummyDetailLogin = DataDummy.generateDummyLogin()

    @Before
    fun setUp() {
        loginViewModel = LoginViewModel(authRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Test
    fun `When Login Should Not Null and Return Success`() {
        val expectedResponse = MutableLiveData<Result<Login>>()
        expectedResponse.value = Result.Success(dummyDetailLogin.login)

        whenever(authRepository.login(AuthBody())).thenReturn(expectedResponse)
        val actualResponse = loginViewModel.login(AuthBody()).getOrAwaitValue()
        assertNotNull(actualResponse)
        assertTrue(actualResponse is Result.Success)
        assertEquals(dummyDetailLogin.login, (actualResponse as Result.Success).data)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `when logout cleared`() = runTest {
        loginViewModel.logout()
        advanceUntilIdle()
        Mockito.verify(authRepository).clearTable()
        Mockito.verify(authRepository).deleteUser()
    }
}
