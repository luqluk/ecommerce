package com.trainee.ecommerce.main.notification

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.trainee.ecommerce.core.data.local.NotificationEntity
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.util.DataDummy
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class NotificationViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val dispatcherRule = DispatcherRule()

    private lateinit var mainRepository: MainRepository

    private lateinit var notificationViewModel: NotificationViewModel

    @Before
    fun setUp() {
        mainRepository = mock()
        notificationViewModel = NotificationViewModel(mainRepository)
    }

    @Test
    fun `when get notification should call get notification`() = runTest {
        val expectedResponse: MutableLiveData<List<NotificationEntity>> = MutableLiveData(
            listOf(DataDummy.generateDummyNotification())
        )

        whenever(mainRepository.getAllNotification()).thenReturn(expectedResponse)

        val actualResponse = notificationViewModel.getNotification()
        actualResponse.observeForTesting {
            assertEquals(expectedResponse.value, actualResponse.value)
        }
    }

    @Test
    fun `when  update should call get update`() = runTest {
        whenever(mainRepository.updateNotification(DataDummy.generateDummyNotification())).thenReturn(
            Unit
        )
        val actualResponse = notificationViewModel.update(DataDummy.generateDummyNotification())
        assertEquals(Unit, actualResponse)
    }
}
