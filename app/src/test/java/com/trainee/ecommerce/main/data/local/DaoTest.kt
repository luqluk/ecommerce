package com.trainee.ecommerce.main.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.trainee.ecommerce.core.data.local.AppDatabase
import com.trainee.ecommerce.core.data.local.Dao
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.util.DataDummy.generateDummyCart
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class DaoTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var dao: Dao

    @Before
    fun initDb() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.detailDao()
    }

    @Test
    fun `insert and get cart product`() = runTest {
        dao.insertCart(actualCart)
        val expectedCart = dao.getCartProduct().getOrAwaitValue()
        assertEquals(expectedCart, listOf(actualCart))
    }

    @Test
    fun `delete checked product`() = runTest {
        dao.insertCart(actualCart)
        dao.deleteChecked(actualCart)
        val expectedCart = dao.getCartProduct().getOrAwaitValue()
        assertEquals(expectedCart, listOf<CartEntity>())
    }

    @Test
    fun `update product`() = runTest {
        dao.insertCart(actualCart)

        val newCart = actualCart.copy(isSelected = false)
        dao.update(newCart)

        val newExpectedCart = dao.getCartProduct().getOrAwaitValue()
        assertEquals(newExpectedCart, listOf(newCart))
    }

    @Test
    fun `update all selected`() = runTest {
        dao.insertCart(actualCart)

        dao.updateAll(false)

        val newExpectedCart = dao.getCartProduct().getOrAwaitValue()
        assertEquals(newExpectedCart, listOf(actualCart.copy(isSelected = false)))
    }

    @After
    fun closeDb() = database.close()

    companion object {
        private val actualCart = generateDummyCart()
    }
}
