package com.trainee.ecommerce.main.child.store

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingData
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.remote.response.Product
import com.trainee.ecommerce.core.data.remote.response.SearchResponse
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.util.DataDummy.generateDummySearch
import com.trainee.ecommerce.util.MainDispatcherRule
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class StoreViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository
    private lateinit var storeViewModel: StoreViewModel

    @Before
    fun setUp() {
        storeViewModel = StoreViewModel(mainRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Test
    fun `when query not empty return list product`() = runTest {
        val expectedResponse = MutableLiveData<PagingData<Product>>()
        expectedResponse.value = PagingData.from(listOf())

        storeViewModel.setQuery()
        backgroundScope.launch {
            val actualResponse = storeViewModel.product.value
            assertEquals(expectedResponse.value, actualResponse)
        }
    }

    @OptIn(FlowPreview::class)
    @Test
    fun `when search not empty start search`() = runTest {
        val expectedResponse =
            MutableLiveData<Result<SearchResponse>>()
        expectedResponse.value =
            Result.Success(generateDummySearch())
        storeViewModel.setSearch()

        backgroundScope.launch {
            val actualResponse = storeViewModel.searching.getOrAwaitValue()
            assertEquals(expectedResponse.value, actualResponse)
        }
    }

    @Test
    fun `when param not empty make chip`() = runTest {
        storeViewModel.setChip()
        assertEquals(null, storeViewModel.paramChip.value?.brand)
    }

    @Test
    fun `reset query`() {
        storeViewModel.reset()
        assertEquals(null, storeViewModel.param.value)
    }

    @Test
    fun `reset search`() {
        storeViewModel.resetSearch()
        assertEquals(null, storeViewModel.search.value)
    }

    @Test
    fun `when checked reset return true`() {
        storeViewModel.param1Checked = false
        storeViewModel.param2Checked = false
        storeViewModel.param3Checked = false
        storeViewModel.param4Checked = false
        storeViewModel.resetChecked()
        assertTrue(storeViewModel.param1Checked)
        assertTrue(storeViewModel.param2Checked)
        assertTrue(storeViewModel.param3Checked)
        assertTrue(storeViewModel.param4Checked)
    }
}
