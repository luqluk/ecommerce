package com.trainee.ecommerce.main.data.repository

import android.content.SharedPreferences
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.local.AppDatabase
import com.trainee.ecommerce.core.data.model.AuthBody
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.core.data.repository.AuthRepository
import com.trainee.ecommerce.core.util.UserPreference.expire
import com.trainee.ecommerce.core.util.UserPreference.image
import com.trainee.ecommerce.core.util.UserPreference.name
import com.trainee.ecommerce.core.util.UserPreference.refreshToken
import com.trainee.ecommerce.core.util.UserPreference.state
import com.trainee.ecommerce.core.util.UserPreference.token
import com.trainee.ecommerce.util.DataDummy.generateDummyLogin
import com.trainee.ecommerce.util.DataDummy.generateDummyRegister
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class AuthRepositoryTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val dispatcherRule = DispatcherRule()

    private lateinit var apiService: ApiService
    private lateinit var authRepository: AuthRepository

    @Mock
    private lateinit var database: AppDatabase

    @Mock
    private lateinit var sharedPreferences: SharedPreferences

    @Mock
    private lateinit var editor: SharedPreferences.Editor

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        apiService = mock()
        whenever(sharedPreferences.edit()).thenReturn(editor)
        whenever(editor.putString(Mockito.anyString(), Mockito.anyString())).thenReturn(editor)

        authRepository = AuthRepository(apiService, sharedPreferences, database)
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun `when register success return data`() = runTest {
        val expectedResponse = generateDummyRegister()
        whenever(apiService.register(AuthBody())).thenReturn(expectedResponse)
        val actualResponse = authRepository.register(AuthBody())

        actualResponse.observeForTesting {
            assertNotNull(actualResponse)
            assertEquals(
                expectedResponse.register,
                (actualResponse.value as Result.Success).data
            )
        }
    }

    @Test
    fun `when register error throw error message`() = runTest {
        val error = RuntimeException("Register Error")
        whenever(apiService.register(AuthBody())).thenThrow(error)

        val actualResponse = authRepository.register(AuthBody())
        actualResponse.observeForTesting {
            assertEquals(error.message, (actualResponse.value as Result.Error).error.message)
        }
    }

    @Test
    fun `when login success return data`() = runTest {
        val expectedResponse = generateDummyLogin()
        whenever(apiService.login(AuthBody())).thenReturn(expectedResponse)
        val actualResponse = authRepository.login(AuthBody())

        actualResponse.observeForTesting {
            assertNotNull(actualResponse)
            assertEquals(
                expectedResponse.login,
                (actualResponse.value as Result.Success).data
            )
        }
    }

    @Test
    fun `when login failed throw error message`() = runTest {
        val error = RuntimeException("Login Error")
        whenever(apiService.login(AuthBody())).thenThrow(error)
        val actualResponse = authRepository.login(AuthBody())

        actualResponse.observeForTesting {
            assertNotNull(actualResponse)
            assertEquals(
                error.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }

    @Test
    fun `delete user`() {
        val userPreferences = TestPreferences()
        whenever(sharedPreferences.name).thenReturn("")
        whenever(sharedPreferences.image).thenReturn("")
        whenever(sharedPreferences.token).thenReturn("")
        whenever(sharedPreferences.refreshToken).thenReturn("")
        whenever(sharedPreferences.expire).thenReturn(0)
        whenever(sharedPreferences.state).thenReturn(false)

        authRepository.deleteUser()

        assertEquals("", userPreferences.name)
    }

    @Test
    fun `clearTable() should clear tables in IO dispatcher`() = runTest {
        launch(StandardTestDispatcher(testScheduler)) {
            authRepository.clearTable()
            verify(database).clearAllTables()
        }
    }
}

class TestPreferences {

    var name: String = ""
    var image: String = ""
    var token: String = ""
    var refreshToken: String = ""
    var expire: Long = 0
    var state: Boolean = false
}
