package com.trainee.ecommerce.main.cart.checkout.payment.status

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.model.RatingBody
import com.trainee.ecommerce.core.data.remote.response.FulfillmentResponse
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.util.DataDummy
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class StatusViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val dispatcherRule = DispatcherRule()

    private lateinit var mainRepository: MainRepository

    private lateinit var statusViewModel: StatusViewModel

    @Before
    fun setUp() {
        mainRepository = mock()
        statusViewModel = StatusViewModel(mainRepository)
    }

    @Test
    fun `when fulfillment should return data`() = runTest {
        val expectedResponse = MutableLiveData<Result<FulfillmentResponse>>()
        val expectedValue = RatingBody("", 0, "")
        expectedResponse.value = Result.Success(DataDummy.generateDummyFulfillment())
        whenever(mainRepository.rating(expectedValue)).thenReturn(expectedResponse)

        val actualResponse = statusViewModel.rating(expectedValue).getOrAwaitValue()
        assertEquals(expectedResponse.value, actualResponse)

        Mockito.verify(mainRepository).rating(expectedValue)
    }
}
