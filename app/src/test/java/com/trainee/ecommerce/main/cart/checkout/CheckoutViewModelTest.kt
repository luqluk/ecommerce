package com.trainee.ecommerce.main.cart.checkout

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.model.FulfillmentBody
import com.trainee.ecommerce.core.data.remote.response.FulfillmentResponse
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.util.DataDummy
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class CheckoutViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val dispatcherRule = DispatcherRule()

    private lateinit var mainRepository: MainRepository

    private lateinit var checkoutViewModel: CheckoutViewModel

    @Before
    fun setUp() {
        mainRepository = mock()
        checkoutViewModel = CheckoutViewModel(mainRepository)
    }

    @Test
    fun `when fulfillment should return data`() = runTest {
        val expectedResponse = MutableLiveData<Result<FulfillmentResponse>>()
        val expectedValue = FulfillmentBody("", listOf())
        expectedResponse.value = Result.Success(DataDummy.generateDummyFulfillment())
        whenever(mainRepository.fulfillment(expectedValue)).thenReturn(expectedResponse)

        val actualResponse = checkoutViewModel.fulfillment(expectedValue).getOrAwaitValue()
        assertEquals(expectedResponse.value, actualResponse)

        Mockito.verify(mainRepository).fulfillment(expectedValue)
    }
}
