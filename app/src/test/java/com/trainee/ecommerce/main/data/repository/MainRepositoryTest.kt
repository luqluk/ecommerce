package com.trainee.ecommerce.main.data.repository

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.trainee.ecommerce.R
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.local.Dao
import com.trainee.ecommerce.core.data.local.NotifDao
import com.trainee.ecommerce.core.data.local.WishlistDao
import com.trainee.ecommerce.core.data.model.FulfillmentBody
import com.trainee.ecommerce.core.data.model.RatingBody
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.core.util.Mapping.asWishlistEntity
import com.trainee.ecommerce.util.DataDummy.generateDummyCart
import com.trainee.ecommerce.util.DataDummy.generateDummyDetail
import com.trainee.ecommerce.util.DataDummy.generateDummyFulfillment
import com.trainee.ecommerce.util.DataDummy.generateDummyNotification
import com.trainee.ecommerce.util.DataDummy.generateDummyReview
import com.trainee.ecommerce.util.DataDummy.generateDummySearch
import com.trainee.ecommerce.util.DataDummy.generateDummyTransaction
import com.trainee.ecommerce.util.DataDummy.generateDummyWishlist
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class MainRepositoryTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val dispatcherRule = DispatcherRule()

    private lateinit var apiService: ApiService

    @Mock
    private lateinit var context: Context

    private lateinit var productDao: Dao
    private lateinit var wishlistDao: WishlistDao
    private lateinit var notificationDao: NotifDao
    private lateinit var mainRepository: MainRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        apiService = mock()
        productDao = mock()
        wishlistDao = mock()
        notificationDao = mock()
        mainRepository =
            MainRepository(context, apiService, productDao, wishlistDao, notificationDao)
    }

    @Test
    fun `when searchProduct should return success`() = runTest {
        val expectedResponse = generateDummySearch()
        whenever(apiService.search("query")).thenReturn(expectedResponse)
        val actualResponse = mainRepository.search("query")
        actualResponse.observeForTesting {
            assertEquals(expectedResponse, (actualResponse.value as Result.Success).data)
        }
    }

    @Test
    fun `when search failed should return error`() = runTest {
        val expectedError = RuntimeException("Error")
        whenever(apiService.search("query")).thenThrow(expectedError)
        val actualResponse = mainRepository.search("query")
        actualResponse.observeForTesting {
            assertEquals(
                expectedError.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }

    @Test
    fun `when detailProduct should return success`() = runTest {
        val expectedResponse = generateDummyDetail()
        whenever(apiService.detailProduct("id")).thenReturn(expectedResponse)
        val actualResponse = mainRepository.detailProduct("id")
        actualResponse.observeForTesting {
            assertEquals(expectedResponse, (actualResponse.value as Result.Success).data)
        }
    }

    @Test
    fun `when detailProduct failed should return error`() = runTest {
        val expectedError = RuntimeException("Something went wrong")
        whenever(apiService.detailProduct("id")).thenThrow(expectedError)
        val actualResponse = mainRepository.detailProduct("id")
        actualResponse.observeForTesting {
            assertEquals(
                expectedError.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }

    @Test
    fun `when fulfillment should return success`() = runTest {
        val expectedResponse = generateDummyFulfillment()
        whenever(apiService.fulfillment(FulfillmentBody("", listOf()))).thenReturn(expectedResponse)
        val actualResponse = mainRepository.fulfillment(FulfillmentBody("", listOf()))
        actualResponse.observeForTesting {
            assertEquals(expectedResponse, (actualResponse.value as Result.Success).data)
        }
    }

    @Test
    fun `when fulfillment failed should return error`() = runTest {
        val expectedError = RuntimeException("Something went wrong")
        whenever(apiService.fulfillment(FulfillmentBody("", listOf()))).thenThrow(expectedError)
        val actualResponse = mainRepository.fulfillment(FulfillmentBody("", listOf()))
        actualResponse.observeForTesting {
            assertEquals(
                expectedError.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }

    @Test
    fun `when rating should return success`() = runTest {
        val expectedResponse = generateDummyFulfillment()
        whenever(apiService.rating(RatingBody("", 0, ""))).thenReturn(expectedResponse)
        val actualResponse = mainRepository.rating(RatingBody("", 0, ""))
        actualResponse.observeForTesting {
            assertEquals(expectedResponse, (actualResponse.value as Result.Success).data)
        }
    }

    @Test
    fun `when rating failed should return error`() = runTest {
        val expectedError = RuntimeException("Something went wrong")
        whenever(apiService.rating(RatingBody("", 0, ""))).thenThrow(expectedError)
        val actualResponse = mainRepository.rating(RatingBody("", 0, ""))
        actualResponse.observeForTesting {
            assertEquals(
                expectedError.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }

    @Test
    fun `when review should return success`() = runTest {
        val expectedResponse = generateDummyReview()
        whenever(apiService.review("id")).thenReturn(expectedResponse)
        val actualResponse = mainRepository.reviewProduct("id")
        actualResponse.observeForTesting {
            assertEquals(expectedResponse, (actualResponse.value as Result.Success).data)
        }
    }

    @Test
    fun `when review failed should return error`() = runTest {
        val expectedError = RuntimeException("Something went wrong")
        whenever(apiService.review("id")).thenThrow(expectedError)
        val actualResponse = mainRepository.reviewProduct("id")
        actualResponse.observeForTesting {
            assertEquals(
                expectedError.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }

    @Test
    fun `when transaction should return success`() = runTest {
        val expectedResponse = generateDummyTransaction()
        whenever(apiService.transaction()).thenReturn(expectedResponse)
        val actualResponse = mainRepository.transaction()
        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun `when insert to cart should return success`() = runTest {
        whenever(productDao.insertCart(generateDummyCart())).thenReturn(Unit)
        val actualResponse = mainRepository.insertCart(generateDummyCart())
        actualResponse.observeForTesting {
            assertEquals(
                context.getString(R.string.item_added_successfully),
                (actualResponse.value as Result.Success).data.getContentIfNotHandled()
            )
        }
    }

    @Test
    fun `when insert to cart failed should return error`() = runTest {
        val expectedError = RuntimeException("Something went wrong")
        whenever(productDao.insertCart(generateDummyCart())).thenThrow(expectedError)
        val actualResponse = mainRepository.insertCart(generateDummyCart())
        actualResponse.observeForTesting {
            assertEquals(
                expectedError.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }

    @Test
    fun `when update cart should return success`() = runTest {
        whenever(productDao.update(generateDummyCart())).thenReturn(Unit)
        val actualResponse = mainRepository.updateCart(listOf(generateDummyCart()))
        assertEquals(
            Unit,
            actualResponse
        )
    }

    @Test
    fun `when update all cart should return success`() = runTest {
        whenever(productDao.updateAll(true)).thenReturn(Unit)
        val actualResponse = mainRepository.updateAllCart(true)
        assertEquals(
            Unit,
            actualResponse
        )
    }

    @Test
    fun `when delete cart should return success`() = runTest {
        whenever(productDao.deleteChecked(generateDummyCart())).thenReturn(Unit)
        val actualResponse = mainRepository.deleteCart(generateDummyCart())
        assertEquals(
            Unit,
            actualResponse
        )
    }

    @Test
    fun `when get all cart should return success`() = runTest {
        val expectedResponse = MutableLiveData(
            listOf(generateDummyCart())
        )
        whenever(productDao.getCartProduct()).thenReturn(expectedResponse)
        val actualResponse = mainRepository.getAllCart()
        assertEquals(
            expectedResponse,
            actualResponse
        )
    }

    @Test
    fun `when get all wishlist should return success`() = runTest {
        val expectedResponse = MutableLiveData(
            listOf(generateDummyWishlist())
        )
        whenever(wishlistDao.getCartWishlist()).thenReturn(expectedResponse)
        val actualResponse = mainRepository.getAllWishlist()
        assertEquals(
            expectedResponse,
            actualResponse
        )
    }

    @Test
    fun `when insert wishlist should return success`() = runTest {
        whenever(wishlistDao.insertWishlist(generateDummyWishlist())).thenReturn(Unit)
        val actualResponse = mainRepository.insertWishlist(generateDummyDetail().data)
        actualResponse.observeForTesting {
            assertEquals(
                context.getString(R.string.item_added_to_wishlist),
                (actualResponse.value as Result.Success).data.getContentIfNotHandled()
            )
        }
    }

    @Test
    fun `when insert wishlist failed should return error`() = runTest {
        val expectedError = RuntimeException("Something went wrong")
        whenever(wishlistDao.insertWishlist(generateDummyDetail().data.asWishlistEntity())).thenThrow(
            expectedError
        )
        val actualResponse = mainRepository.insertWishlist(generateDummyDetail().data)
        actualResponse.observeForTesting {
            assertEquals(
                expectedError.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }

    @Test
    fun `when delete wishlist should return success`() = runTest {
        whenever(wishlistDao.delete(generateDummyWishlist())).thenReturn(Unit)
        val actualResponse = mainRepository.deleteWishlist(generateDummyWishlist())
        actualResponse.observeForTesting {
            assertEquals(
                context.getString(R.string.item_removed_from_wishlist),
                (actualResponse.value as Result.Success).data.getContentIfNotHandled()
            )
        }
    }

    @Test
    fun `when delete wishlist failed should return error`() = runTest {
        val expectedError = RuntimeException("Something went wrong")
        whenever(wishlistDao.delete(generateDummyWishlist())).thenThrow(expectedError)
        val actualResponse = mainRepository.deleteWishlist(generateDummyWishlist())
        actualResponse.observeForTesting {
            assertEquals(
                expectedError.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }

    @Test
    fun `when get notification should return success`() {
        val expectedResponse = MutableLiveData(
            listOf(generateDummyNotification())
        )
        whenever(notificationDao.getNotification()).thenReturn(expectedResponse)
        val actualResponse = mainRepository.getAllNotification()
        assertEquals(
            expectedResponse,
            actualResponse
        )
    }

    @Test
    fun `when insert notification should return success`() = runTest {
        whenever(notificationDao.insertNotification(generateDummyNotification())).thenReturn(Unit)
        val actualResponse = mainRepository.insertNotification(generateDummyNotification())
        assertEquals(
            Unit,
            actualResponse
        )
    }

    @Test
    fun `when update notification should return success`() = runTest {
        whenever(notificationDao.update(generateDummyNotification())).thenReturn(Unit)
        val actualResponse = mainRepository.updateNotification(generateDummyNotification())
        assertEquals(
            Unit,
            actualResponse
        )
    }
}
