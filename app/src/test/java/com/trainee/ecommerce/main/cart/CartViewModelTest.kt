package com.trainee.ecommerce.main.cart

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.util.DataDummy
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class CartViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val dispatcherRule = DispatcherRule()

    private lateinit var mainRepository: MainRepository

    private lateinit var cartViewModel: CartViewModel

    @Before
    fun setUp() {
        mainRepository = mock()
        cartViewModel = CartViewModel(mainRepository)
    }

    @Test
    fun `when get cart should call get cart`() = runTest {
        val expectedResponse: MutableLiveData<List<CartEntity>> =
            MutableLiveData(listOf(DataDummy.generateDummyCart()))
        whenever(mainRepository.getAllCart()).thenReturn(expectedResponse)

        val actualResponse = cartViewModel.getAllCart().getOrAwaitValue()
        assertEquals(expectedResponse.value, actualResponse)

        Mockito.verify(mainRepository).getAllCart()
    }

    @Test
    fun `when checked then delete`() = runTest {
        val expectedValue = DataDummy.generateDummyCart()
        whenever(mainRepository.deleteCart(expectedValue)).thenReturn(Unit)
        val actualResponse = cartViewModel.deleteCheck(DataDummy.generateDummyCart())
        assertEquals(Unit, actualResponse)
    }

    @Test
    fun `cart update`() = runTest {
        val expectedValue = listOf(DataDummy.generateDummyCart())
        whenever(mainRepository.updateCart(expectedValue)).thenReturn(Unit)
        val actualResponse = cartViewModel.update(expectedValue)
        assertEquals(Unit, actualResponse)
    }

    @Test
    fun `when checked then update`() = runTest {
        val expectedBoolean = true
        whenever(mainRepository.updateAllCart(expectedBoolean)).thenReturn(Unit)
        val actualResponse = cartViewModel.updateAll(true)
        assertEquals(Unit, actualResponse)
    }
}
