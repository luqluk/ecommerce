package com.trainee.ecommerce.main.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.trainee.ecommerce.core.data.local.AppDatabase
import com.trainee.ecommerce.core.data.local.WishlistDao
import com.trainee.ecommerce.core.data.local.WishlistEntity
import com.trainee.ecommerce.util.DataDummy.generateDummyWishlist
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class WishlistTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var dao: WishlistDao

    @Before
    fun initDb() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.wishlistDao()
    }

    @Test
    fun `insert and get wishlist product`() = runTest {
        dao.insertWishlist(actualWishlist)
        val expectedWishlist = dao.getCartWishlist().getOrAwaitValue()
        assertEquals(expectedWishlist, listOf(actualWishlist))
    }

    @Test
    fun `insert and delete wishlist product`() = runTest {
        dao.insertWishlist(actualWishlist)
        dao.delete(actualWishlist)
        val expectedWishlist = dao.getCartWishlist().getOrAwaitValue()
        assertEquals(expectedWishlist, emptyList<WishlistEntity>())
    }

    @Test
    fun `insert and delete all wishlist product`() = runTest {
        dao.insertWishlist(actualWishlist)
        dao.deleteAll()
        val expectedWishlist = dao.getCartWishlist().getOrAwaitValue()
        assertEquals(expectedWishlist, emptyList<WishlistEntity>())
    }

    @Test
    fun `insert and update wishlist product`() = runTest {
        dao.insertWishlist(actualWishlist)
        val expectedWishlist = dao.getCartWishlist().getOrAwaitValue()
        assertEquals(expectedWishlist, listOf(actualWishlist))
        val updatedWishlist = actualWishlist.copy(productVariant = "RAM 32GB")
        dao.update(updatedWishlist)
        val expectedWishlistAfterUpdate = dao.getCartWishlist().getOrAwaitValue()
        assertEquals(expectedWishlistAfterUpdate, listOf(updatedWishlist))
    }

    companion object {
        private val actualWishlist = generateDummyWishlist()
    }
}
