package com.trainee.ecommerce.main.child.transaction

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.util.DataDummy
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class TransactionViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val dispatcherRule = DispatcherRule()

    private lateinit var mainRepository: MainRepository

    private lateinit var transactionViewModel: TransactionViewModel

    @Before
    fun setUp() {
        mainRepository = mock()
        transactionViewModel = TransactionViewModel(mainRepository)
    }

    @Test
    fun `when get transaction should call get transaction`() = runTest {
        val expectedResponse = DataDummy.generateDummyTransaction()

        whenever(mainRepository.transaction()).thenReturn(expectedResponse)
        transactionViewModel.transaction()

        val actualResponse = transactionViewModel.transaction
        actualResponse.observeForTesting {
            assertEquals(expectedResponse, (actualResponse.value as Result.Success).data)
        }
    }

    @Test
    fun `when get transaction failed should error`() = runTest {
        val expectedError = RuntimeException("Error")

        whenever(mainRepository.transaction()).thenThrow(expectedError)
        transactionViewModel.transaction()

        val actualResponse = transactionViewModel.transaction
        actualResponse.observeForTesting {
            assertEquals(
                expectedError.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }
}
