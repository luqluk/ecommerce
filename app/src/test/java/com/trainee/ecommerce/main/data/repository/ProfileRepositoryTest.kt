package com.trainee.ecommerce.main.data.repository

import android.content.SharedPreferences
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.remote.ApiService
import com.trainee.ecommerce.core.data.repository.ProfileRepository
import com.trainee.ecommerce.util.DataDummy.generateDummyProfile
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.MultipartBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class ProfileRepositoryTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val dispatcherRule = DispatcherRule()

    private lateinit var apiService: ApiService
    private lateinit var profileRepository: ProfileRepository

    @Mock
    private lateinit var sharedPreferences: SharedPreferences

    @Mock
    private lateinit var editor: SharedPreferences.Editor

    @Before
    fun setUp() {
        apiService = mock()

        MockitoAnnotations.initMocks(this)
        whenever(sharedPreferences.edit()).thenReturn(editor)
        whenever(editor.putString(Mockito.anyString(), Mockito.anyString())).thenReturn(editor)

        profileRepository = ProfileRepository(apiService, sharedPreferences)
    }

    @Test
    fun `when getProfile Should Not Null and Return Success`() = runTest {
        val expectedProfile = generateDummyProfile()
        val nameMultiPart = MultipartBody.Part.createFormData("userName", "name")
        val imageMultiPart = MultipartBody.Part.createFormData(
            "userImage",
            "image"
        )
        whenever(apiService.profile(nameMultiPart, imageMultiPart)).thenReturn(expectedProfile)
        val actualProfile = profileRepository.profile(nameMultiPart, imageMultiPart)
        assertNotNull(actualProfile)
        actualProfile.observeForTesting {
            assertEquals(expectedProfile.profile, (actualProfile.value as Result.Success).data)
        }
    }

    @Test
    fun `when get profile failed Return Error Message`() = runTest {
        val expectedError = RuntimeException("Update Error")
        val nameMultiPart = MultipartBody.Part.createFormData("userName", "name")
        val imageMultiPart = MultipartBody.Part.createFormData(
            "userImage",
            "image"
        )
        whenever(apiService.profile(nameMultiPart, imageMultiPart)).thenThrow(expectedError)
        val actualProfile = profileRepository.profile(nameMultiPart, imageMultiPart)
        actualProfile.observeForTesting {
            assertEquals(expectedError.message, (actualProfile.value as Result.Error).error.message)
        }
    }
}
