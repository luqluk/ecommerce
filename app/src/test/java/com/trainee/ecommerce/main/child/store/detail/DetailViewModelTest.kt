package com.trainee.ecommerce.main.child.store.detail

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.local.WishlistEntity
import com.trainee.ecommerce.core.data.remote.response.CartEntity
import com.trainee.ecommerce.core.data.remote.response.DetailResponse
import com.trainee.ecommerce.core.data.remote.response.ReviewResponse
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.core.util.Event
import com.trainee.ecommerce.util.DataDummy
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.getOrAwaitValue
import com.trainee.ecommerce.util.observeForTesting
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

class DetailViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val dispatcherRule = DispatcherRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var context: Context

    @Mock
    private lateinit var savedStateHandle: SavedStateHandle

    private lateinit var detailViewModel: DetailViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        detailViewModel = DetailViewModel(context, mainRepository, savedStateHandle)
    }

    @Test
    fun `when product id not empty should call get product`() {
        val expectedResponse = MutableLiveData<Result<DetailResponse>>()
        expectedResponse.value = Result.Success(DataDummy.generateDummyDetail())

        val expectedValue = "id"
        whenever(mainRepository.detailProduct(expectedValue)).thenReturn(expectedResponse)
        whenever(savedStateHandle.get<String>("id")).thenReturn(expectedValue)
        detailViewModel.getDetailProduct()

        detailViewModel.detailProduct.getOrAwaitValue()

        verify(mainRepository).detailProduct(expectedValue)
    }

    @Test
    fun `when product id not empty should call get review`() {
        val expectedResponse = MutableLiveData<Result<ReviewResponse>>()
        expectedResponse.value = Result.Success(DataDummy.generateDummyReview())

        val expectedValue = "id"

        whenever(mainRepository.reviewProduct(expectedValue)).thenReturn(expectedResponse)
        whenever(savedStateHandle.get<String>("id")).thenReturn(expectedValue)
        detailViewModel.getReview()

        detailViewModel.reviewProduct.getOrAwaitValue()

        verify(mainRepository).reviewProduct(expectedValue)
    }

    @Test
    fun `when insert cart should return success`() {
        val expectedResponse = MutableLiveData<Result<Event<String>>>()
        expectedResponse.value = Result.Success(Event("Success"))
        whenever(mainRepository.insertCart(DataDummy.generateDummyCart())).thenReturn(
            expectedResponse
        )
        detailViewModel.insertCart(DataDummy.generateDummyCart())

        val actualResponse = detailViewModel.insert.getOrAwaitValue()
        assertEquals(expectedResponse.value, actualResponse)
    }

    @Test
    fun `when insert wishlist should return success`() {
        val expectedResponse = MutableLiveData<Result<Event<String>>>()
        expectedResponse.value = Result.Success(Event("Success"))

        val expectedValue = DataDummy.generateDummyDetail().data
        whenever(mainRepository.insertWishlist(expectedValue)).thenReturn(expectedResponse)
        detailViewModel.insertWishlist(expectedValue)

        detailViewModel.insertWishlist.getOrAwaitValue()
        verify(mainRepository).insertWishlist(expectedValue)
    }

    @Test
    fun `when update cart should return success`() = runTest {
        val expectedResponse = MutableLiveData<Result<Event<String>>>()
        expectedResponse.value = Result.Success(Event("Success"))

        val expectedValue = listOf(DataDummy.generateDummyCart())
        whenever(mainRepository.updateCart(expectedValue)).thenReturn(Unit)

        detailViewModel.update(expectedValue).getOrAwaitValue()
        verify(mainRepository).updateCart(expectedValue)
    }

    @Test
    fun `when update cart failed should return error`() = runTest {
        val expectedResponse = RuntimeException("Error")
        val expectedValue = listOf(DataDummy.generateDummyCart())

        whenever(mainRepository.updateCart(expectedValue)).thenThrow(expectedResponse)
        detailViewModel.update(expectedValue)

        val actualResponse = detailViewModel.update(expectedValue)
        actualResponse.observeForTesting {
            assertEquals(
                expectedResponse.message,
                (actualResponse.value as Result.Error).error.message
            )
        }
    }

    @Test
    fun `get cart should not null and return data`() {
        val expectedResponse: MutableLiveData<List<CartEntity>> =
            MutableLiveData(listOf(DataDummy.generateDummyCart()))
        whenever(mainRepository.getAllCart()).thenReturn(expectedResponse)
        val actualResponse = detailViewModel.getAllCart().getOrAwaitValue()

        assertNotNull(actualResponse)
        assertEquals(expectedResponse.value, actualResponse)
    }

    @Test
    fun `get wishlist should not null and return data`() {
        val expectedResponse: MutableLiveData<List<WishlistEntity>> =
            MutableLiveData(listOf(DataDummy.generateDummyWishlist()))
        whenever(mainRepository.getAllWishlist()).thenReturn(expectedResponse)
        val actualResponse = detailViewModel.getAllWishlist().getOrAwaitValue()

        assertNotNull(actualResponse)
        assertEquals(expectedResponse.value, actualResponse)
    }

    @Test
    fun `get notification should not null and return data`() {
        val expectedResponse = MutableLiveData(listOf(DataDummy.generateDummyNotification()))
        whenever(mainRepository.getAllNotification()).thenReturn(expectedResponse)
        val actualResponse = detailViewModel.getNotification().getOrAwaitValue()

        assertNotNull(actualResponse)
        assertEquals(expectedResponse.value, actualResponse)
    }

    @Test
    fun `when wishlist deleted should return success`() = runTest {
        val expectedResponse = MutableLiveData<Result<Event<String>>>()
        expectedResponse.value = Result.Success(Event("Success"))

        val expectedValue = DataDummy.generateDummyWishlist()
        whenever(mainRepository.deleteWishlist(expectedValue)).thenReturn(expectedResponse)
        detailViewModel.deleteWishlist(expectedValue)

        detailViewModel.delete.getOrAwaitValue()
        verify(mainRepository).deleteWishlist(expectedValue)
    }

    @Test
    fun `initial checked id`() {
        assertEquals(0, detailViewModel.checkedId)
    }

    @Test
    fun `set checked id`() {
        detailViewModel.checkedId = 1
        assertEquals(1, detailViewModel.checkedId)
    }
}
