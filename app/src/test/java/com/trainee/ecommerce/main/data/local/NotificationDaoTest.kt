package com.trainee.ecommerce.main.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.trainee.ecommerce.core.data.local.AppDatabase
import com.trainee.ecommerce.core.data.local.NotifDao
import com.trainee.ecommerce.core.data.local.NotificationEntity
import com.trainee.ecommerce.util.DataDummy.generateDummyNotification
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class NotificationDaoTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var dao: NotifDao

    @Before
    fun initDb() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.notificationDao()
    }

    @Test
    fun `insert and get notification`() = runTest {
        dao.insertNotification(actualNotification)
        val expectedNotif = dao.getNotification().getOrAwaitValue()
        println(expectedNotif)
        assertEquals(expectedNotif, listOf(actualNotification))
    }

    @Test
    fun `insert and delete notification`() = runTest {
        dao.insertNotification(actualNotification)
        dao.deleteAll()
        val expectedNotif = dao.getNotification().getOrAwaitValue()
        assertEquals(expectedNotif, emptyList<NotificationEntity>())
    }

    @Test
    fun `insert and update notification`() = runTest {
        dao.insertNotification(actualNotification)

        val newNotification = actualNotification.copy(isNew = false)
        dao.update(newNotification)

        val expectedNotif = dao.getNotification().getOrAwaitValue()
        assertEquals(expectedNotif, listOf(newNotification))
    }

    companion object {
        private val actualNotification = generateDummyNotification()
    }
}
