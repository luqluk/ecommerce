package com.trainee.ecommerce.main.child.wishlist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.repository.MainRepository
import com.trainee.ecommerce.core.util.Event
import com.trainee.ecommerce.util.DataDummy
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.verify
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class WishlistViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val dispatcherRule = DispatcherRule()

    private lateinit var mainRepository: MainRepository

    private lateinit var wishlistViewModel: WishlistViewModel

    @Before
    fun setUp() {
        mainRepository = mock()
        wishlistViewModel = WishlistViewModel(mainRepository)
    }

    @Test
    fun `when get wishlist should call get wishlist`() = runTest {
        val expectedResponse = MutableLiveData(listOf(DataDummy.generateDummyWishlist()))
        whenever(mainRepository.getAllWishlist()).thenReturn(expectedResponse)

        backgroundScope.launch {
            val actualResponse = wishlistViewModel.getAllWishlist.getOrAwaitValue()
            assertEquals(expectedResponse.value, actualResponse)
        }

        verify(mainRepository).getAllWishlist()
    }

    @Test
    fun `when get cart should call get cart`() = runTest {
        val expectedResponse = MutableLiveData(listOf(DataDummy.generateDummyCart()))
        whenever(mainRepository.getAllCart()).thenReturn(expectedResponse)

        val actualResponse = wishlistViewModel.getAllCart().getOrAwaitValue()
        assertEquals(expectedResponse.value, actualResponse)

        verify(mainRepository).getAllCart()
    }

    @Test
    fun `when get wishlist should call get cart`() = runTest {
        backgroundScope.launch {
            wishlistViewModel.getAllWishlist.observeForever {
            }
        }
    }

    @Test
    fun `when insert should return data`() {
        val expectedResponse = MutableLiveData<Result<Event<String>>>()
        expectedResponse.value = Result.Success(Event(""))

        val expectedValue = DataDummy.generateDummyCart()
        whenever(mainRepository.insertCart(expectedValue)).thenReturn(expectedResponse)

        wishlistViewModel.insertCart(expectedValue)

        wishlistViewModel.insert.getOrAwaitValue()

        verify(mainRepository).insertCart(expectedValue)
    }

    @Test
    fun `delete wishlist`() {
        val expectedResponse = MutableLiveData<Result<Event<String>>>()
        expectedResponse.value = Result.Success(Event(""))

        val expectedValue = DataDummy.generateDummyWishlist()
        whenever(mainRepository.deleteWishlist(expectedValue)).thenReturn(expectedResponse)

        wishlistViewModel.deleteWishlist(expectedValue)

        wishlistViewModel.delete.getOrAwaitValue()

        verify(mainRepository).deleteWishlist(expectedValue)
    }

    @Test
    fun `check span count`() {
        val expectedResponse = 1
        wishlistViewModel.spanCount = 2
        assertEquals(expectedResponse + 1, wishlistViewModel.spanCount)
    }
}
