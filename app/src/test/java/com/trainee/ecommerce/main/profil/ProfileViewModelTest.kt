package com.trainee.ecommerce.main.profil

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.trainee.ecommerce.core.data.Result
import com.trainee.ecommerce.core.data.remote.response.Profile
import com.trainee.ecommerce.core.data.repository.ProfileRepository
import com.trainee.ecommerce.util.DataDummy
import com.trainee.ecommerce.util.DispatcherRule
import com.trainee.ecommerce.util.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.MultipartBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class ProfileViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var profileRepository: ProfileRepository
    private lateinit var profileViewModel: ProfileViewModel

    @Before
    fun setUp() {
        profileViewModel = ProfileViewModel(profileRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val mainDispatcherRule = DispatcherRule()

    @Test
    fun `When Update Profile Should Not Null and Return Success`() {
        val nameMultiPart = MultipartBody.Part.createFormData("userName", "name")
        val imageMultiPart = MultipartBody.Part.createFormData(
            "userImage",
            "image"
        )

        val expectedResponse = MutableLiveData<Result<Profile>>()
        expectedResponse.value = Result.Success(DataDummy.generateDummyProfile().profile)

        whenever(profileRepository.profile(imageMultiPart, nameMultiPart)).thenReturn(
            expectedResponse
        )
        val actualResponse =
            profileViewModel.updateProfile(imageMultiPart, nameMultiPart).getOrAwaitValue()

        assertNotNull(expectedResponse)
        assertEquals(
            (expectedResponse.value as Result.Success).data,
            (actualResponse as Result.Success).data
        )
    }
}
