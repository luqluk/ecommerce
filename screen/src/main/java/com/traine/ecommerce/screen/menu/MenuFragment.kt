package com.traine.ecommerce.screen.menu

import android.os.Bundle
import android.view.View
import com.android.commons.base.BaseFragment
import com.traine.ecommerce.screen.databinding.FragmentMenuBinding


class MenuFragment : BaseFragment<FragmentMenuBinding>(FragmentMenuBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {

            text.text = "THIS IS A MODULAR FRAGMENT WITH BASE FRAGMENT"


        }
    }
}